/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50527
Source Host           : 127.0.0.1:3306
Source Database       : r3_bi

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2015-12-23 08:39:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `crm_contacts`
-- ----------------------------
DROP TABLE IF EXISTS `crm_contacts`;
CREATE TABLE `crm_contacts` (
  `id` varchar(45) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `dept` varchar(255) DEFAULT NULL,
  `birt` varchar(255) DEFAULT NULL,
  `deptpr` varchar(255) DEFAULT NULL,
  `qian` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `fphone` varchar(255) DEFAULT NULL,
  `ophone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `zhuli` varchar(255) DEFAULT NULL,
  `zhuliphone` varchar(255) DEFAULT NULL,
  `zhulimobile` varchar(255) DEFAULT NULL,
  `otheraddress` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `share` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_contacts
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_account`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_account`;
CREATE TABLE `rivu5_account` (
  `ID` varchar(32) NOT NULL,
  `TID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `BALANCE` double DEFAULT NULL,
  `AMOUNT` double DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LASTOP` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SPARE` varchar(255) DEFAULT NULL,
  `SPARE1` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155529560` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_account
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_analyzereport`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_analyzereport`;
CREATE TABLE `rivu5_analyzereport` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `REPORTTYPE` varchar(32) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `OBJECTCOUNT` int(11) DEFAULT NULL,
  `DICID` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DESCRIPTION` longtext,
  `HTML` longtext,
  `REPORTPACKAGE` varchar(255) DEFAULT NULL,
  `USEACL` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `rolename` text,
  `userid` text,
  `blacklist` text,
  `REPORTCONTENT` text,
  `reportmodel` varchar(32) DEFAULT NULL,
  `updatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `creater` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155529680` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_analyzereport
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_analyzereportmodel`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_analyzereportmodel`;
CREATE TABLE `rivu5_analyzereportmodel` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `TB` varchar(32) DEFAULT NULL,
  `DB` varchar(32) DEFAULT NULL,
  `CUBE` varchar(32) DEFAULT NULL,
  `MODELTYPE` varchar(255) DEFAULT NULL,
  `STYLESTR` varchar(255) DEFAULT NULL,
  `CSSCLASSNAME` varchar(255) DEFAULT NULL,
  `MPOSLEFT` varchar(32) DEFAULT NULL,
  `MPOSTOP` varchar(32) DEFAULT NULL,
  `DSTYPE` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `OBJECTID` varchar(32) DEFAULT NULL,
  `REPORTID` varchar(32) DEFAULT NULL,
  `SORTINDEX` int(11) DEFAULT NULL,
  `ROWDIMENSION` longtext,
  `COLDIMENSION` longtext,
  `MEASURE` longtext,
  `EVENTSTR` varchar(255) DEFAULT NULL,
  `DSMODEL` varchar(32) DEFAULT NULL,
  `VIEWTYPE` varchar(32) DEFAULT NULL,
  `CHARTEMPLET` varchar(32) DEFAULT NULL,
  `CHARTDATATYPE` varchar(100) DEFAULT NULL,
  `CHART3D` varchar(32) DEFAULT NULL,
  `XTITLE` varchar(255) DEFAULT NULL,
  `YTITLE` varchar(50) DEFAULT NULL,
  `CHARTTITLE` varchar(32) DEFAULT NULL,
  `DISPLAYBORDER` varchar(32) DEFAULT NULL,
  `BORDERCOLOR` varchar(32) DEFAULT NULL,
  `DISPLAYDESC` varchar(32) DEFAULT NULL,
  `FILTERSTR` longtext,
  `SORTSTR` varchar(255) DEFAULT NULL,
  `LABELTEXT` varchar(32) DEFAULT NULL,
  `FORMDISPLAY` varchar(32) DEFAULT NULL,
  `LABELSTYLE` varchar(32) DEFAULT NULL,
  `FORMNAME` varchar(32) DEFAULT NULL,
  `DEFAULTVALUE` varchar(255) DEFAULT NULL,
  `EXCHANGERW` smallint(6) DEFAULT NULL,
  `QUERYTEXT` longtext,
  `DISPLAYTITLE` smallint(6) DEFAULT NULL,
  `TITLESTR` varchar(255) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `ROWFORMATSTR` varchar(4000) DEFAULT NULL,
  `COLFORMATSTR` varchar(4000) DEFAULT NULL,
  `STARTr` varchar(255) DEFAULT NULL,
  `ENDT` varchar(255) DEFAULT NULL,
  `sqldialect` varchar(255) DEFAULT NULL,
  `pageSize` int(11) DEFAULT NULL,
  `isloadfulldata` varchar(255) DEFAULT NULL,
  `pagesetting` varchar(32) DEFAULT NULL,
  `REPORTCONTENT` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155529750` (`ID`),
  KEY `FK21289552CE4E2285` (`REPORTID`),
  KEY `FK2128955225F5B3A5` (`TB`),
  KEY `FK21289552F9EF0726` (`CUBE`),
  KEY `FK212895522DA4DEB5` (`DB`),
  KEY `FK2128955260C1BE8C` (`pagesetting`),
  CONSTRAINT `rivu5_analyzereportmodel_ibfk_1` FOREIGN KEY (`pagesetting`) REFERENCES `rivu5_analyzereportpagesetting` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_analyzereportmodel
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_analyzereportpagesetting`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_analyzereportpagesetting`;
CREATE TABLE `rivu5_analyzereportpagesetting` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `height` varchar(32) DEFAULT NULL,
  `font` varchar(500) DEFAULT NULL,
  `titlecolor` varchar(32) DEFAULT NULL,
  `backcolor` varchar(32) DEFAULT NULL,
  `width` varchar(500) DEFAULT NULL,
  `backstyle` varchar(10) DEFAULT NULL,
  `widthcolor` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_analyzereportpagesetting
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_auth`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_auth`;
CREATE TABLE `rivu5_auth` (
  `ID` varchar(32) NOT NULL COMMENT '主键',
  `resourceid` varchar(32) DEFAULT NULL COMMENT '资源ID',
  `resourcetype` varchar(10) DEFAULT NULL COMMENT '资源类型 1 报表  2模型',
  `resourcedic` varchar(32) DEFAULT NULL,
  `authread` varchar(45) DEFAULT NULL COMMENT '操作权限 1读',
  `authtype` varchar(255) DEFAULT NULL,
  `ownerid` varchar(32) DEFAULT NULL COMMENT '所有者id',
  `ownertype` varchar(10) DEFAULT NULL COMMENT '所有者类型',
  `orgi` varchar(16) DEFAULT NULL COMMENT 'orgi',
  `createtime` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `dataid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_auth
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_blacklist`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_blacklist`;
CREATE TABLE `rivu5_blacklist` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ROLES` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `LISTTYPE` varchar(255) DEFAULT NULL,
  `USERNAMES` varchar(255) DEFAULT NULL,
  `IPSTART` varchar(255) DEFAULT NULL,
  `IPEND` varchar(255) DEFAULT NULL,
  `IPROLES` varchar(255) DEFAULT NULL,
  `TIMELIMIT` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155529830` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_blacklist
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_category`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_category`;
CREATE TABLE `rivu5_category` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `BLOCKLISTTEMPLET` varchar(255) DEFAULT NULL,
  `DATASOURCE` varchar(255) DEFAULT NULL,
  `FULLCONTENTVIEW` varchar(255) DEFAULT NULL,
  `DEFAULTCATE` smallint(6) DEFAULT NULL,
  `MLT` smallint(6) DEFAULT NULL,
  `MLTFIELD` varchar(255) DEFAULT NULL,
  `MLTCOUNT` int(11) DEFAULT NULL,
  `MLTNTP` int(11) DEFAULT NULL,
  `RULENAME` varchar(255) DEFAULT NULL,
  `FACETFIELD` longtext,
  `FACETQUERY` longtext,
  `FACETDATE` longtext,
  `SECLEV` varchar(50) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155529880` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_category
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_cloudpackage`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_cloudpackage`;
CREATE TABLE `rivu5_cloudpackage` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ORGI` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USERID` varchar(255) DEFAULT NULL,
  `SPARE0` varchar(255) DEFAULT NULL,
  `PRICE` double DEFAULT NULL,
  `SPARE1` varchar(255) DEFAULT NULL,
  `SERVERHOST` varchar(255) DEFAULT NULL,
  `HOSTAREA` varchar(255) DEFAULT NULL,
  `CLUSTERTYPE` varchar(255) DEFAULT NULL,
  `SHARECORE` smallint(6) NOT NULL,
  `INDEXSIZE` bigint(20) NOT NULL,
  `TRANSFEROUT` bigint(20) NOT NULL,
  `DOCNUM` bigint(20) NOT NULL,
  `CORES` int(11) NOT NULL,
  `PHONESUPPORT` int(11) NOT NULL,
  `MAILSUPPORT` int(11) NOT NULL,
  `BBSUPPORT` int(11) NOT NULL,
  `PACKAGETYPE` int(11) NOT NULL,
  `CLUSTERS` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155529950` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_cloudpackage
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_clusterserver`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_clusterserver`;
CREATE TABLE `rivu5_clusterserver` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ORGI` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USERID` varchar(255) DEFAULT NULL,
  `SPARE0` varchar(255) DEFAULT NULL,
  `SPARE1` varchar(255) DEFAULT NULL,
  `SERVERHOST` varchar(255) DEFAULT NULL,
  `ZKHOST` varchar(255) DEFAULT NULL,
  `HOSTAREA` varchar(2000) DEFAULT NULL,
  `CLUSTERTYPE` varchar(255) DEFAULT NULL,
  `SERVERS` int(11) NOT NULL,
  `SEARCHCORES` int(11) NOT NULL,
  `PORT` int(11) DEFAULT NULL,
  `BACK` tinyint(4) DEFAULT NULL,
  `EXCHANGE` tinyint(4) DEFAULT NULL,
  `DISPATCH` tinyint(4) DEFAULT NULL,
  `ANALYSE` tinyint(4) DEFAULT NULL,
  `OTHERS` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530020` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_clusterserver
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_columnproperties`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_columnproperties`;
CREATE TABLE `rivu5_columnproperties` (
  `id` varchar(32) NOT NULL,
  `format` varchar(255) DEFAULT NULL,
  `prefix` varchar(255) DEFAULT NULL,
  `width` varchar(255) DEFAULT NULL,
  `suffix` varchar(255) DEFAULT NULL,
  `font` varchar(255) DEFAULT NULL,
  `colname` varchar(255) DEFAULT NULL,
  `border` varchar(255) DEFAULT NULL,
  `decimalCount` varchar(255) DEFAULT NULL,
  `sepsymbol` varchar(255) DEFAULT NULL,
  `alignment` varchar(255) DEFAULT NULL,
  `fontStyle` varchar(255) DEFAULT NULL,
  `fontColor` varchar(255) DEFAULT NULL,
  `paramName` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `dataid` varchar(255) DEFAULT NULL,
  `modelid` varchar(255) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  `cur` varchar(255) DEFAULT NULL,
  `hyp` varchar(255) DEFAULT NULL,
  `timeFormat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKB671B6F262D48197` (`modelid`),
  CONSTRAINT `rivu5_columnproperties_ibfk_1` FOREIGN KEY (`modelid`) REFERENCES `rivu5_analyzereportmodel` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_columnproperties
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_configureparam`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_configureparam`;
CREATE TABLE `rivu5_configureparam` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `DEFAULTTHREADS` int(11) NOT NULL,
  `OPERATIONMODE` int(11) NOT NULL,
  `MAXTHREADS` int(11) NOT NULL,
  `COUNTTHREADS` int(11) NOT NULL,
  `CACHEREC` int(11) NOT NULL,
  `CACHEMEMORY` int(11) NOT NULL,
  `MAXCACHEMEMORY` int(11) NOT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `PROTYPE` varchar(32) DEFAULT NULL,
  `VALUE` text,
  `LEVELS` varchar(32) DEFAULT NULL,
  `description` text,
  `TITLE` varchar(255) DEFAULT NULL,
  `PARAMTYPE` varchar(32) DEFAULT NULL,
  `PARAMVALUE` varchar(255) DEFAULT NULL,
  `type` varchar(32) DEFAULT NULL COMMENT '数据类型',
  `earlywarning` varchar(32) DEFAULT NULL COMMENT '是否预警',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530080` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_configureparam
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_crawltask`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_crawltask`;
CREATE TABLE `rivu5_crawltask` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `DOCTYPE` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `LASTCRAWL` varchar(255) DEFAULT NULL,
  `SUBDIR` varchar(255) DEFAULT NULL,
  `PARSEPARAM` varchar(255) DEFAULT NULL,
  `NATIVEXML` varchar(255) DEFAULT NULL,
  `LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CREATEUSER` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `LISTBLOCKTEMPLET` varchar(255) DEFAULT NULL,
  `ACCOUNT` varchar(255) DEFAULT NULL,
  `RESOURCEURI` varchar(255) DEFAULT NULL,
  `MOREPARAMES` varchar(255) DEFAULT NULL,
  `DRIVERPLUGIN` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530160` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_crawltask
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_cube`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_cube`;
CREATE TABLE `rivu5_cube` (
  `ID` varchar(255) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DB` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `MPOSLEFT` varchar(32) DEFAULT NULL,
  `MPOSTOP` varchar(32) DEFAULT NULL,
  `TYPEID` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `DSTYPE` varchar(255) DEFAULT NULL,
  `MODELTYPE` varchar(32) DEFAULT NULL,
  `createdata` varchar(32) DEFAULT NULL,
  `startindex` int(11) DEFAULT NULL,
  `startdate` datetime DEFAULT NULL,
  `dataid` varchar(32) DEFAULT NULL,
  `dataflag` varchar(255) DEFAULT NULL,
  `CREATER` varchar(32) DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `CUBEFILE` longtext,
  UNIQUE KEY `SQL121227155530220` (`ID`),
  KEY `FKD104A39E2DA4DEB5` (`DB`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_cube
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_cubelevel`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_cubelevel`;
CREATE TABLE `rivu5_cubelevel` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COLUMNAME` varchar(255) DEFAULT NULL,
  `UNIQUEMEMBERS` smallint(6) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `LEVELTYPE` varchar(32) DEFAULT NULL,
  `TABLENAME` varchar(255) DEFAULT NULL,
  `CUBEID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `SORTINDEX` int(11) DEFAULT NULL,
  `PARAMETERS` longtext,
  `ATTRIBUE` longtext,
  `DIMID` varchar(32) DEFAULT NULL,
  `PERMISSIONS` smallint(6) DEFAULT NULL,
  `TABLEPROPERTY` varchar(32) DEFAULT NULL,
  `FORMATSTR` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530260` (`ID`),
  KEY `FK6B288AA64F175B08` (`TABLEPROPERTY`),
  KEY `FK6B288AA660EA53CD` (`DIMID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_cubelevel
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_cubemeasure`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_cubemeasure`;
CREATE TABLE `rivu5_cubemeasure` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COLUMNAME` varchar(255) DEFAULT NULL,
  `UNIQUEMEMBERS` smallint(6) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `LEVELTYPE` varchar(32) DEFAULT NULL,
  `TABLENAME` varchar(255) DEFAULT NULL,
  `CUBEID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `SORTINDEX` int(11) DEFAULT NULL,
  `PARAMETERS` longtext,
  `ATTRIBUE` longtext,
  `MID` varchar(32) DEFAULT NULL,
  `AGGREGATOR` varchar(32) DEFAULT NULL,
  `FORMATSTRING` varchar(255) DEFAULT NULL,
  `CALCULATEDMEMBER` smallint(6) DEFAULT NULL,
  `MODELTYPE` varchar(32) DEFAULT NULL,
  `MEASURE` varchar(32) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530310` (`ID`),
  KEY `FK76F5540039E09E8B` (`MEASURE`),
  KEY `FK76F55400A94EDB01` (`CUBEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_cubemeasure
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_cubemetadata`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_cubemetadata`;
CREATE TABLE `rivu5_cubemetadata` (
  `ID` varchar(32) NOT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TB` varchar(32) DEFAULT NULL,
  `ORGI` varchar(255) DEFAULT NULL,
  `CUBE` varchar(32) DEFAULT NULL,
  `POSTOP` varchar(32) DEFAULT NULL,
  `POSLEFT` varchar(32) DEFAULT NULL,
  `MTYPE` varchar(5) DEFAULT NULL,
  `NAMEALIAS` varchar(255) DEFAULT NULL,
  `PARAMETERS` varchar(255) DEFAULT NULL,
  `ATTRIBUE` longtext,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530340` (`ID`),
  KEY `FK871BA0CD25F5B3A5` (`TB`),
  KEY `FK871BA0CDF9EF0726` (`CUBE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_cubemetadata
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_databasetask`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_databasetask`;
CREATE TABLE `rivu5_databasetask` (
  `ID` varchar(32) NOT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ENCODING` varchar(255) DEFAULT NULL,
  `PORT` int(11) NOT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `DOCTYPE` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `LASTCRAWL` varchar(255) DEFAULT NULL,
  `LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CREATEUSER` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `LISTBLOCKTEMPLET` varchar(255) DEFAULT NULL,
  `ACCOUNT` varchar(255) DEFAULT NULL,
  `ATTACHMENT` varchar(255) DEFAULT NULL,
  `DATABASETYPE` varchar(255) DEFAULT NULL,
  `DATABASENAME` varchar(255) DEFAULT NULL,
  `CONNECTPARAM` varchar(255) DEFAULT NULL,
  `DATABASEURL` varchar(255) DEFAULT NULL,
  `DRIVERCLAZZ` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `sqldialect` varchar(255) DEFAULT NULL,
  `JNDIPARAM` varchar(255) DEFAULT NULL,
  `JNDINAME` varchar(255) DEFAULT NULL,
  `CONNCTIONTYPE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530370` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_databasetask
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_datadic`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_datadic`;
CREATE TABLE `rivu5_datadic` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(32) DEFAULT NULL,
  `TITLE` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `STATUS` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `CREATER` varchar(255) DEFAULT NULL,
  `PUBLISHEDTYPE` varchar(32) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `TABTYPE` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530400` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_datadic
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_datasource`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_datasource`;
CREATE TABLE `rivu5_datasource` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `DRIVER` varchar(255) DEFAULT NULL,
  `URL` longtext,
  `USERNAME` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `MINCONN` int(11) DEFAULT NULL,
  `MAXCONN` int(11) DEFAULT NULL,
  `INITCONN` int(11) DEFAULT NULL,
  `TIMEOUTVALUE` int(11) DEFAULT NULL,
  `WAITTIME` int(11) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530430` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_datasource
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_datatable`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_datatable`;
CREATE TABLE `rivu5_datatable` (
  `id` varchar(32) NOT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `typeid` varchar(255) DEFAULT NULL,
  `ename` varchar(255) DEFAULT NULL,
  `cname` varchar(255) DEFAULT NULL,
  `typename` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_datatable
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_datatabletype`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_datatabletype`;
CREATE TABLE `rivu5_datatabletype` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `parentid` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_datatabletype
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_datatableview`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_datatableview`;
CREATE TABLE `rivu5_datatableview` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `tablename` varchar(255) DEFAULT NULL,
  `typeid` varchar(255) DEFAULT NULL,
  `typename` varchar(255) DEFAULT NULL,
  `tableid` varchar(255) DEFAULT NULL,
  `databaseid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_datatableview
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_datatableviewfield`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_datatableviewfield`;
CREATE TABLE `rivu5_datatableviewfield` (
  `id` varchar(32) NOT NULL,
  `viewid` varchar(45) DEFAULT NULL,
  `viewname` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `field` varchar(45) DEFAULT NULL,
  `datatype` varchar(255) DEFAULT NULL,
  `viewtype` varchar(255) DEFAULT NULL,
  `head` smallint(6) DEFAULT NULL,
  `addfield` smallint(6) DEFAULT NULL,
  `edit` smallint(6) DEFAULT NULL,
  `validateadd` varchar(4000) DEFAULT NULL,
  `validateedit` varchar(4000) DEFAULT NULL,
  `validateother` varchar(4000) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `classname` varchar(255) DEFAULT NULL,
  `cssstyle` varchar(1000) DEFAULT NULL,
  `phonenumber` smallint(6) DEFAULT NULL,
  `readonly` smallint(6) DEFAULT NULL,
  `querylistlshow` smallint(6) DEFAULT NULL,
  `defaultvalue` varchar(255) DEFAULT NULL,
  `orderfield` smallint(6) DEFAULT NULL,
  `ordersequence` int(11) DEFAULT NULL,
  `ordertype` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `display` smallint(6) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `field` (`field`),
  KEY `FK4E6DE33AB25E0A4D` (`viewid`),
  KEY `FK4E6DE33A4E1905F` (`field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_datatableviewfield
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_datatype`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_datatype`;
CREATE TABLE `rivu5_datatype` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CLAZZ` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `COMPRESSD` smallint(6) DEFAULT NULL,
  `ENCRYPTION` varchar(255) DEFAULT NULL,
  `INDEXANALYZER` varchar(255) DEFAULT NULL,
  `QUERYANALYZER` varchar(255) DEFAULT NULL,
  `INDEXFILTER` varchar(255) DEFAULT NULL,
  `QUERYFILTER` varchar(255) DEFAULT NULL,
  `TOKENIZED` smallint(6) NOT NULL,
  `STORED` smallint(6) NOT NULL,
  `INDEXED` smallint(6) NOT NULL,
  `MULTVALUE` smallint(6) NOT NULL,
  `OMITNORMS` smallint(6) NOT NULL,
  `POSITIONINCREMENTGAP` int(11) NOT NULL,
  `SORTMISSINGLAST` smallint(6) NOT NULL,
  `SORTMISSINGFAST` smallint(6) NOT NULL,
  `OMITTERMFREQANDPOSITIONS` smallint(6) NOT NULL,
  `COMPRESSTHRESHOLD` int(11) NOT NULL,
  `DIMENSION` int(11) NOT NULL,
  `SUBFIELDTYPES` varchar(255) DEFAULT NULL,
  `PRECISIONSTEP` int(11) NOT NULL,
  `DATABASEDATATYPE` varchar(255) DEFAULT NULL,
  `MOREPARAM` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530450` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_datatype
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_dimension`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_dimension`;
CREATE TABLE `rivu5_dimension` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CUBEID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(255) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `SORTINDEX` int(11) DEFAULT NULL,
  `PARAMETERS` longtext,
  `ATTRIBUE` longtext,
  `POSLEFT` varchar(32) DEFAULT NULL,
  `POSTOP` varchar(32) DEFAULT NULL,
  `FORMATSTR` varchar(32) DEFAULT NULL,
  `MODELTYPE` varchar(32) DEFAULT NULL,
  `DIM` varchar(32) DEFAULT NULL,
  `ALLMEMBERNAME` varchar(32) DEFAULT NULL,
  `FKFIELD` varchar(255) DEFAULT NULL,
  `FKTABLE` varchar(255) DEFAULT NULL,
  `FKTABLEID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530490` (`ID`),
  KEY `FK66E5C79D5B3948B2` (`DIM`),
  KEY `FK66E5C79DA94EDB01` (`CUBEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_dimension
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_drilldown`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_drilldown`;
CREATE TABLE `rivu5_drilldown` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `dataid` varchar(255) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  `tdstyle` varchar(255) DEFAULT NULL,
  `reportid` varchar(255) DEFAULT NULL,
  `modelid` varchar(255) DEFAULT NULL,
  `paramname` varchar(255) DEFAULT NULL,
  `paramtype` varchar(255) DEFAULT NULL,
  `paramurl` varchar(255) DEFAULT NULL,
  `paramtarget` varchar(255) DEFAULT NULL,
  `paramreport` varchar(255) DEFAULT NULL,
  `paramvalue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK460671B462D48197` (`modelid`),
  CONSTRAINT `rivu5_drilldown_ibfk_1` FOREIGN KEY (`modelid`) REFERENCES `rivu5_analyzereportmodel` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_drilldown
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_expimplog`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_expimplog`;
CREATE TABLE `rivu5_expimplog` (
  `ID` varchar(32) NOT NULL,
  `ERROR` smallint(6) NOT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `ERRORMSG` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `OPTYPE` varchar(255) DEFAULT NULL,
  `OPTIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530530` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_expimplog
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_extensionpoints`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_extensionpoints`;
CREATE TABLE `rivu5_extensionpoints` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PARAMETERS` longtext,
  `CLAZZ` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `DSCRIPTION` varchar(255) DEFAULT NULL,
  `INTERFACECLAZZ` varchar(255) DEFAULT NULL,
  `EXTENSIONTYPE` varchar(255) DEFAULT NULL,
  `ICONIMAGEPATH` varchar(255) DEFAULT NULL,
  `IMAGETYPE` varchar(255) DEFAULT NULL,
  `ICONPATH` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530560` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_extensionpoints
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_fieldqueryfilter`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_fieldqueryfilter`;
CREATE TABLE `rivu5_fieldqueryfilter` (
  `ID` varchar(32) NOT NULL,
  `FIELDTYPEID` varchar(255) DEFAULT NULL,
  `SORTINDEX` varchar(255) DEFAULT NULL,
  `FILTERID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530600` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_fieldqueryfilter
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_fieldtypefilter`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_fieldtypefilter`;
CREATE TABLE `rivu5_fieldtypefilter` (
  `ID` varchar(32) NOT NULL,
  `FIELDTYPEID` varchar(255) DEFAULT NULL,
  `SORTINDEX` varchar(255) DEFAULT NULL,
  `FILTERID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530650` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_fieldtypefilter
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_ftp`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_ftp`;
CREATE TABLE `rivu5_ftp` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `DOCTYPE` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `FILEPATH` varchar(255) DEFAULT NULL,
  `LASTCRAWL` varchar(255) DEFAULT NULL,
  `SUBDIR` varchar(255) DEFAULT NULL,
  `PARSEPARAM` varchar(255) DEFAULT NULL,
  `NATIVEXML` varchar(255) DEFAULT NULL,
  `LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CREATEUSER` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `LISTBLOCKTEMPLET` varchar(255) DEFAULT NULL,
  `ACCOUNT` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530690` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_ftp
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_historyjobdetail`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_historyjobdetail`;
CREATE TABLE `rivu5_historyjobdetail` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SOURCE` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `CRAWLTASKID` varchar(255) DEFAULT NULL,
  `CLAZZ` varchar(255) DEFAULT NULL,
  `TASKFIRETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `TASKID` varchar(255) DEFAULT NULL,
  `STARTTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ENDTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `REPORTID` varchar(255) DEFAULT NULL,
  `PLANTASK` smallint(6) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CATALOG` varchar(255) DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `NICKNAME` varchar(255) DEFAULT NULL,
  `RESULTTYPE` smallint(6) DEFAULT NULL,
  `DATAID` varchar(1000) DEFAULT NULL COMMENT '报表id字符串',
  `DICID` varchar(1000) DEFAULT NULL COMMENT '目录id字符串',
  `PRIORITY` int(11) DEFAULT NULL COMMENT '优先级',
  `CRAWLTASK` varchar(32) DEFAULT NULL,
  `TARGETTASK` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530740` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_historyjobdetail
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_historyreport`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_historyreport`;
CREATE TABLE `rivu5_historyreport` (
  `ID` varchar(32) NOT NULL,
  `BYTES` bigint(20) NOT NULL,
  `THREADS` int(11) NOT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `ERRORMSG` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STARTTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ENDTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `AMOUNT` varchar(255) DEFAULT NULL,
  `PAGES` int(11) NOT NULL,
  `ERRORS` int(11) NOT NULL,
  `SPEED` double NOT NULL,
  `BYTESPEED` bigint(20) NOT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530800` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_historyreport
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_indexfield`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_indexfield`;
CREATE TABLE `rivu5_indexfield` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `STORED` smallint(6) NOT NULL,
  `INDEXED` smallint(6) NOT NULL,
  `MOREPARAM` varchar(255) DEFAULT NULL,
  `DATATYPE` varchar(255) DEFAULT NULL,
  `MULTIVALUED` smallint(6) NOT NULL,
  `COPYTO` varchar(255) DEFAULT NULL,
  `FIELDTYPE` varchar(255) DEFAULT NULL,
  `UNIQUEKEY` smallint(6) NOT NULL,
  `DEFAULTSEARCH` smallint(6) NOT NULL,
  `FACET` smallint(6) NOT NULL,
  `SORTABLE` smallint(6) NOT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530860` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_indexfield
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_indexfieldtype`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_indexfieldtype`;
CREATE TABLE `rivu5_indexfieldtype` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530900` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_indexfieldtype
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_instance`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_instance`;
CREATE TABLE `rivu5_instance` (
  `ID` varchar(32) NOT NULL,
  `TID` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `R3MASTE` varchar(255) DEFAULT NULL,
  `R3SLAVE` varchar(255) DEFAULT NULL,
  `ZKHOST` varchar(255) DEFAULT NULL,
  `RECSEARCHHIS` smallint(6) NOT NULL,
  `CLOUDPACKAGE` longtext,
  `DOCNUM` int(11) DEFAULT NULL,
  `INDEXSIZE` int(11) DEFAULT NULL,
  `TRANSFEROUT` int(11) DEFAULT NULL,
  `CORES` int(11) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `EDITTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EXPIRETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `INTRO` varchar(255) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `SPARE` varchar(255) DEFAULT NULL,
  `SPARE1` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530940` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_instance
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_instanceorder`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_instanceorder`;
CREATE TABLE `rivu5_instanceorder` (
  `ID` varchar(32) NOT NULL,
  `INSTANCEID` varchar(32) DEFAULT NULL,
  `PACKAGEID` varchar(32) DEFAULT NULL,
  `TID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `PRICE` double DEFAULT NULL,
  `NUM` int(11) DEFAULT NULL,
  `OFF` double DEFAULT NULL,
  `TOTAL` double DEFAULT NULL,
  `SIGNCODE` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PAYTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EXPTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SPARE` varchar(255) DEFAULT NULL,
  `SPARE1` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530990` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_instanceorder
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_jobdetail`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_jobdetail`;
CREATE TABLE `rivu5_jobdetail` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SOURCE` varchar(255) DEFAULT NULL,
  `CLAZZ` varchar(255) DEFAULT NULL,
  `TASKFIRETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CRAWLTASKID` varchar(255) DEFAULT NULL,
  `ORGI` varchar(255) DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `NICKNAME` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKID` varchar(255) DEFAULT NULL,
  `FETCHER` smallint(6) NOT NULL,
  `PAUSE` smallint(6) NOT NULL,
  `PLANTASK` smallint(6) NOT NULL,
  `SECURE_ID` varchar(32) DEFAULT NULL,
  `CONFIGURE_ID` varchar(32) DEFAULT NULL,
  `TAKSPLAN_ID` varchar(32) DEFAULT NULL,
  `CRAWLTASK` varchar(32) DEFAULT NULL,
  `TARGETTASK` varchar(32) DEFAULT NULL,
  `STARTINDEX` int(11) DEFAULT NULL,
  `LASTDATE` timestamp NULL DEFAULT NULL,
  `CREATETABLE` tinyint(4) DEFAULT NULL,
  `MEMO` text,
  `NEXTFIRETIME` timestamp NULL DEFAULT NULL,
  `CRONEXP` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(32) DEFAULT NULL,
  `usearea` varchar(255) DEFAULT '',
  `areafield` varchar(255) DEFAULT NULL,
  `areafieldtype` varchar(32) DEFAULT NULL,
  `arearule` varchar(255) DEFAULT NULL,
  `minareavalue` varchar(255) DEFAULT NULL,
  `maxareavalue` varchar(255) DEFAULT NULL,
  `formatstr` varchar(255) DEFAULT NULL,
  `DATAID` varchar(1000) DEFAULT NULL COMMENT '报表id字符串',
  `DICID` varchar(1000) DEFAULT NULL COMMENT '目录id字符串',
  `taskinfo` longtext COMMENT 'taskinfo信息',
  `PRIORITY` int(11) DEFAULT NULL COMMENT '优先级',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531080` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_jobdetail
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_localfile`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_localfile`;
CREATE TABLE `rivu5_localfile` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `DOCTYPE` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `FILEPATH` varchar(255) DEFAULT NULL,
  `LASTCRAWL` varchar(255) DEFAULT NULL,
  `SUBDIR` varchar(255) DEFAULT NULL,
  `PARSEPARAM` varchar(255) DEFAULT NULL,
  `NATIVEXML` varchar(255) DEFAULT NULL,
  `LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CREATEUSER` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `LISTBLOCKTEMPLET` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531150` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_localfile
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_log_event`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_log_event`;
CREATE TABLE `rivu5_log_event` (
  `id` varchar(32) NOT NULL,
  `port` int(11) NOT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `triggerwarning` varchar(255) DEFAULT NULL,
  `triggertime` varchar(255) DEFAULT NULL,
  `ipaddr` varchar(255) DEFAULT NULL,
  `groupid` varchar(255) DEFAULT NULL,
  `datatype` varchar(255) DEFAULT NULL,
  `dataflag` int(11) NOT NULL,
  `eventmsg` text,
  `eventdate` datetime DEFAULT NULL,
  `eventype` varchar(255) DEFAULT NULL,
  `eventlevel` varchar(255) DEFAULT NULL,
  `userid` varchar(32) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `dataid` varchar(32) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_log_event
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_log_report`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_log_report`;
CREATE TABLE `rivu5_log_report` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `type` varchar(255) DEFAULT NULL,
  `parameters` text,
  `throwable` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `usermail` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `error` text,
  `dataprocesstime` bigint(20) DEFAULT NULL,
  `dataformatime` bigint(20) DEFAULT NULL,
  `queryparsetime` bigint(20) DEFAULT NULL,
  `executetime` bigint(20) DEFAULT NULL,
  `classname` varchar(255) DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `detailtype` varchar(255) DEFAULT NULL,
  `url` text,
  `reportdic` varchar(255) DEFAULT NULL,
  `dataid` varchar(32) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  `datatype` varchar(32) DEFAULT NULL,
  `reportname` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `statues` varchar(255) DEFAULT NULL,
  `methodname` text,
  `linenumber` varchar(255) DEFAULT NULL,
  `querytime` int(255) DEFAULT NULL,
  `optext` varchar(255) DEFAULT NULL,
  `field6` varchar(255) DEFAULT NULL,
  `field7` varchar(255) DEFAULT NULL,
  `field8` varchar(255) DEFAULT NULL,
  `flowid` varchar(32) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `funtype` varchar(32) DEFAULT NULL,
  `fundesc` varchar(255) DEFAULT NULL,
  `triggerwarning` varchar(32) DEFAULT NULL,
  `triggertime` varchar(32) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_log_report
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_log_request`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_log_request`;
CREATE TABLE `rivu5_log_request` (
  `id` varchar(32) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `parameters` longtext,
  `throwable` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `usermail` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `error` text,
  `classname` varchar(255) DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `detailtype` varchar(255) DEFAULT NULL,
  `url` text,
  `reportdic` varchar(255) DEFAULT NULL,
  `reportname` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `statues` varchar(255) DEFAULT NULL,
  `methodname` text,
  `linenumber` varchar(255) DEFAULT NULL,
  `querytime` int(255) DEFAULT NULL,
  `optext` varchar(255) DEFAULT NULL,
  `field6` varchar(255) DEFAULT NULL,
  `field7` varchar(255) DEFAULT NULL,
  `field8` varchar(255) DEFAULT NULL,
  `flowid` varchar(32) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `funtype` varchar(32) DEFAULT NULL,
  `fundesc` varchar(255) DEFAULT NULL,
  `triggerwarning` varchar(32) DEFAULT NULL,
  `triggertime` varchar(32) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_log_request
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_log_systemaction`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_log_systemaction`;
CREATE TABLE `rivu5_log_systemaction` (
  `id` varchar(32) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `throwable` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `detailtype` varchar(255) DEFAULT NULL,
  `field6` varchar(255) DEFAULT NULL,
  `field7` varchar(255) DEFAULT NULL,
  `field8` varchar(255) DEFAULT NULL,
  `startid` varchar(32) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `times` int(11) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `triggerwarning` varchar(32) DEFAULT NULL,
  `triggertime` varchar(32) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `ipaddr` varchar(255) DEFAULT NULL,
  `port` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_log_systemaction
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_log_systeminfo`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_log_systeminfo`;
CREATE TABLE `rivu5_log_systeminfo` (
  `id` varchar(32) NOT NULL,
  `port` int(11) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `unavailabletype` varchar(32) DEFAULT NULL,
  `unavailabletime` bigint(11) DEFAULT NULL,
  `serverdowntime` bigint(20) DEFAULT NULL,
  `netread` bigint(20) NOT NULL,
  `netwrite` bigint(20) NOT NULL,
  `createdate` datetime DEFAULT NULL,
  `ipaddr` varchar(255) DEFAULT NULL,
  `groupid` varchar(255) DEFAULT NULL,
  `datatype` varchar(255) DEFAULT NULL,
  `cpuus` double NOT NULL,
  `cpusys` double NOT NULL,
  `cpuidle` double NOT NULL,
  `cpuwait` double NOT NULL,
  `cpunice` double NOT NULL,
  `cpupercent` double DEFAULT NULL,
  `memav` bigint(20) NOT NULL,
  `memused` bigint(20) NOT NULL,
  `memfree` bigint(20) NOT NULL,
  `diskfree` bigint(20) NOT NULL,
  `diskuse` bigint(20) NOT NULL,
  `diskusepercent` double NOT NULL,
  `disktotal` bigint(20) NOT NULL,
  `diskread` bigint(20) NOT NULL,
  `diskwrite` bigint(20) NOT NULL,
  `diskqueue` double NOT NULL,
  `netreadspeed` bigint(20) NOT NULL,
  `netwritespeed` bigint(20) NOT NULL,
  `netname` varchar(255) DEFAULT NULL,
  `mempercent` double NOT NULL,
  `memtotal` bigint(20) NOT NULL,
  `rxpackage` bigint(20) NOT NULL,
  `txpackage` bigint(20) NOT NULL,
  `txpackagespeed` double DEFAULT NULL,
  `rxpackagespeed` double DEFAULT NULL,
  `rxerrors` bigint(20) NOT NULL,
  `txerrors` bigint(20) NOT NULL,
  `dataflag` int(11) DEFAULT NULL,
  `jvmmemtotal` bigint(20) DEFAULT NULL,
  `jvmmemfree` bigint(20) DEFAULT NULL,
  `jvmmemusepercent` double DEFAULT NULL,
  `jvmmemuse` bigint(20) DEFAULT NULL,
  `triggertime` varchar(32) DEFAULT NULL,
  `triggerwarning` varchar(32) DEFAULT NULL,
  `netspeed` bigint(20) DEFAULT NULL,
  `netuseprecent` double DEFAULT NULL,
  `uptime` bigint(20) DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  `ostype` varchar(255) DEFAULT NULL,
  `hardware` varchar(255) DEFAULT NULL,
  `tempdiskfree` bigint(20) DEFAULT NULL,
  `tempdisk` bigint(20) DEFAULT NULL,
  `tempdiskpercent` double DEFAULT NULL,
  `diskwritespeed` double DEFAULT NULL,
  `diskreadspeed` double DEFAULT NULL,
  `swaptotal` bigint(20) DEFAULT NULL,
  `swapuse` bigint(20) DEFAULT NULL,
  `swapfree` bigint(20) DEFAULT NULL,
  `swappercent` double DEFAULT NULL,
  `swappagein` bigint(20) DEFAULT NULL,
  `swappageout` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_log_systeminfo
-- ----------------------------
INSERT INTO `rivu5_log_systeminfo` VALUES ('297e1e8751cc3bef0151cc464189008d', '0', '', null, '0', '0', '0', '0', '2015-12-23 08:38:55', '', '', 'r3query_v50_server', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '290166227', '375914496', '93901160', '0.7502061128616333', '282013336', null, null, '0', '0', '1450830460127', '2015-12-23 08:27:40', 'Windows 8 6.2', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `rivu5_log_systeminfo` VALUES ('297e1e8751cc3bef0151cc465513008e', '0', '', null, '0', '0', '0', '0', '2015-12-23 08:39:00', '', '', 'r3query_v50_server', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '290166228', '375914496', '93901160', '0.7502061128616333', '282013336', null, null, '0', '0', '1450830460127', '2015-12-23 08:27:40', 'Windows 8 6.2', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `rivu5_log_systeminfo` VALUES ('297e1e8751cc3bef0151cc46689b008f', '0', '', null, '0', '0', '0', '0', '2015-12-23 08:39:05', '', '', 'r3query_v50_server', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '290166229', '375914496', '92927720', '0.7527956366539001', '282986776', null, null, '0', '0', '1450830460127', '2015-12-23 08:27:40', 'Windows 8 6.2', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `rivu5_log_systeminfo` VALUES ('297e1e8751cc3bef0151cc467c220090', '0', '', null, '0', '0', '0', '0', '2015-12-23 08:39:10', '', '', 'r3query_v50_server', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '290166230', '375914496', '92927720', '0.7527956366539001', '282986776', null, null, '0', '0', '1450830460127', '2015-12-23 08:27:40', 'Windows 8 6.2', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `rivu5_log_systeminfo` VALUES ('297e1e8751cc3bef0151cc468fab0091', '0', '', null, '0', '0', '0', '0', '2015-12-23 08:39:15', '', '', 'r3query_v50_server', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '290166231', '375914496', '91871016', '0.7556066513061523', '284043480', null, null, '0', '0', '1450830460127', '2015-12-23 08:27:40', 'Windows 8 6.2', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for `rivu5_log_systemout`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_log_systemout`;
CREATE TABLE `rivu5_log_systemout` (
  `id` varchar(32) NOT NULL,
  `orgi` varchar(32) DEFAULT NULL,
  `flowid` varchar(32) DEFAULT NULL,
  `logtype` varchar(32) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `msg` longtext,
  `LEVELS` varchar(32) DEFAULT NULL,
  `thread` varchar(255) DEFAULT NULL,
  `clazz` varchar(255) DEFAULT NULL,
  `FILES` varchar(255) DEFAULT NULL,
  `linenumber` varchar(32) DEFAULT NULL,
  `method` varchar(32) DEFAULT NULL,
  `startid` varchar(32) DEFAULT NULL,
  `errorinfo` text,
  `triggerwarning` varchar(32) DEFAULT NULL,
  `triggertime` varchar(32) DEFAULT NULL,
  `triggertimes` int(11) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `userid` varchar(32) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL,
  `logtime` varchar(32) DEFAULT NULL,
  `ipaddr` varchar(255) DEFAULT NULL,
  `port` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_log_systemout
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_logicdatabase`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_logicdatabase`;
CREATE TABLE `rivu5_logicdatabase` (
  `ID` varchar(50) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `EXPRESS` longtext,
  `MEMO` varchar(255) DEFAULT NULL,
  `MAPPINGTYPE` varchar(255) DEFAULT NULL,
  `PROPERTYNAME` longtext,
  `VALUEOPERATOR` varchar(255) DEFAULT NULL,
  `CATEGORYID` varchar(255) DEFAULT NULL,
  `RULENAME` longtext,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531230` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_logicdatabase
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_measureconditiondata`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_measureconditiondata`;
CREATE TABLE `rivu5_measureconditiondata` (
  `id` varchar(32) NOT NULL,
  `value` double NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `dataid` varchar(255) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  `ruleid` varchar(255) DEFAULT NULL,
  `compute` varchar(255) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `excontent` varchar(255) DEFAULT NULL,
  `datacode` varchar(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `resultType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9A50C810A9C2F11F` (`ruleid`),
  CONSTRAINT `rivu5_measureconditiondata_ibfk_1` FOREIGN KEY (`ruleid`) REFERENCES `rivu5_measureruledata` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_measureconditiondata
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_measureruledata`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_measureruledata`;
CREATE TABLE `rivu5_measureruledata` (
  `id` varchar(32) NOT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `dataid` varchar(255) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  `warningId` varchar(255) DEFAULT NULL,
  `ifStr` varchar(255) DEFAULT NULL,
  `thenStr` varchar(255) DEFAULT NULL,
  `ifvalue` varchar(255) DEFAULT NULL,
  `thenvalue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6417A4FB1715F2F6` (`warningId`),
  CONSTRAINT `rivu5_measureruledata_ibfk_1` FOREIGN KEY (`warningId`) REFERENCES `rivu5_measurewarningmeta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_measureruledata
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_measurewarningmeta`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_measurewarningmeta`;
CREATE TABLE `rivu5_measurewarningmeta` (
  `id` varchar(32) NOT NULL,
  `modelid` varchar(255) DEFAULT NULL,
  `dataid` varchar(255) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  `paramtype` varchar(255) DEFAULT NULL,
  `regionmin` double DEFAULT NULL,
  `regionmax` double DEFAULT NULL,
  `imgclass` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `reportid` varchar(255) DEFAULT NULL,
  `tdstylevar` varchar(255) DEFAULT NULL,
  `html` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_measurewarningmeta
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_metadata`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_metadata`;
CREATE TABLE `rivu5_metadata` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `MIMEID` varchar(255) DEFAULT NULL,
  `INDEXFIELD` varchar(32) DEFAULT NULL,
  `PLUGIN` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531290` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_metadata
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_mime`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_mime`;
CREATE TABLE `rivu5_mime` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `DOCTYPE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `LISTBLOCKTEMPLET` varchar(255) DEFAULT NULL,
  `MIME` varchar(255) DEFAULT NULL,
  `EXTENTIONPOINT` varchar(255) DEFAULT NULL,
  `CONTENTICON` varchar(255) DEFAULT NULL,
  `CONTENTNAME` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531330` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_mime
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_mindmap_nodes`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_mindmap_nodes`;
CREATE TABLE `rivu5_mindmap_nodes` (
  `id` varchar(100) NOT NULL,
  `nodeid` varchar(100) DEFAULT NULL,
  `nodename` varchar(100) NOT NULL,
  `nodecode` varchar(100) DEFAULT NULL,
  `nodetype` varchar(100) DEFAULT NULL,
  `typevalue` varchar(100) DEFAULT NULL,
  `displaytype` varchar(100) DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  `hasroot` varchar(5) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `creater` varchar(100) DEFAULT NULL,
  `orgi` varchar(100) NOT NULL,
  `parentid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_mindmap_nodes
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_mindmaps`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_mindmaps`;
CREATE TABLE `rivu5_mindmaps` (
  `id` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `orgi` varchar(100) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `projecttypeid` varchar(100) DEFAULT NULL,
  `creater` varchar(100) DEFAULT NULL,
  `creatertime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_mindmaps
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_modelcontainer`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_modelcontainer`;
CREATE TABLE `rivu5_modelcontainer` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `TITLE` varchar(100) DEFAULT NULL,
  `ICONSTR` varchar(100) DEFAULT NULL,
  `STYLESTR` varchar(255) DEFAULT NULL,
  `FORMSTYLE` varchar(32) DEFAULT NULL,
  `USEFORM` smallint(6) DEFAULT NULL,
  `FORMACTION` varchar(255) DEFAULT NULL,
  `FORMASUBMITYPE` varchar(32) DEFAULT NULL,
  `FORMTOGGLE` varchar(32) DEFAULT NULL,
  `FROMDATAACTION` varchar(255) DEFAULT NULL,
  `REPORTID` varchar(32) DEFAULT NULL,
  `MODELCOLS` varchar(32) DEFAULT NULL,
  `DIVID` varchar(32) DEFAULT NULL,
  `FORMTARGET` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531360` (`ID`),
  KEY `FK20733FA1CE4E2285` (`REPORTID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_modelcontainer
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_monitoraccessstatistics`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_monitoraccessstatistics`;
CREATE TABLE `rivu5_monitoraccessstatistics` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `PUBLISHTYPE` varchar(255) DEFAULT NULL,
  `PUBLISHSTATUS` varchar(255) DEFAULT NULL,
  `REPORTTYPE` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `TOTALPRODUCTS` bigint(255) DEFAULT NULL,
  `MINPRODUCTS` int(11) DEFAULT NULL,
  `MAXPRODUCTS` int(11) DEFAULT NULL,
  `AVGACCESSTIME` bigint(255) DEFAULT NULL,
  `ACCESSERRORNUM` int(11) DEFAULT NULL,
  `CALCULATENUM` int(11) DEFAULT NULL,
  `MODELDBNUM` int(11) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `REPORTMS1` int(11) DEFAULT NULL,
  `REPORTMS2` int(11) DEFAULT NULL,
  `REPORTMS3` int(11) DEFAULT NULL,
  `REPORTS1` int(11) DEFAULT NULL,
  `REPORTS2` int(11) DEFAULT NULL,
  `REPORTS3` int(11) DEFAULT NULL,
  `REPORTS4` int(11) DEFAULT NULL,
  `REPORTS5` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_monitoraccessstatistics
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_netfile`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_netfile`;
CREATE TABLE `rivu5_netfile` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `DOCTYPE` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `FILEPATH` varchar(255) DEFAULT NULL,
  `LASTCRAWL` varchar(255) DEFAULT NULL,
  `SUBDIR` varchar(255) DEFAULT NULL,
  `PARSEPARAM` varchar(255) DEFAULT NULL,
  `NATIVEXML` varchar(255) DEFAULT NULL,
  `LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CREATEUSER` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `LISTBLOCKTEMPLET` varchar(255) DEFAULT NULL,
  `ACCOUNT` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531390` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_netfile
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_organ`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_organ`;
CREATE TABLE `rivu5_organ` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `CREATER` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `FIELD4` varchar(255) DEFAULT NULL,
  `FIELD5` varchar(255) DEFAULT NULL,
  `FIELD6` varchar(255) DEFAULT NULL,
  `FIELD7` varchar(255) DEFAULT NULL,
  `FIELD8` varchar(255) DEFAULT NULL,
  `FIELD9` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_organ
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_pagechild`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_pagechild`;
CREATE TABLE `rivu5_pagechild` (
  `id` varchar(32) NOT NULL,
  `incviewid` varchar(32) DEFAULT NULL,
  `incviewname` varchar(255) DEFAULT NULL,
  `pageviewid` varchar(32) DEFAULT NULL,
  `pageviewname` varchar(255) DEFAULT NULL,
  `pageviewtext` longtext,
  `defaulttemplate` varchar(32) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_pagechild
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_pagedataview`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_pagedataview`;
CREATE TABLE `rivu5_pagedataview` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `pageview` varchar(32) DEFAULT NULL,
  `dataview` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK94370F474DBD1D1C` (`dataview`),
  CONSTRAINT `rivu5_pagedataview_ibfk_1` FOREIGN KEY (`dataview`) REFERENCES `rivu5_datatableview` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_pagedataview
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_pagetemplate`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_pagetemplate`;
CREATE TABLE `rivu5_pagetemplate` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `parentid` varchar(32) DEFAULT NULL,
  `templatetext` longtext,
  `datamodel` varchar(32) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_pagetemplate
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_pagetype`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_pagetype`;
CREATE TABLE `rivu5_pagetype` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `datamodel` varchar(32) DEFAULT NULL,
  `incview` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `parentid` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_pagetype
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_pageview`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_pageview`;
CREATE TABLE `rivu5_pageview` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `datamodel` varchar(32) DEFAULT NULL,
  `incview` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `typeid` varchar(255) DEFAULT NULL,
  `pagecode` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `viewid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_pageview
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_payment`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_payment`;
CREATE TABLE `rivu5_payment` (
  `ID` varchar(32) NOT NULL,
  `TID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `RESULT` int(11) DEFAULT NULL,
  `SOURCEID` varchar(32) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `DEMSUM` double DEFAULT NULL,
  `ACCOUNTOTAL` double DEFAULT NULL,
  `PAYMENTYPE` varchar(32) DEFAULT NULL,
  `MESSAGE` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SPARE` varchar(255) DEFAULT NULL,
  `SPARE1` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531420` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_payment
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_pluginparam`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_pluginparam`;
CREATE TABLE `rivu5_pluginparam` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `TPID` varchar(255) DEFAULT NULL,
  `PLUGINID` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531510` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_pluginparam
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_pop3`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_pop3`;
CREATE TABLE `rivu5_pop3` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `DOCTYPE` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `LASTCRAWL` varchar(255) DEFAULT NULL,
  `PARSEPARAM` varchar(255) DEFAULT NULL,
  `NATIVEXML` varchar(255) DEFAULT NULL,
  `LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CREATEUSER` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `LISTBLOCKTEMPLET` varchar(255) DEFAULT NULL,
  `ACCOUNT` varchar(255) DEFAULT NULL,
  `POPSERVER` varchar(255) DEFAULT NULL,
  `ATTACHMENT` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531580` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_pop3
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_project`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_project`;
CREATE TABLE `rivu5_project` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `contexturl` longtext,
  `multilang` smallint(6) NOT NULL,
  `langtype` varchar(255) DEFAULT NULL,
  `packagename` varchar(255) DEFAULT NULL,
  `dbid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_project
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_projecttype`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_projecttype`;
CREATE TABLE `rivu5_projecttype` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `viewtype` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `viewtypeid` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `parentid` varchar(255) DEFAULT NULL,
  `jsontext` varchar(255) DEFAULT NULL,
  `showtype` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_projecttype
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_projecttypechild`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_projecttypechild`;
CREATE TABLE `rivu5_projecttypechild` (
  `id` varchar(32) NOT NULL,
  `typeid` varchar(255) DEFAULT NULL,
  `viewtypeid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_projecttypechild
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_publishedcube`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_publishedcube`;
CREATE TABLE `rivu5_publishedcube` (
  `ID` varchar(255) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DB` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `MPOSLEFT` varchar(32) DEFAULT NULL,
  `MPOSTOP` varchar(32) DEFAULT NULL,
  `TYPEID` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `DSTYPE` varchar(255) DEFAULT NULL,
  `MODELTYPE` varchar(32) DEFAULT NULL,
  `createdata` varchar(32) DEFAULT NULL,
  `startindex` int(11) DEFAULT NULL,
  `startdate` datetime DEFAULT NULL,
  `dataid` varchar(32) DEFAULT NULL,
  `dataflag` varchar(255) DEFAULT NULL,
  `DATAVERSION` int(11) DEFAULT NULL,
  `CREATER` varchar(255) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `CUBECONTENT` longtext,
  `DBID` varchar(32) DEFAULT NULL,
  `DICLOCATION` varchar(255) DEFAULT NULL,
  `USEREMAIL` varchar(255) DEFAULT NULL,
  UNIQUE KEY `SQL121227155530220` (`ID`),
  KEY `FKD104A39E2DA4DEB5` (`DB`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_publishedcube
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_publishedreport`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_publishedreport`;
CREATE TABLE `rivu5_publishedreport` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `REPORTTYPE` varchar(32) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `OBJECTCOUNT` int(11) DEFAULT NULL,
  `DICID` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DESCRIPTION` longtext,
  `HTML` longtext,
  `REPORTPACKAGE` varchar(255) DEFAULT NULL,
  `USEACL` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `rolename` text,
  `userid` text,
  `blacklist` text,
  `REPORTCONTENT` longtext,
  `reportmodel` varchar(32) DEFAULT NULL,
  `updatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `creater` varchar(255) DEFAULT NULL,
  `reportversion` int(11) DEFAULT NULL,
  `publishedtype` varchar(32) DEFAULT NULL,
  `tabtype` varchar(32) DEFAULT NULL,
  `USERNAME` varchar(32) DEFAULT NULL,
  `USEREMAIL` varchar(255) DEFAULT NULL,
  `CACHE` smallint(6) DEFAULT NULL,
  `EXTPARAM` varchar(255) DEFAULT NULL,
  `TARGETREPORT` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155529680` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_publishedreport
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_reportfilter`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_reportfilter`;
CREATE TABLE `rivu5_reportfilter` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `dataid` varchar(32) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  `modelid` varchar(32) DEFAULT NULL,
  `reportid` varchar(32) DEFAULT NULL,
  `contype` varchar(32) DEFAULT NULL,
  `filtertype` varchar(32) DEFAULT NULL,
  `formatstr` varchar(255) DEFAULT NULL,
  `convalue` varchar(255) DEFAULT NULL,
  `userdefvalue` text,
  `valuefiltertype` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `orgi` varchar(32) DEFAULT NULL,
  `content` text,
  `valuestr` varchar(255) DEFAULT NULL,
  `filterprefix` varchar(255) DEFAULT NULL,
  `filtersuffix` varchar(255) DEFAULT NULL,
  `modeltype` varchar(32) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `funtype` varchar(32) DEFAULT NULL,
  `measureid` varchar(32) DEFAULT NULL,
  `valuecompare` varchar(32) DEFAULT NULL,
  `defaultvalue` text,
  `comparetype` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_reportfilter
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_reportoperations`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_reportoperations`;
CREATE TABLE `rivu5_reportoperations` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `reporttype` varchar(32) DEFAULT NULL,
  `reportid` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `orgi` varchar(32) DEFAULT NULL,
  `objectcount` int(11) DEFAULT NULL,
  `dicid` varchar(32) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `description` text,
  `html` text,
  `reportpackage` varchar(255) DEFAULT NULL,
  `useacl` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `rolename` text,
  `userid` varchar(32) DEFAULT NULL,
  `blacklist` text,
  `reportcontent` text,
  `reportmodel` varchar(32) DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  `creater` varchar(255) DEFAULT NULL,
  `reportversion` int(11) DEFAULT NULL,
  `publishedtype` varchar(32) DEFAULT NULL,
  `tabtype` varchar(32) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL,
  `useremail` varchar(255) DEFAULT NULL,
  `cache` int(11) DEFAULT NULL,
  `extparam` varchar(255) DEFAULT NULL,
  `targetreport` varchar(32) DEFAULT NULL,
  `flowid` double(32,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_reportoperations
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_role`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_role`;
CREATE TABLE `rivu5_role` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `ROLEMAP` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL,
  `CREATER` varchar(32) DEFAULT NULL,
  `DESCRIPTION` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531620` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_role
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_role_group`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_role_group`;
CREATE TABLE `rivu5_role_group` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) DEFAULT NULL,
  `parentid` varchar(32) DEFAULT NULL,
  `creater` varchar(32) DEFAULT NULL,
  `orgi` varchar(32) DEFAULT NULL,
  `createtime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_role_group
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_rsasetting`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_rsasetting`;
CREATE TABLE `rivu5_rsasetting` (
  `ID` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `TID` varchar(255) DEFAULT NULL,
  `PUBLICKEY` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_rsasetting
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_rule`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_rule`;
CREATE TABLE `rivu5_rule` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `SEARCHPROJECTID` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531660` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_rule
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_searchproject`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_searchproject`;
CREATE TABLE `rivu5_searchproject` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `VERSION` varchar(255) DEFAULT NULL,
  `AUTHOR` varchar(255) DEFAULT NULL,
  `THEMEURL` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `RULES` varchar(255) DEFAULT NULL,
  `AUTH` smallint(6) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USERID` varchar(255) DEFAULT NULL,
  `FILTERQ` longtext,
  `QUERYRESULTSET` varchar(255) DEFAULT NULL,
  `ADMINUSER` varchar(255) DEFAULT NULL,
  `DEFAULTPROJECT` smallint(6) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531710` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_searchproject
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_searchresulttemplet`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_searchresulttemplet`;
CREATE TABLE `rivu5_searchresulttemplet` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `DESCRIPTION` longtext,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USERID` varchar(255) DEFAULT NULL,
  `TEMPLETTEXT` longtext,
  `TEMPLETTYPE` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `ICONSTR` varchar(255) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `ORDERINDEX` int(11) DEFAULT NULL,
  `TYPEID` varchar(32) DEFAULT NULL,
  `SELDATA` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531770` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_searchresulttemplet
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_searchsetting`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_searchsetting`;
CREATE TABLE `rivu5_searchsetting` (
  `ID` varchar(32) NOT NULL,
  `STARTDELAY` int(11) NOT NULL,
  `MOREPARAM` varchar(255) DEFAULT NULL,
  `HIGHTLIGHT` varchar(255) DEFAULT NULL,
  `FIELDMAXLENGTH` varchar(255) DEFAULT NULL,
  `HLSNIPPETS` varchar(255) DEFAULT NULL,
  `HLHTMLSTART` varchar(255) DEFAULT NULL,
  `HLHTMLEND` varchar(255) DEFAULT NULL,
  `FACET` varchar(255) DEFAULT NULL,
  `DEFAULTPAGESIZE` varchar(255) DEFAULT NULL,
  `DEFAULTOPERATOR` varchar(255) DEFAULT NULL,
  `DATEFORMAT` varchar(255) DEFAULT NULL,
  `UPLOADIMAGESIZE` int(11) NOT NULL,
  `UPLOADJARSIZE` int(11) NOT NULL,
  `DEFAULTUPLOADIMAGEPATH` varchar(255) DEFAULT NULL,
  `DEFAULTUPLOADJARPATH` varchar(255) DEFAULT NULL,
  `DEFAULTTEMPLETPATH` varchar(255) DEFAULT NULL,
  `FACTTEMPLET` varchar(255) DEFAULT NULL,
  `RELATEDTEMPLET` varchar(255) DEFAULT NULL,
  `FACTSYSTEMFIELD` varchar(255) DEFAULT NULL,
  `SORTSYSTEMFIELD` varchar(255) DEFAULT NULL,
  `SORTTEMPLET` varchar(255) DEFAULT NULL,
  `SPELLCHECKTEMPLET` varchar(255) DEFAULT NULL,
  `SPELLCHECK` varchar(255) DEFAULT NULL,
  `SUGGESTAREATEMPLET` varchar(255) DEFAULT NULL,
  `SITENAME` varchar(255) DEFAULT NULL,
  `LOGONIMAGEPATH` varchar(255) DEFAULT NULL,
  `SITEDESC` varchar(255) DEFAULT NULL,
  `COPYRIGHT` varchar(255) DEFAULT NULL,
  `USERWELCOMETEMPLET` varchar(255) DEFAULT NULL,
  `SEARCHBOXTEMPLET` varchar(255) DEFAULT NULL,
  `FACTSEARCHQUERY` longtext,
  `FACTDATE` longtext,
  `FACETDATEGAP` varchar(255) DEFAULT NULL,
  `SITEEMAIL` varchar(255) DEFAULT NULL,
  `SEARCHTYPE` varchar(255) DEFAULT NULL,
  `DEFAULTTERMSFIELD` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `LOCALSERVER` varchar(255) DEFAULT NULL,
  `R3CLOUDSERVER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531810` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_searchsetting
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_secureconfigure`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_secureconfigure`;
CREATE TABLE `rivu5_secureconfigure` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `SECURELEVEL` varchar(255) DEFAULT NULL,
  `ENCRYPTION` smallint(6) NOT NULL,
  `SECSTATUS` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531860` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_secureconfigure
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_selffiltercondtion`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_selffiltercondtion`;
CREATE TABLE `rivu5_selffiltercondtion` (
  `id` varchar(32) NOT NULL,
  `modelid` varchar(32) DEFAULT NULL,
  `levelid` varchar(32) DEFAULT NULL,
  `levelname` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `comparision` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_selffiltercondtion
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_selfreportchoicelevel`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_selfreportchoicelevel`;
CREATE TABLE `rivu5_selfreportchoicelevel` (
  `ID` varchar(32) NOT NULL DEFAULT '',
  `modelid` varchar(255) DEFAULT NULL,
  `levelid` varchar(255) DEFAULT NULL,
  `choicemembers` blob,
  `ORGI` varchar(255) DEFAULT NULL,
  `memberprefix` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_selfreportchoicelevel
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_server`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_server`;
CREATE TABLE `rivu5_server` (
  `ID` varchar(32) NOT NULL,
  `TID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PLATFORMSERVER` varchar(255) DEFAULT NULL,
  `TENANTSERVER` varchar(255) DEFAULT NULL,
  `INSTANCESERVER` varchar(255) DEFAULT NULL,
  `CRAWLSERVER` varchar(255) DEFAULT NULL,
  `INDEXSERVER` varchar(255) DEFAULT NULL,
  `SEARCHSERVER` varchar(255) DEFAULT NULL,
  `ZKHOST` varchar(255) DEFAULT NULL,
  `MQSERVER` varchar(255) DEFAULT NULL,
  `UPDATESERVER` varchar(255) DEFAULT NULL,
  `OTHERSERVER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531900` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_server
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_site`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_site`;
CREATE TABLE `rivu5_site` (
  `ID` varchar(32) NOT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `DEFAULTERRORMSGNUM` int(11) NOT NULL,
  `DEFAULTTHREADS` int(11) NOT NULL,
  `OPERATIONMODE` int(11) NOT NULL,
  `MAXTHREADS` int(11) NOT NULL,
  `COUNTTHREADS` int(11) NOT NULL,
  `CACHEREC` int(11) NOT NULL,
  `CACHEMEMORY` int(11) NOT NULL,
  `MAXCACHEMEMORY` int(11) NOT NULL,
  `SEARCHINDEX` varchar(255) DEFAULT NULL,
  `UNIQUEKEY` varchar(255) DEFAULT NULL,
  `HLFRAGMENTERLENGTH` int(11) NOT NULL,
  `MAXRUNNINGTASK` int(11) NOT NULL,
  `DATABASEMAXTHREADS` int(11) NOT NULL,
  `SMTPSERVER` varchar(255) DEFAULT NULL,
  `SMTPUSER` varchar(255) DEFAULT NULL,
  `SMTPPASSWORD` varchar(255) DEFAULT NULL,
  `MAILFROM` varchar(255) DEFAULT NULL,
  `SECLEV` varchar(50) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531970` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_site
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_tabledir`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_tabledir`;
CREATE TABLE `rivu5_tabledir` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(32) DEFAULT NULL,
  `TITLE` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `STATUS` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `CREATER` varchar(255) DEFAULT NULL,
  `DATABASEID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530400` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_tabledir
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_tableproperties`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_tableproperties`;
CREATE TABLE `rivu5_tableproperties` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `FIELDNAME` varchar(255) DEFAULT NULL,
  `DATATYPECODE` int(11) NOT NULL,
  `DATATYPENAME` varchar(255) DEFAULT NULL,
  `DBTABLEID` varchar(255) DEFAULT NULL,
  `INDEXDATATYPE` varchar(255) DEFAULT NULL,
  `PK` smallint(6) DEFAULT NULL,
  `MODITS` smallint(6) DEFAULT NULL,
  `INDEXFIELD` varchar(32) DEFAULT NULL,
  `PLUGIN` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `FKTABLE` varchar(32) DEFAULT NULL,
  `FKPROPERTY` varchar(32) DEFAULT NULL,
  `TABLENAME` varchar(255) DEFAULT NULL,
  `viewtype` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL130112140848940` (`ID`),
  KEY `FKF8D74787854BC62` (`DBTABLEID`),
  KEY `FKF8D747811BE44FF` (`FKPROPERTY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_tableproperties
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_tabletask`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_tabletask`;
CREATE TABLE `rivu5_tabletask` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `TABLEDIRID` varchar(255) DEFAULT NULL,
  `DBID` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` text,
  `LISTBLOCKTEMPLET` text,
  `TABLENAME` varchar(255) DEFAULT NULL,
  `TABLETYPE` varchar(255) DEFAULT NULL,
  `STARTINDEX` int(11) NOT NULL,
  `UPDATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATETIMENUMBER` bigint(20) NOT NULL,
  `DATASQL` longtext,
  `DATABASETASK` varchar(32) DEFAULT NULL,
  `DRIVERPLUGIN` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL130112140849020` (`ID`),
  KEY `FK31B2348A1258E3B7` (`DATABASETASK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_tabletask
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_taskplanconfigure`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_taskplanconfigure`;
CREATE TABLE `rivu5_taskplanconfigure` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PLANTYPE` varchar(255) DEFAULT NULL,
  `RIT` varchar(255) DEFAULT NULL,
  `RUNONDATE` varchar(255) DEFAULT NULL,
  `RHOURS` varchar(255) DEFAULT NULL,
  `RMIN` varchar(255) DEFAULT NULL,
  `RSEC` varchar(255) DEFAULT NULL,
  `RWEEK` varchar(255) DEFAULT NULL,
  `RMONTH` varchar(255) DEFAULT NULL,
  `BEGINTIME` varchar(255) DEFAULT NULL,
  `RPT` varchar(255) DEFAULT NULL,
  `REPEATTERVAL` varchar(255) DEFAULT NULL,
  `CRT` varchar(255) DEFAULT NULL,
  `CRONDSTR` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155532100` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_taskplanconfigure
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_templetmapping`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_templetmapping`;
CREATE TABLE `rivu5_templetmapping` (
  `ID` varchar(32) NOT NULL,
  `TASKID` varchar(255) DEFAULT NULL,
  `LISTBLOCKID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLETID` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155532140` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_templetmapping
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_tenant`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_tenant`;
CREATE TABLE `rivu5_tenant` (
  `ID` varchar(32) NOT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `PASSWORD` varchar(32) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `SOURCE` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `EDITTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CTYPE` int(11) DEFAULT NULL,
  `CCODE` varchar(32) DEFAULT NULL,
  `PHONE` varchar(32) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `POSTCODE` varchar(8) DEFAULT NULL,
  `EMAIL` varchar(64) DEFAULT NULL,
  `INTRO` longtext,
  `USERID` varchar(32) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `SPARE0` varchar(255) DEFAULT NULL,
  `SPARE1` varchar(255) DEFAULT NULL,
  `STATUS` varchar(32) DEFAULT NULL,
  `SECKEY` varchar(32) DEFAULT NULL,
  `SITE` varchar(255) DEFAULT NULL,
  `LICENCE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155532160` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_tenant
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_typecategory`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_typecategory`;
CREATE TABLE `rivu5_typecategory` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `TITLE` varchar(100) DEFAULT NULL,
  `CODE` varchar(100) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CTYPE` varchar(32) DEFAULT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `MEMO` varchar(32) DEFAULT NULL,
  `ICONSTR` varchar(255) DEFAULT NULL,
  `ICONSKIN` varchar(32) DEFAULT NULL,
  `CATETYPE` varchar(32) DEFAULT NULL,
  `CREATER` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155532210` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_typecategory
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_user`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_user`;
CREATE TABLE `rivu5_user` (
  `ID` varchar(32) NOT NULL,
  `LANGUAGE` varchar(255) DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `FIRSTNAME` varchar(255) DEFAULT NULL,
  `MIDNAME` varchar(255) DEFAULT NULL,
  `LASTNAME` varchar(255) DEFAULT NULL,
  `JOBTITLE` varchar(255) DEFAULT NULL,
  `DEPARTMENT` varchar(255) DEFAULT NULL,
  `GENDER` varchar(255) DEFAULT NULL,
  `BIRTHDAY` varchar(255) DEFAULT NULL,
  `NICKNAME` varchar(255) DEFAULT NULL,
  `USERTYPE` varchar(255) DEFAULT NULL,
  `RULENAME` varchar(255) DEFAULT NULL,
  `SEARCHPROJECTID` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CREATER` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `MEMO` varchar(255) DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `ORGAN` varchar(32) DEFAULT NULL,
  `MOBILE` varchar(32) DEFAULT NULL,
  `passupdatetime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155532250` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_user
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_userole`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_userole`;
CREATE TABLE `rivu5_userole` (
  `ID` varchar(32) DEFAULT NULL,
  `ROLEID` varchar(32) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_userole
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_userorgan`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_userorgan`;
CREATE TABLE `rivu5_userorgan` (
  `ID` varchar(32) DEFAULT NULL,
  `ORGANID` varchar(32) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_userorgan
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_vouchers`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_vouchers`;
CREATE TABLE `rivu5_vouchers` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ORGI` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `USERID` varchar(255) DEFAULT NULL,
  `SIGNCODE` varchar(255) DEFAULT NULL,
  `DENSUM` double DEFAULT NULL,
  `VSTATUS` int(11) DEFAULT NULL,
  `TENANT` varchar(32) DEFAULT NULL,
  `STARTTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ENDTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155532290` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_vouchers
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_wsbusiness`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_wsbusiness`;
CREATE TABLE `rivu5_wsbusiness` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `parentid` varchar(32) DEFAULT NULL,
  `wstype` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_wsbusiness
-- ----------------------------


INSERT INTO `rivu5_user` VALUES ('4028d481428add1801428ae6be120003', null, 'admin', 'a5092280546414436147', '5', 'panjunfeng@rivues.com', null, null, null, '123', '运维用户', null, '', '管理员', '0', '4028d481428abd8001428acb2b830002', null, 'rivues', null, '2015-11-13 12:31:44', '', '2015-12-23 08:58:46', null, '18025950907', '2015-11-13 12:32:54');