package com.rivues.module.datamodel.web.handler;

import java.util.ArrayList;
import java.util.List;

public class DefaultParam implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5996690826927221375L;
	private String format ;
	private boolean userdate = false ;
	private List<DefaultValue> values = new ArrayList();
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public List<DefaultValue> getValues() {
		return values;
	}
	public void setValues(List<DefaultValue> values) {
		this.values = values;
	}
	public boolean isUserdate() {
		return userdate;
	}
	public void setUserdate(boolean userdate) {
		this.userdate = userdate;
	}
}
