package com.rivues.module.design.web.handler.query;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rivues.module.design.web.handler.DesignController;
@Controller  
@RequestMapping("/{orgi}/design/query") 
public class QueryDesignController extends DesignController{ 
	/**
	 * QueryDesign 和 CustomDesign 公用相同的 方法，已合并为同一个， PlayDesign 同样和 QueryDesign一个方法，目前，涉及到权限和数据缓存问题，暂时保持独立
	 */
	public QueryDesignController(){
		super("query") ;
	}
} 