package com.rivues.module.platform.web.model;

public class RenameCol implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1072808550826849938L;
	private String type ="rename" ;
	private String id ;
	private String formatstr  =  "###" ;
	private String name ;
	private boolean group ;
	private String grouptype ;
	private String groupdim ;
	private String express ;
	private String style ;
	private int rowindex ;
	public RenameCol(){}
	public RenameCol(String name){
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getExpress() {
		return express;
	}
	public void setExpress(String express) {
		this.express = express;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
//		this.type = type;
	}
	public String getFormatstr() {
		return formatstr;
	}
	public void setFormatstr(String formatstr) {
		this.formatstr = formatstr;
	}
	public boolean isGroup() {
		return group;
	}
	public void setGroup(boolean group) {
		this.group = group;
	}
	public String getGrouptype() {
		return grouptype;
	}
	public void setGrouptype(String grouptype) {
		this.grouptype = grouptype;
	}
	public String getGroupdim() {
		return groupdim;
	}
	public void setGroupdim(String groupdim) {
		this.groupdim = groupdim;
	}
	public int getRowindex() {
		return rowindex;
	}
	public void setRowindex(int rowindex) {
		this.rowindex = rowindex;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
}
