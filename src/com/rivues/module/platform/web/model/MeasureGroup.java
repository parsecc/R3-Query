package com.rivues.module.platform.web.model;

import java.io.Serializable;
import java.util.List;

public class MeasureGroup implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id = String.valueOf(System.currentTimeMillis());
	private String name;
	private List<MeasureGroupData> measures;
	private String mdx;
	private String grouptype;//mdx 和measure两种类型
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<MeasureGroupData> getMeasures() {
		return measures;
	}
	public void setMeasures(List<MeasureGroupData> measures) {
		this.measures = measures;
	}
	public String getMdx() {
		return mdx;
	}
	public void setMdx(String mdx) {
		this.mdx = mdx;
	}
	public String getGrouptype() {
		return grouptype;
	}
	public void setGrouptype(String grouptype) {
		this.grouptype = grouptype;
	}
	
	
	
	

}
