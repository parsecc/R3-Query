package com.rivues.module.platform.web.model.chart;

public class LayoutStyle implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -848694801985457916L;
	private String id ;
	private String cellwidth ;	//合并单元格后计算的宽度 ， 50% 或 50px ;
	private String cellheight ;//合并单元格后计算的高度 ， 50% 或 50px ;
	private int colspan = 1;
	private int rowspan = 1;
	private String width ;
	private String height ;
	private String widthdef = "auto"; //	auto OR custom
	private String heightdef = "auto";//	auto OR custom
	private String padding_left = "3";
	private String padding_right = "2";
	private String padding_top  = "2" ;
	private String padding_bottom = "0";
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getColspan() {
		return colspan;
	}
	public void setColspan(int colspan) {
		this.colspan = colspan;
	}
	public int getRowspan() {
		return rowspan;
	}
	public void setRowspan(int rowspan) {
		this.rowspan = rowspan;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWidthdef() {
		return widthdef;
	}
	public void setWidthdef(String widthdef) {
		this.widthdef = widthdef;
	}
	public String getHeightdef() {
		return heightdef;
	}
	public void setHeightdef(String heightdef) {
		this.heightdef = heightdef;
	}
	public String getPadding_left() {
		return padding_left;
	}
	public void setPadding_left(String padding_left) {
		this.padding_left = padding_left;
	}
	public String getPadding_right() {
		return padding_right;
	}
	public void setPadding_right(String padding_right) {
		this.padding_right = padding_right;
	}
	public String getPadding_top() {
		return padding_top;
	}
	public void setPadding_top(String padding_top) {
		this.padding_top = padding_top;
	}
	public String getPadding_bottom() {
		return padding_bottom;
	}
	public void setPadding_bottom(String padding_bottom) {
		this.padding_bottom = padding_bottom;
	}
	public String getCellwidth() {
		return cellwidth;
	}
	public void setCellwidth(String cellwidth) {
		this.cellwidth = cellwidth;
	}
	public String getPadding(){
		return new StringBuffer().append("padding:").append(padding_top).append("px ").append(padding_right).append("px ").append(padding_bottom).append("px ").append(padding_left).append("px;").toString() ;
	}
	public String getCellheight() {
		return cellheight;
	}
	public void setCellheight(String cellheight) {
		this.cellheight = cellheight;
	}
}
