package com.rivues.module.platform.web.model.chart;

public class LabelData implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3284889273177912475L;
	private String label ;
	private String value ;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}