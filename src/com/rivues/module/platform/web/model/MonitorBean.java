package com.rivues.module.platform.web.model;

import java.util.Date;

/**
 * 系统监控的超类
 * @author admin
 *
 */
public abstract class MonitorBean implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5724282709215492963L;

	/**
	 * 预警服务
	 * @param infoData
	 */
	public void warning(Object infoData){}
	
	public abstract Date getCreatedate();
	
	public abstract void setTriggerwarning(String warning) ;
	
	public abstract void setTriggertime(String time) ;
}
