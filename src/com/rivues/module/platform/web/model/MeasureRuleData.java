package com.rivues.module.platform.web.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "rivu5_measureruledata")
@org.hibernate.annotations.Proxy(lazy = false)
public class MeasureRuleData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String warningId;
	private String dataid;//
	private String dataname ; 	//指标名称显示报表的名称  判断报表列数据是否为预警数据 
	private String orgi ;
	private String ifStr;//页面显示
	private String thenStr;
	private String ifvalue;//未用
	private String thenvalue;//未用
	private Set<MeasureConditionData> rulesData;
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getWarningId() {
		return warningId;
	}
	public void setWarningId(String warningId) {
		this.warningId = warningId;
	}
	public String getDataid() {
		return dataid;
	}
	public void setDataid(String dataid) {
		this.dataid = dataid;
	}
	public String getDataname() {
		return dataname;
	}
	public void setDataname(String dataname) {
		this.dataname = dataname;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getIfStr() {
		return ifStr;
	}
	public void setIfStr(String ifStr) {
		this.ifStr = ifStr;
	}
	public String getThenStr() {
		return thenStr;
	}
	public void setThenStr(String thenStr) {
		this.thenStr = thenStr;
	}
	public String getIfvalue() {
		return ifvalue;
	}
	public void setIfvalue(String ifvalue) {
		this.ifvalue = ifvalue;
	}
	public String getThenvalue() {
		return thenvalue;
	}
	public void setThenvalue(String thenvalue) {
		this.thenvalue = thenvalue;
	}
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "ruleid")
	public Set<MeasureConditionData> getRulesData() {
		return rulesData;
	}
	public void setRulesData(Set<MeasureConditionData> rulesData) {
		this.rulesData = rulesData;
	}
	
	
	
	
	
}
