package com.rivues.module.platform.web.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.rivues.core.RivuDataContext;

@Entity
@Table(name = "rivu5_datadic")
@org.hibernate.annotations.Proxy(lazy = false)
public class DataDic implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2593399616448881368L;
	private String id ;
	private String name ;
	private String code = RivuDataContext.ReportTypeEnum.REPORT.toString();			//变更用途，用于区分仪表盘，目录类型，默认为空
	private String title ;
	private String parentid;
	private String type ;
	private String memo ;
	private String orgi ;
	private String status ;
	private String creater ;
	private String publishedtype ;
	private Date createtime = new Date();
	private Date updatetime ;
	private String description ;
	private String tabtype ;
	private boolean spsearch = true;	//是否支持搜索
	private User user;
	/**
	 * 文件夹类型 public
	 */
	@Transient
	public static final  String TABTYPE_PUB="pub";
	/**
	 * 文件夹类型 private 
	 */
	@Transient
	public static final  String TABTYPE_PRI="pri";
	/**
	 * 权限列表
	 * add by huqi 2014/5/10
	 */
	private List<Auth> authList = new ArrayList<Auth>();
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public String getPublishedtype() {
		return publishedtype;
	}
	public void setPublishedtype(String publishedtype) {
		this.publishedtype = publishedtype;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTabtype() {
		return tabtype;
	}
	public void setTabtype(String tabtype) {
		this.tabtype = tabtype;
	}
	@Transient
	public List<Auth> getAuthList() {
		return authList;
	}
	public void setAuthList(List<Auth> authList) {
		this.authList = authList;
	}
	
	@Transient
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * 
	 * @param code
	 * @return
	 */
	public boolean isAllow(String code , User user){
		boolean allow = false ;
		if(code!=null && this.authList!=null){
			for(Auth auth : this.authList){
				if(auth.getAuthtype()!=null && auth.getAuthtype().indexOf(code)>=0){
					allow = true ;
					break ;
				}
			}
		}
		return (user!=null && user.getUsertype()!=null && user.getUsertype().equals(RivuDataContext.UserTypeEnum.SUPER.getValue())) || (this.tabtype!=null && this.tabtype.equals(RivuDataContext.TabType.PRI.toString())) || (this.creater!=null && user!=null && this.creater.equals(user.getId())) ? true : allow ;
	}
	@Transient
	public boolean isSpsearch() {
		return spsearch;
	}
	public void setSpsearch(boolean spsearch) {
		this.spsearch = spsearch;
	}
}
