package com.rivues.module.platform.web.interceptor;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import com.rivues.module.platform.web.handler.ResponseData;
import com.rivues.util.local.LocalTools;

public class ErrorInterceptorHandler implements org.springframework.web.servlet.HandlerInterceptor{

	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response,
			Object arg2, ModelAndView mv) throws Exception {
		String msgcode = request.getParameter("msgcode");
		if(msgcode!=null){
			mv.addObject("ation_message",LocalTools.getMessage(request.getParameter("msgcode")));
		}
		
		
	}
	@Override
	public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2) throws Exception {
		return true;
	}
}
