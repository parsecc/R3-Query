package com.rivues.module.manage.web.handler.role;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.management.relation.RoleResult;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.handler.Handler;
import com.rivues.module.platform.web.handler.RequestData;
import com.rivues.module.platform.web.handler.ResponseData;
import com.rivues.module.platform.web.interceptor.LogIntercreptorHandler;
import com.rivues.module.platform.web.model.Auth;
import com.rivues.module.platform.web.model.ConfigureParam;
import com.rivues.module.platform.web.model.Organ;
import com.rivues.module.platform.web.model.Role;
import com.rivues.module.platform.web.model.RoleGroup;
import com.rivues.module.platform.web.model.UserOrgan;
import com.rivues.module.platform.web.model.UserRole;
  
@Controller  
@RequestMapping("/{orgi}/manage/role")  
public class RoleController extends Handler{  
	private final Logger log = LoggerFactory.getLogger(LogIntercreptorHandler.class); 
    @RequestMapping(value="/index" , name="index" , type="manage",subtype="role")
    public ModelAndView index(HttpServletRequest request , @PathVariable String orgi) throws Exception{  
    	ModelAndView modelView = request(super.createManageTempletResponse("/pages/manage/role/index") , orgi) ;
    	modelView.addObject("roleid","0");
    	modelView.addObject("c","0");
    	modelView.addObject("groupid","0");
    	modelView.addObject("useroleList", super.getService().findAllByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("roleid","0")))) ;
    	modelView.addObject("groupList",super.getService().findAllByCriteria(DetachedCriteria.forClass(RoleGroup.class).add(Restrictions.eq("orgi",orgi))));
    	modelView.addObject("roleList", super.getService().findAllByCriteria(DetachedCriteria.forClass(Role.class).add(Restrictions.eq("orgi",orgi)))) ;
    	
    	return modelView;
    }
    
    @RequestMapping(value="/list/{roleid}/{groupid}" , name="index" , type="manage",subtype="role")
    public ModelAndView index(HttpServletRequest request , @PathVariable String orgi, @PathVariable String roleid,@PathVariable String groupid,@ModelAttribute RequestData data) throws Exception{  
    	ModelAndView modelView = request(super.createManageTempletResponse("/pages/manage/role/index") , orgi) ;
    	modelView.addObject("roleid",roleid);
    	if("0".equals(roleid)){
    		List<UserRole> urList = new ArrayList<UserRole>();
			List<Role> roleList =  super.getService().findAllByCriteria(DetachedCriteria.forClass(Role.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("groupid",groupid)));
			
//			if(roleList != null && roleList.size()>0) {
//				for(Role role : roleList){
//					urList.addAll(super.getService().findAllByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("roleid",role.getId()))));
//				}
//			}
    		modelView.addObject("useroleList", urList);
    	} else {
    		DetachedCriteria criteria = DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("roleid",roleid));
			modelView = page(request, orgi, modelView, criteria, "beginNum", "endNum", "useroleList");
//	    	modelView.addObject("useroleList", 
//	    			super.getService().findAllByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("roleid",roleid)))) ;
	    	Role role = (Role)super.getService().getIObjectByPK(Role.class, roleid);
	    	groupid = role.getGroupid();
    	}
    	modelView.addObject("groupList",super.getService().findAllByCriteria(DetachedCriteria.forClass(RoleGroup.class).add(Restrictions.eq("orgi",orgi))));
    	modelView.addObject("roleList", super.getService().findAllByCriteria(DetachedCriteria.forClass(Role.class).add(Restrictions.eq("orgi",orgi)))) ;
    	modelView.addObject("groupid",groupid);
    
    	return modelView;
    }
    
    @RequestMapping(value="/rolelevel/{roleid}/{groupid}" , name="rolelevel" , type="manage",subtype="role")
    public ModelAndView rolelevel(HttpServletRequest request , @PathVariable String orgi, @PathVariable String roleid,@PathVariable String groupid, @ModelAttribute RequestData data) throws Exception{  
    	ModelAndView modelView = request(super.createManageTempletResponse("/pages/manage/role/rolelevel") , orgi) ;
    	modelView.addObject("roleid",roleid);
    	modelView.addObject("groupid",groupid);
    	modelView.addObject("useroleList", super.getService().findPageByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("roleid",roleid)))) ;
    	modelView.addObject("roleList", super.getService().findPageByCriteria(DetachedCriteria.forClass(Role.class).add(Restrictions.eq("orgi",orgi)))) ;
    	modelView.addObject("authList", super.getService().findPageByCriteria(DetachedCriteria.forClass(Auth.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("resourcetype",Auth.RESOURCE_TYPE_MENU)).add(Restrictions.eq("ownerid",roleid)))) ;
    	
    	//查询根
    	List<ConfigureParam> datas = super.getService().findAllByCriteria(
    			DetachedCriteria.forClass(ConfigureParam.class)
    			.add(Restrictions.eq("protype", RivuDataContext.ConfigParamEnum.AUTH.toString()))
    			.add(Restrictions.eq("paramtype", "6"))
    			.add(Restrictions.or(Restrictions.isNull("groupid"),Restrictions.eq("groupid","")))
    			.add(Restrictions.eq("orgi", orgi)));

    	if(datas != null && datas.size() >0){
    		modelView.addObject("datas",datas);
    	}
    	modelView.addObject("type", RivuDataContext.ConfigParamEnum.AUTH.toString());
    	modelView.addObject("propertiesList",
				super.getService().findAllByCriteria(
						DetachedCriteria.forClass(ConfigureParam.class)
								.add(Restrictions.eq("protype", RivuDataContext.ConfigParamEnum.AUTH.toString()))
								.add(Restrictions.eq("orgi", orgi))
								.add(Restrictions.eq("paramtype", "8"))));

    	return modelView;
    }
    
    @RequestMapping(value="/{groupid}/roleadd" , name="roleadd" , type="manage",subtype="role")
    public ModelAndView roleadd(HttpServletRequest request , @PathVariable String orgi,@PathVariable String groupid) throws Exception{
    	ResponseData responseData = new ResponseData("/pages/manage/role/roleadd") ;
    	ModelAndView modelView = request(responseData , orgi);
    	modelView.addObject("groupid",groupid);
    	return modelView;
    }
    
    @RequestMapping(value="/{groupid}/roleauth" , name="roleauth" , type="manage",subtype="roleauth")
    public ModelAndView roleauth(HttpServletRequest request , @PathVariable String orgi,@PathVariable String groupid , @Valid String name , @Valid String value) throws Exception{
    	ResponseData responseData = new ResponseData("/pages/public/success") ;
    	List<Auth> authList = super.getService().findAllByCriteria(
    			DetachedCriteria.forClass(Auth.class)
    			.add(Restrictions.eq("resourcetype", Auth.RESOURCE_TYPE_MENU))
    			.add(Restrictions.eq("dataid", name))
    			.add(Restrictions.eq("ownerid", groupid))
    			.add(Restrictions.eq("orgi", orgi)));
    	if(authList!=null && authList.size()>0){
    		Auth auth = authList.get(0) ;
    		if("false".equals(value)){
    			super.getService().deleteIObject(auth) ;
    		}
    	}else if("true".equals(value)){
    		Auth auth = new Auth();
    		auth.setResourcetype(Auth.RESOURCE_TYPE_MENU) ;
    		auth.setOwnerid(groupid) ;
    		auth.setDataid(name) ;
    		auth.setOrgi(orgi) ;
    		auth.setResourceid(groupid);
     		auth.setOwnertype(Auth.OWNER_TYPE_ROLE) ;
    		super.getService().saveIObject(auth) ;
    	}
    	ModelAndView modelView = request(responseData , orgi);
    	return modelView;
    }
    
    @RequestMapping(value="/roleaddo", method=RequestMethod.POST)  
    public ModelAndView roleaddo(HttpServletRequest request , @PathVariable String orgi, @Valid Role role){  
    	
    	ResponseData responseData = new ResponseData("");
    	int count = 0;
    	count = super.getService().getCountByCriteria(DetachedCriteria.forClass(Role.class).add(Restrictions.eq("name",role.getName())).add(Restrictions.eq("groupid",role.getGroupid())).add(Restrictions.eq("orgi",orgi)));
    	if(count>0){
    		responseData.setPage("redirect:/{orgi}/manage/role/index.html?msgcode=E_MANAGE_10010025");
    	}else{
    		role.setOrgi(orgi);
        	role.setCreatetime(new Date());
        	role.setCreater(super.getUser(request).getId());
        	super.getService().saveIObject(role);
        	responseData.setPage(new StringBuffer().append("redirect:/{orgi}/manage/role/list/").append(role.getId()).append("/").append(role.getGroupid()).append(".html?msgcode=S_MANAGE_10010026").toString());
    	}
        return request(responseData , orgi) ;
    } 
    
    @RequestMapping(value="/roledit/{roleid}" , name="roledit" , type="manage",subtype="role")
    public ModelAndView roledit(HttpServletRequest request , @PathVariable String orgi, @PathVariable String roleid) throws Exception{
    	ResponseData responseData = new ResponseData("/pages/manage/role/roledit");
    	Role role = (Role)super.getService().getIObjectByPK(Role.class, roleid);
    	ModelAndView modelView = request(responseData , orgi);
    	modelView.addObject("role",role);
    	return modelView;
    }
    @RequestMapping(value="/roledito", method=RequestMethod.POST)  
    public ModelAndView roledito(HttpServletRequest request , @PathVariable String orgi, @Valid Role role){ 
    	ResponseData responseData = new ResponseData(new StringBuffer().append("redirect:/{orgi}/manage/role/list/").append(role.getId()).append("/0.html?msgcode=S_MANAGE_10010025").toString()) ;
    	Role r = (Role)super.getService().getIObjectByPK(Role.class, role.getId());
    	int count = 0;
    	count = super.getService().getCountByCriteria(DetachedCriteria.forClass(Role.class).add(Restrictions.eq("name",role.getName())).add(Restrictions.eq("groupid",r.getGroupid())).add(Restrictions.eq("orgi",orgi)));
    	if(!r.getName().equals(role.getName())&&count>0){
    		responseData.setPage(new StringBuffer().append("redirect:/{orgi}/manage/role/list/").append(role.getId()).append("/0.html?msgcode=E_MANAGE_10010026").toString());
    	}else{
    		r.setDescription(role.getDescription());
    		r.setName(role.getName());
    		r.setUpdatetime(new Date());
        	super.getService().saveOrUpdateIObject(r);
    	}
    	
    	ModelAndView modelView = request(responseData , orgi);
        return modelView ;
    } 
    @RequestMapping(value="/roledelo/{roleid}" , name="roledelo" , type="manage",subtype="role")
    public ModelAndView roledelo(HttpServletRequest request , @PathVariable String orgi, @PathVariable String roleid){  
    	ResponseData responseData = new ResponseData("redirect:/{orgi}/manage/role/index.html?msgcode=S_MANAGE_10010013") ;
    	int count = super.getService().getCountByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("roleid",roleid)));
    	if(count>0){
    		responseData.setPage(new StringBuffer().append("redirect:/{orgi}/manage/role/list/").append(roleid).append("/0.html?msgcode=E_MANAGE_10010016").toString());
    	}else{
    		super.getService().deleteIObject(super.getService().getIObjectByPK(Role.class, roleid));
    	}

    	ModelAndView modelView = request(responseData , orgi);
        return modelView;
    } 
    @RequestMapping(value="{roleid}/userimport" , name="userimport" , type="manage",subtype="role")
    public ModelAndView userimport(HttpServletRequest request , @PathVariable String orgi, @PathVariable String roleid) throws Exception{
    	ResponseData responseData = new ResponseData("/pages/manage/role/userimport") ;
    	ModelAndView modelView = request(responseData , orgi);
    	modelView.addObject("organList", super.getService().findAllByCriteria(DetachedCriteria.forClass(Organ.class).add(Restrictions.eq("orgi",orgi)))) ;
    	modelView.addObject("userorgans",super.getService().findAllByCriteria(DetachedCriteria.forClass(UserOrgan.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("organid","0"))));
    	modelView.addObject("roleid",roleid);
    	return modelView;
    }
    
    @RequestMapping(value="/rolecopy" , name="rolecopy" , type="manage",subtype="role")
    public ModelAndView rolecopy(HttpServletRequest request , @PathVariable String orgi) throws Exception{
    	ResponseData responseData = new ResponseData("/pages/manage/role/rolecopy") ;
    	ModelAndView modelView = request(responseData , orgi);
    	modelView.addObject("groupList",super.getService().findAllByCriteria(DetachedCriteria.forClass(RoleGroup.class).add(Restrictions.eq("orgi",orgi))));
    	modelView.addObject("roleList", super.getService().findAllByCriteria(DetachedCriteria.forClass(Role.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("groupid","0")))) ;
    	return modelView;
    }
    @RequestMapping(value="/{groupid}/rolelist" , name="rolelist" , type="manage",subtype="role")
    public ModelAndView rolelist(HttpServletRequest request , @PathVariable String orgi,@PathVariable String groupid) throws Exception{
    	ResponseData responseData = new ResponseData("/pages/manage/role/rolelist") ; 
    	ModelAndView modelView = request(responseData , orgi);
    	modelView.addObject("roleList", super.getService().findAllByCriteria(DetachedCriteria.forClass(Role.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("groupid",groupid)))) ;
    	return modelView;
    }
    @RequestMapping(value="/rolecopylist" , name="rolecopylist" , type="manage",subtype="role")
    public ModelAndView rolecopylist(HttpServletRequest request , @PathVariable String orgi,@Valid String[] userids,@Valid String roleid) throws Exception{
    	ResponseData responseData = new ResponseData(new StringBuffer().append("redirect:/{orgi}/manage/role/list/").append(roleid).append("/0.html?msgcode=S_MANAGE_10010030").toString()) ; 
    	if(userids!=null){
    		UserRole ur = null;
    		List<UserRole> urs = null;
    		int count = 0 ;
    		boolean flag = true;
    		for (int i = 0; i < userids.length; i++) {
    			count = super.getService().getCountByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("userid",userids[i])).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("roleid",roleid)));
				if(count>0){
					flag=false;
					break;
				}
			}
    		if(flag){
    			for (int i = 0; i < userids.length; i++) {
    				ur = new UserRole();
					ur.setUserid(userids[i]);
					ur.setRoleid(roleid);
					ur.setOrgi(orgi);
					super.getService().saveIObject(ur);
				}
    		}else{
    			responseData.setPage(new StringBuffer().append("redirect:/{orgi}/manage/role/list/").append(roleid).append("/0.html?msgcode=E_MANAGE_10010030").toString());
    		}
    	}
    	ModelAndView modelView = request(responseData , orgi);
    	return modelView;
    }
    
    @RequestMapping(value="/rolemovelist" , name="rolemovelist" , type="manage",subtype="role")
    public ModelAndView rolemovelist(HttpServletRequest request , @PathVariable String orgi,@Valid String[] userids,@Valid String roleid,@Valid String old_roleid) throws Exception{
    	ResponseData responseData = new ResponseData(new StringBuffer().append("redirect:/{orgi}/manage/role/list/").append(roleid).append("/0.html?msgcode=S_MANAGE_10010031").toString()) ; 
    	if(userids!=null){
    		UserRole ur = null;
    		List<UserRole> urs = null;
    		int count = 0 ;
    		boolean flag = true;
    		for (int i = 0; i < userids.length; i++) {
    			count = super.getService().getCountByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("userid",userids[i])).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("roleid",roleid)));
				if(count>0){
					flag=false;
					break;
				}
			}
    		if(flag){
    			for (int i = 0; i < userids.length; i++) {
    				urs = super.getService().findAllByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("roleid",old_roleid)).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("userid",userids[i])));
    				if(urs.size()>0){
    					ur = urs.get(0);
    					ur.setRoleid(roleid);
    					ur.setUserid(userids[i]);
    					super.getService().saveOrUpdateIObject(ur);
    				}
					
				}
    		}else{
    			responseData.setPage(new StringBuffer().append("redirect:/{orgi}/manage/role/list/").append(roleid).append("/0.html?msgcode=E_MANAGE_10010031").toString());
    		}
    	}
    	ModelAndView modelView = request(responseData , orgi);
    	return modelView;
    }
    
    @RequestMapping(value="rolemove" , name="rolemove" , type="manage",subtype="role")
    public ModelAndView rolemove(HttpServletRequest request , @PathVariable String orgi) throws Exception{
    	ResponseData responseData = new ResponseData("/pages/manage/role/rolemove") ;
    	ModelAndView modelView = request(responseData , orgi);
    	modelView.addObject("groupList",super.getService().findAllByCriteria(DetachedCriteria.forClass(RoleGroup.class).add(Restrictions.eq("orgi",orgi))));
    	modelView.addObject("roleList", super.getService().findAllByCriteria(DetachedCriteria.forClass(Role.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("groupid","0")))) ;
    	return modelView;
    }
    
    @RequestMapping(value="/userselect" , name="userselect" , type="manage",subtype="role")
    public ModelAndView userselect(HttpServletRequest request , @PathVariable String orgi) throws Exception{
    	ResponseData responseData = new ResponseData("/pages/manage/task/userselect") ;
    	ModelAndView modelView = request(responseData , orgi);
    	modelView.addObject("organList", super.getService().findPageByCriteria(DetachedCriteria.forClass(Organ.class).add(Restrictions.eq("orgi",orgi)))) ;
    	modelView.addObject("userorgans",super.getService().findAllByCriteria(DetachedCriteria.forClass(UserOrgan.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("organid","0"))));
    	return modelView;
    }
    
    @RequestMapping(value="/userimportlist" , method=RequestMethod.POST, name="userimportlist" , type="manage",subtype="role")
    public ModelAndView userimportlist(HttpServletRequest request , @PathVariable String orgi, @Valid String[] userids, @Valid String roleid) throws Exception{
    	ResponseData responseData = new ResponseData(new StringBuffer().append("redirect:/{orgi}/manage/role/list/").append(roleid).append("/0.html?msgcode=S_MANAGE_10010015").toString()) ;
    	if(userids!=null){
    		boolean flag = true;
    		int count = 0;
    		
    		for (int i = 0; i < userids.length; i++) {
    			count = super.getService().getCountByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("userid",userids[i])).add(Restrictions.eq("roleid",roleid)));
    			if(count>0){
    				flag=false;
    				break;
    			}
			}
    		
    		if(flag){
    			UserRole ur = null;
        		for (int i = 0; i < userids.length; i++) {
    				ur = new UserRole();
    				ur.setOrgi(orgi);
    				ur.setRoleid(roleid);
    				ur.setUserid(userids[i]);
    				super.getService().saveIObject(ur);
    			}
    		}else{
    			responseData.setPage(new StringBuffer().append("redirect:/{orgi}/manage/role/list/").append(roleid).append("/0.html?msgcode=E_MANAGE_10010014").toString());
    		}
    		
    	}
    	ModelAndView modelView = request(responseData , orgi);
    	return modelView;
    }
    
    @RequestMapping(value="{roleid}/roleuserdelo/{userid}" , name="roleuserdelo" , type="manage",subtype="role")
    public ModelAndView roleuserdelo(HttpServletRequest request , @PathVariable String orgi, @PathVariable String roleid, @PathVariable String userid){  
    	ResponseData responseData = new ResponseData(new StringBuffer().append("redirect:/{orgi}/manage/role/list/").append(roleid).append("/0.html?msgcode=S_MANAGE_10010017").toString()) ;
    	List<UserRole> urs = super.getService().findAllByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("roleid",roleid)).add(Restrictions.eq("userid",userid)));
		if(urs.size()>0){
			super.getService().deleteIObject(urs.get(0));
		}
    	ModelAndView modelView = request(responseData , orgi);
        return modelView;
    } 
    @RequestMapping(value="{roleid}/roleuserdellist" , name="roleuserdellist" , method=RequestMethod.POST, type="manage",subtype="role")
    public ModelAndView roleuserdellist(HttpServletRequest request , @PathVariable String orgi, @PathVariable String roleid, @Valid String[] userids){  
    	ResponseData responseData = new ResponseData(new StringBuffer().append("redirect:/{orgi}/manage/role/list/").append(roleid).append("/0.html?msgcode=S_MANAGE_10010018").toString()) ;
    	List<UserRole> urs = null;
    	if(userids!=null){
    		for (int i = 0; i < userids.length; i++) {
        		urs = super.getService().findAllByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("roleid",roleid)).add(Restrictions.eq("userid",userids[i])));
        		if(urs.size()>0){
        			super.getService().deleteIObject(urs.get(0));
        		}
    		}
    		
    	}

    	ModelAndView modelView = request(responseData , orgi);
        return modelView;
    } 
    @RequestMapping(value="/{parentid}/groupadd" , name="groupadd" , type="manage",subtype="role")
    public ModelAndView groupadd(HttpServletRequest request , @PathVariable String orgi, @PathVariable String parentid) throws Exception{
    	ResponseData responseData = new ResponseData("/pages/manage/role/groupadd") ;
    	ModelAndView modelView = request(responseData , orgi);
    	RoleGroup group = new RoleGroup();
    	if("0".equals(parentid)){
    		group.setId("0");
    		group.setName("系统角色");
    	}else{
    		group = (RoleGroup)super.getService().getIObjectByPK(RoleGroup.class, parentid);
    	}
    	modelView.addObject("group",group);
    	return modelView;
    }
    
    @RequestMapping(value="/groupaddo" , name="groupaddo" , type="manage",subtype="role")
    public ModelAndView groupaddo(HttpServletRequest request , @PathVariable String orgi, @Valid RoleGroup group) throws Exception{
    	ResponseData responseData = new ResponseData("");
    	int count = 0;
    	count = super.getService().getCountByCriteria(DetachedCriteria.forClass(RoleGroup.class).add(Restrictions.eq("name",group.getName())).add(Restrictions.eq("parentid",group.getParentid())).add(Restrictions.eq("orgi",orgi)));
    	if(count>0){
    		responseData.setPage("redirect:/{orgi}/manage/role/index.html?msgcode=E_MANAGE_10010018");
    	}else{
    		group.setCreater(super.getUser(request).getId());
        	group.setCreatetime(new Date());
        	group.setOrgi(orgi);
        	super.getService().saveIObject(group);
        	responseData.setPage(new StringBuffer().append("redirect:/{orgi}/manage/role/list/0/").append(group.getId()).append(".html?msgcode=S_MANAGE_10010019").toString()) ;
    	}
    	
    	ModelAndView modelView = request(responseData , orgi);
    	return modelView;
    }
    
    @RequestMapping(value="/{groupid}/groupedit" , name="groupedit" , type="manage",subtype="role")
    public ModelAndView groupedit(HttpServletRequest request , @PathVariable String orgi, @PathVariable String groupid) throws Exception{
    	ResponseData responseData = new ResponseData("/pages/manage/role/groupedit") ;
    	ModelAndView modelView = request(responseData , orgi);
    	RoleGroup group = new RoleGroup();
    	if("0".equals(groupid)){
    		group.setId("0");
    		group.setName("系统角色");
    	}else{
    		group = (RoleGroup)super.getService().getIObjectByPK(RoleGroup.class, groupid);
    		if ("0".equals(group.getParentid())){
    			group.setParentname("系统角色");
    		} else {
    			String parentName = ((RoleGroup)super.getService().getIObjectByPK(RoleGroup.class, group.getParentid())).getName();
    			group.setParentname(parentName);
    		}
    	}
    	modelView.addObject("group",group);
    	return modelView;
    }
    @RequestMapping(value="/groupedito" , name="groupedito" , type="manage",subtype="role")
    public ModelAndView groupedito(HttpServletRequest request , @PathVariable String orgi, @Valid RoleGroup group) throws Exception{
    	ResponseData responseData = new ResponseData("");
    	int count = 0;
    	RoleGroup g = (RoleGroup)super.getService().getIObjectByPK(RoleGroup.class, group.getId());
    	count = super.getService().getCountByCriteria(DetachedCriteria.forClass(RoleGroup.class).add(Restrictions.eq("name",group.getName())).add(Restrictions.eq("parentid",g.getParentid())).add(Restrictions.eq("orgi",orgi)));
    	
    	
    	if(count>0&&!g.getName().equals(group.getName())){
    		responseData.setPage("redirect:/{orgi}/manage/role/index.html?msgcode=E_MANAGE_10010027");
    	}else{
    		g.setName(group.getName());
        	super.getService().updateIObject(g);
        	responseData.setPage(new StringBuffer().append("redirect:/{orgi}/manage/role/list/0/").append(group.getId()).append(".html?msgcode=S_MANAGE_10010027").toString()) ;
    	}
    	
    	ModelAndView modelView = request(responseData , orgi);
    	return modelView;
    }
    
    @RequestMapping(value="/{groupid}/groupdelo" , name="groupdelo" , type="manage",subtype="role")
    public ModelAndView groupdelo(HttpServletRequest request , @PathVariable String orgi, @PathVariable String groupid) throws Exception{
    	ResponseData responseData = new ResponseData("");
    	int count = 0;
    	int groupcount = 0 ;
    	count = super.getService().getCountByCriteria(DetachedCriteria.forClass(Role.class).add(Restrictions.eq("groupid",groupid)).add(Restrictions.eq("orgi",orgi)));
    	groupcount = super.getService().getCountByCriteria(DetachedCriteria.forClass(RoleGroup.class).add(Restrictions.eq("parentid",groupid)).add(Restrictions.eq("orgi",orgi)));
    	if(count>0||groupcount>0){
    		responseData.setPage("redirect:/{orgi}/manage/role/index.html?msgcode=E_MANAGE_10010028");
    	}else{
    		RoleGroup g = new RoleGroup();
    		g.setId(groupid);
    		super.getService().deleteIObject(g);
        	responseData.setPage("redirect:/{orgi}/manage/role/list/0/0.html?msgcode=S_MANAGE_10010028") ;
    	}
    	
    	ModelAndView modelView = request(responseData , orgi);
    	return modelView;
    }
} 