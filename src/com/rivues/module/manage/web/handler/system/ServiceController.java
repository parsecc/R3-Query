package com.rivues.module.manage.web.handler.system;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.handler.Handler;
import com.rivues.module.platform.web.handler.ResponseData;
import com.rivues.module.platform.web.interceptor.LogIntercreptorHandler;
import com.rivues.module.platform.web.model.ConfigureParam;
import com.rivues.module.platform.web.model.SearchResultTemplet;
import com.rivues.util.jdbc.expression.Expression;
import com.rivues.util.service.cache.CacheHelper;

@Controller
@RequestMapping("/{orgi}/manage/system/{service}")
public class ServiceController extends Handler {
	private final Logger log = LoggerFactory
			.getLogger(LogIntercreptorHandler.class);

	/**
	 * 
	 * @param request
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/servicesetting", name = "servicesetting", type = "manage", subtype = "system")
	public ModelAndView servicesetting(HttpServletRequest request,@PathVariable String orgi, @PathVariable String service , @Valid String ex)
			throws Exception {
		ModelAndView view = request(
				super.createManageTempletResponse("/pages/manage/system/propertiessetting"),
				orgi);
		view.addObject("type", service);
		if(ex!=null){
			view.addObject("ex", ex);
		}
		view.addObject("propertiesList",
				super.getService().findAllByCriteria(
						DetachedCriteria.forClass(ConfigureParam.class)
								.add(Restrictions.eq("protype", service))
								.add(Restrictions.not(Restrictions.eq("paramtype", "6")))
								.add(Restrictions.eq("orgi", orgi))));
		//查询根
		List<ConfigureParam> datas = super.getService().findAllByCriteria(
				DetachedCriteria.forClass(ConfigureParam.class)
				.add(Restrictions.eq("protype", service))
				.add(Restrictions.eq("paramtype", "6"))
				.add(Restrictions.or(Restrictions.isNull("groupid"),Restrictions.eq("groupid","")))
				.add(Restrictions.eq("orgi", orgi)));
		
		if(datas != null && datas.size() >0){
			view.addObject("datas",datas);
		}
		List<ConfigureParam> configdatas = super.getService().findAllByCriteria(
				DetachedCriteria.forClass(ConfigureParam.class)
				.add(Restrictions.eq("protype", service))
				.add(Restrictions.eq("paramtype", "6"))
				.add(Restrictions.isNotNull("groupid"))
				.add(Restrictions.not(Restrictions.eq("groupid","")))
				.add(Restrictions.eq("orgi", orgi)));
		if(configdatas != null && configdatas.size() >0){
			view.addObject("configdatas",configdatas);
		}
		return view;
	}

	/**
	 * 
	 * @param request
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/propertiessettingadd", name = "propertiessettingadd", type = "manage", subtype = "system")
	public ModelAndView propertiessettingadd(HttpServletRequest request,
			@PathVariable String orgi, @PathVariable String service , @Valid String ex)
			throws Exception {
		ModelAndView view = request(
				super.createManageResponse("/pages/manage/system/propertiessettingadd"),
				orgi);
		if(ex!=null){
			view.addObject("ex", ex);
		}
		view.addObject("data",
				super.getService().findAllByCriteria(
						DetachedCriteria.forClass(ConfigureParam.class)
								.add(Restrictions.eq("protype", service))
								.add(Restrictions.eq("paramtype", "6"))
								.add(Restrictions.eq("orgi", orgi))));
		view.addObject("type", service);
		return view;
	}

	/**
	 * 
	 * @param request
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/propertiessettingaddo", name = "propertiessettingaddo", type = "manage", subtype = "system")
	public ModelAndView propertiessettingaddo(HttpServletRequest request,
			@PathVariable String orgi, @PathVariable String service,
			@Valid ConfigureParam param) throws Exception {
		int count = super.getService().getCountByCriteria(
				DetachedCriteria.forClass(ConfigureParam.class)
						.add(Restrictions.eq("name", param.getName()))
						.add(Restrictions.eq("orgi", orgi)));
		ResponseData response = new ResponseData(
				"redirect:/{orgi}/manage/system/{service}/servicesetting.html");
		if (count == 0) {
			param.setOrgi(orgi);
			param.setProtype(service);
			CacheHelper.getDistributedDictionaryCacheBean().put(param.getName(),param.getValue() , orgi) ;
			super.getService().saveIObject(param);
			response = new ResponseData(
					"redirect:/{orgi}/manage/system/{service}/servicesetting.html?msgcode=I_MANAGE_70010001");
		} else {
			response = new ResponseData(
					"redirect:/{orgi}/manage/system/{service}/servicesetting.html?msgcode=E_MANAGE_70010002");
		}
		return super.request(response, orgi);
	}

	/**
	 * 
	 * @param request
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/propertiessettingedit/{pid}", name = "propertiessettingedit", type = "manage", subtype = "system")
	public ModelAndView propertiessettingedit(HttpServletRequest request,
			@PathVariable String orgi, @PathVariable String service,
			@PathVariable String pid) throws Exception {
		ModelAndView view = request(
				super.createManageResponse("/pages/manage/system/propertiessettingedit"),
				orgi);
		view.addObject("type", service);
		view.addObject("properties",
				super.getService().getIObjectByPK(ConfigureParam.class, pid));
		view.addObject("data",
				super.getService().findAllByCriteria(
						DetachedCriteria.forClass(ConfigureParam.class)
								.add(Restrictions.eq("protype", service))
								.add(Restrictions.eq("paramtype", "6"))
								.add(Restrictions.eq("orgi", orgi))));
		return view;
	}

	/**
	 * 
	 * @param request
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/propertiessettingcode/{pid}", name = "propertiessettingcode", type = "manage", subtype = "system")
	public ModelAndView propertiessettingcode(HttpServletRequest request,
			@PathVariable String orgi, @PathVariable String service,
			@PathVariable String pid) throws Exception {
		ModelAndView view = request(
				super.createManageResponse("/pages/manage/system/propertiessettingcode"),
				orgi);
		view.addObject("type", service);
		view.addObject("properties",
				super.getService().getIObjectByPK(ConfigureParam.class, pid));
		return view;
	}
	
	/**
	 * 
	 * @param request
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/propertiessettingeditdo", name = "propertiessettingeditdo", type = "manage", subtype = "system")
	public ModelAndView propertiessettingeditdo(HttpServletRequest request,
			@PathVariable String orgi, @PathVariable String service,
			@Valid ConfigureParam param) throws Exception {
		int count = super.getService().getCountByCriteria(
				DetachedCriteria.forClass(ConfigureParam.class)
						.add(Restrictions.eq("name", param.getName()))
						.add(Restrictions.not(Restrictions.eq("id", param.getId())))
						.add(Restrictions.eq("orgi", orgi)));
		ResponseData response = new ResponseData(
				"redirect:/{orgi}/manage/system/{service}/servicesetting.html");
		if (count == 0) {
			if(request.getParameter("update")!=null && request.getParameter("update").equals("true")){
				ConfigureParam temp = (ConfigureParam) super.getService().getIObjectByPK(ConfigureParam.class, param.getId()) ;
				temp.setValue(param.getValue()) ;
				param = temp ;
			}
			param.setOrgi(orgi);
			param.setProtype(service);
			super.getService().updateIObject(param);
			CacheHelper.getDistributedDictionaryCacheBean().put(param.getName(), param.getValue() ,orgi) ;
			response = new ResponseData(
					"redirect:/{orgi}/manage/system/{service}/servicesetting.html?msgcode=I_MANAGE_80030001");
		} else {
			response = new ResponseData(
					"redirect:/{orgi}/manage/system/{service}/servicesetting.html?msgcode=E_MANAGE_80030002");
		}
		return super.request(response, orgi);
	}
	
	
	/**
	 * 
	 * @param request
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/propertiessettingeditdof", name = "propertiessettingeditdof", type = "manage", subtype = "system")
	public ModelAndView propertiessettingeditdof(HttpServletRequest request,
			@PathVariable String orgi, @PathVariable String service,
			@Valid ConfigureParam param) throws Exception {
		int count = super.getService().getCountByCriteria(
				DetachedCriteria.forClass(ConfigureParam.class)
						.add(Restrictions.eq("id", param.getId()))
						.add(Restrictions.eq("orgi", orgi)));
		if (count == 1) {
			ConfigureParam temp = (ConfigureParam) super.getService().getIObjectByPK(ConfigureParam.class, param.getId()) ;
			temp.setValue(param.getValue()) ;
			param = temp ;
			param.setOrgi(orgi);
			param.setProtype(service);
			super.getService().updateIObject(param);
			CacheHelper.getDistributedDictionaryCacheBean().update(param.getName(), orgi, param.getValue()) ;
		}
		ModelAndView view = request(
				super.createPageResponse("/pages/public/success"),
				orgi);
		view.addObject("type", service);
		view.addObject(
				"propertiesList",
				super.getService().findAllByCriteria(
						DetachedCriteria.forClass(ConfigureParam.class)
								.add(Restrictions.eq("protype", service))
								.add(Restrictions.eq("orgi", orgi))));
		return view;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/propertiessettingdel/{id}" , name="propertiessettingdel" , type="manage",subtype="system")
    public ModelAndView propertiessettingdel(HttpServletRequest request , @PathVariable String orgi , @PathVariable String id, @PathVariable String service) throws Exception{  
		ConfigureParam config= new ConfigureParam();
		config.setId(id) ;
		config = (ConfigureParam) super.getService().getIObjectByPK(ConfigureParam.class, id) ;
    	super.getService().deleteIObject(config) ;
    	CacheHelper.getDistributedDictionaryCacheBean().delete(config.getName(), orgi) ;
    	return super.request(new ResponseData("redirect:/{orgi}/manage/system/{service}/servicesetting.html?msgcode=I_MANAGE_80030002")  , orgi) ;
    }

}