package com.rivues.module.manage.web.service;

import java.io.InputStream;
import java.util.List;

import com.rivues.module.platform.web.exception.BusinessException;
import com.rivues.module.platform.web.model.User;

/**
 * 账号service
 * @author leon
 *
 */
public interface AccountService {
	/**
	 * 用户导入实现
	 * @param is
	 * @param loginUser
	 * @throws BusinessException
	 */
	public List<String> importuser(InputStream is ,User loginUser) throws BusinessException;
}
