package com.rivues.module.report.web.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.AnalyzerReport;
import com.rivues.module.platform.web.model.Auth;
import com.rivues.module.platform.web.model.User;
import com.rivues.util.serialize.JSON;

/**
 * 发布以后的报表，只读报表 , 兼容以前版本，不做改动，个人报表 和 开发中的报表保存在 rivu_analyzerreport表中
 * @author admin
 *
 */
@Entity
@Table(name = "rivu5_publishedreport")
@org.hibernate.annotations.Proxy(lazy = false)
public class PublishedReport implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id ;
	private String name ;	
	private String reporttype ; //0 代表动态报表  1 代表自助查询报表
	private String viewtype;
	private String title ;		 //变更用处，修改为 报表code		
	private String orgi ;
	private int objectcount ;    //用来标记报表默认打开是否加载数据
	private String dicid ;	//目录ID
	private String description ;			
	private Date createtime ;
	private String html ;			//改变用处，用于存储 是否允许里面访问移动端报表
	private String reportcontent;	//报表序列化以后的结果，JSON格式
	private String status;
	private String rolename ;		//变更用处，标记为 动态报表 默认为 null 或者 0 都是 自助查询，1表示自定义报表
	private String userid ;			//变更用处，标记为 仪表盘的 属主ID
	private String blacklist ;		//变更用处，用于区分是否是  仪表盘
	private String reportpackage ;	//报表路径
	private String useacl ;			//启用权限控制    ,  变更用处，  用于控制是否覆盖上级目录的权限
	private String reportmodel	;	//自助查询的是 保存 Model 的ID
	private Date updatetime;		//修改时间 
	private String creater;
	private int reportversion ;
	private String publishedtype ;
	private String tabtype ;
	private String username ;
	private String useremail ;
	private boolean cache;//1启用缓存，0不启用
	private String extparam;		//默认使用 player 打开
	private String targetReport;//reporttype=shortcuts 的时候的目标报表
	private AnalyzerReport report ;
	private String source ;			//报表来源，如果是在  事件设计器里创建的 报表，则此字段不为空，无法保存
	
	/**
	 * 权限列表
	 * add by huqi 2014/5/7
	 */
	private List<Auth> authList = new ArrayList<Auth>();
	@Transient
	public List<Auth> getAuthList() {
		return authList;
	}
	public void setAuthList(List<Auth> authList) {
		this.authList = authList;
	}
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getReporttype() {
		return reporttype;
	}
	public void setReporttype(String reporttype) {
		this.reporttype = reporttype;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public int getObjectcount() {
		return objectcount;
	}
	public void setObjectcount(int objectcount) {
		this.objectcount = objectcount;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getDicid() {
		return dicid;
	}
	public void setDicid(String dicid) {
		this.dicid = dicid;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	@Transient
	public String getViewtype() {
		return viewtype;
	}
	public void setViewtype(String viewtype) {
		this.viewtype = viewtype;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRolename() {
		return rolename!=null ? rolename : RivuDataContext.ReportTypeEnum.REPORT.toString();
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getBlacklist() {
		return blacklist != null ? blacklist : rolename ;
	}
	public void setBlacklist(String blacklist) {
		this.blacklist = blacklist;
	}
	public String getReportpackage() {
		return reportpackage;
	}
	public void setReportpackage(String reportpackage) {
		this.reportpackage = reportpackage;
	}
	public String getUseacl() {
		return useacl;
	}
	public void setUseacl(String useacl) {
		this.useacl = useacl;
	}
	public String getReportmodel() {
		return reportmodel;
	}
	public void setReportmodel(String reportmodel) {
		this.reportmodel = reportmodel;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public int getReportversion() {
		return reportversion;
	}
	public void setReportversion(int reportversion) {
		this.reportversion = reportversion;
	}
	public String getReportcontent() {
		return reportcontent;
	}
	public void setReportcontent(String reportcontent) {
		this.reportcontent = reportcontent;
	}
	public String getPublishedtype() {
		return publishedtype;
	}
	public void setPublishedtype(String publishedtype) {
		this.publishedtype = publishedtype;
	}
	public String getTabtype() {
		return tabtype;
	}
	public void setTabtype(String tabtype) {
		this.tabtype = tabtype;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUseremail() {
		return useremail;
	}
	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}
	public boolean isCache() {
		return cache;
	}
	public void setCache(boolean cache) {
		this.cache = cache;
	}
	public String getExtparam() {
		return extparam;
	}
	public void setExtparam(String extparam) {
		this.extparam = extparam;
	}
	
	public String getTargetReport() {
		return targetReport;
	}
	public void setTargetReport(String targetReport) {
		this.targetReport = targetReport;
	}
	/**
	 * 
	 * @param code
	 * @return
	 */
	public boolean isAllow(String code , User user){
		boolean allow = false ;
		if(code!=null && this.authList!=null){
			for(Auth auth : this.authList){
				if(auth.getAuthtype()!=null && auth.getAuthtype().indexOf(code)>=0){
					allow = true ;
					break ;
				}
			}
		}
		return (user!=null && user.getUsertype()!=null && user.getUsertype().equals(RivuDataContext.UserTypeEnum.SUPER.getValue()))|| (this.tabtype!=null && this.tabtype.equals(RivuDataContext.TabType.PRI.toString())) || (this.creater!=null && user!=null && this.creater.equals(user.getId())) ? true : allow ;
	}
	
	@Transient
	public AnalyzerReport getReport() {
		AnalyzerReport analyzerReport = report!=null && reportcontent!=null ? report : (report = JSON.parseObject(this.reportcontent, AnalyzerReport.class));
		if(analyzerReport!=null){
			analyzerReport.setLazy(this.objectcount == 1);
		}
		analyzerReport.setLoadata(false);
		analyzerReport.setReportname(this.name);//把report的名称设置进去
		return analyzerReport ;
	}
	public void setReport(AnalyzerReport report) {
		this.report = report;
	}
	@Transient
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
}
