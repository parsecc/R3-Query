package com.rivues.module.report.web.model;

import java.util.ArrayList;
import java.util.List;

public class TaskReportInfo implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6329622220084023651L;
	
	private List<TaskReport> reportList = new ArrayList<TaskReport>();
	private List<TaskNode> taskNodeList = new ArrayList<TaskNode>();
	
	public List<TaskReport> getReportList() {
		return reportList;
	}
	public void setReportList(List<TaskReport> reportList) {
		this.reportList = reportList;
	}
	public List<TaskNode> getTaskNodeList() {
		return taskNodeList == null ? taskNodeList = new ArrayList<TaskNode>() : taskNodeList ;
	}
	public void setTaskNodeList(List<TaskNode> taskNodeList) {
		this.taskNodeList = taskNodeList;
	}
}
