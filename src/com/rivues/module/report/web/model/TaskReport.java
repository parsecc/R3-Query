package com.rivues.module.report.web.model;

import java.util.ArrayList;
import java.util.List;

import com.rivues.module.platform.web.model.ReportFilter;

public class TaskReport implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4005098016743294602L;
	private String reportid ;
	private String userDefined;//是否用户自定义的，system系统插入的，user用户自定义的
	private PublishedReport publishedReport ;
	public TaskReport(){}
	public TaskReport(String reportid){
		this.reportid = reportid ;
	}
	
	private List<ReportFilter> filterList = new ArrayList<ReportFilter>();
	public String getReportid() {
		return reportid;
	}
	public void setReportid(String reportid) {
		this.reportid = reportid;
	}
	public PublishedReport getPublishedReport() {
		return publishedReport;
	}
	public void setPublishedReport(PublishedReport publishedReport) {
		this.publishedReport = publishedReport;
	}
	public List<ReportFilter> getFilterList() {
		return filterList;
	}
	public void setFilterList(List<ReportFilter> filterList) {
		this.filterList = filterList;
	}
	public String getUserDefined() {
		return userDefined;
	}
	public void setUserDefined(String userDefined) {
		this.userDefined = userDefined;
	}
	
	
}
