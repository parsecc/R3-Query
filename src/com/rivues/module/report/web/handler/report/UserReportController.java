package com.rivues.module.report.web.handler.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.tools.zip.ZipOutputStream;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.rivues.core.RivuDataContext;
import com.rivues.core.RivuDataContext.ReportTypeEnum;
import com.rivues.module.platform.web.auth.AuthInterface;
import com.rivues.module.platform.web.bean.TaskInfo;
import com.rivues.module.platform.web.handler.Handler;
import com.rivues.module.platform.web.handler.ReportAPIController;
import com.rivues.module.platform.web.handler.ResponseData;
import com.rivues.module.platform.web.model.Auth;
import com.rivues.module.platform.web.model.DataDic;
import com.rivues.module.platform.web.model.JobDetail;
import com.rivues.module.platform.web.model.Organ;
import com.rivues.module.platform.web.model.PublishedCube;
import com.rivues.module.platform.web.model.ReportModel;
import com.rivues.module.platform.web.model.Role;
import com.rivues.module.platform.web.model.RoleGroup;
import com.rivues.module.platform.web.model.SearchResultTemplet;
import com.rivues.module.platform.web.model.User;
import com.rivues.module.platform.web.model.UserOrgan;
import com.rivues.module.platform.web.model.UserRole;
import com.rivues.module.report.web.model.PublishedReport;
import com.rivues.util.RivuTools;
import com.rivues.util.exception.DicExistEception;
import com.rivues.util.exception.DicNotExistException;
import com.rivues.util.exception.ReportException;
import com.rivues.util.iface.report.ReportFactory;
import com.rivues.util.local.LocalTools;
import com.rivues.util.serialize.JSON;
import com.rivues.util.service.cache.CacheHelper;
import com.rivues.util.tools.CronTools;
import com.rivues.util.tools.ZipTools;
/**
 * 用户报表门户功能  
 * @author admin
 *
 */
@Controller  
@RequestMapping("/{orgi}/user/report/{tabid}")  
public class UserReportController extends Handler{
	
	private final Logger logger = LoggerFactory.getLogger(UserReportController.class);
    @Autowired
    private AuthInterface authInterface;
    
	/**
	 * 报表新增页面
	 * @param request
	 * @param orgi
	 * @param location
	 * @return
	 */
	@RequestMapping(value="/{location}/reportadd" , name="reportadd" , type="user" , subtype="public")  
    public ModelAndView reportadd(HttpServletRequest request , @PathVariable String orgi , @PathVariable String tabid , @PathVariable String location)throws DicNotExistException{  
		
    	ResponseData responseData = new ResponseData("/pages/user/report/reportadd") ; 
    	ModelAndView modelView = request(responseData , orgi) ;
    	modelView.addObject("dicList", ReportFactory.getReportInstance().getReportDicList(orgi , super.getUser(request) , location , tabid)) ;
    	modelView.addObject("parentid", location) ;
    	DataDic dic = null;
    	if("0".equals(location)){
    		dic = new DataDic();
    		dic.setId("0");
    		if("pub".equals(tabid)){
    			dic.setName("公共文件夹");
    		}else if("pri".equals(tabid)){
    			dic.setName("个人文件夹");
    		}else{
    			dic.setName("我的文件夹");
    		}
    		
    	}else{
    		dic = ReportFactory.getReportInstance().getReportDic(location, orgi, super.getUser(request));
    	}
    	modelView.addObject("reportdic",dic);
    	return modelView ; 
    }
	/**
	 * 创建报表目录页面
	 * @param request
	 * @param orgi
	 * @param location
	 * @return
	 */
	@RequestMapping(value="/{location}/reportdicadd" , name="dicadd" , type="user" , subtype="public")  
    public ModelAndView dicadd(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid , @PathVariable String location)throws Exception{  
    	ResponseData responseData = new ResponseData("/pages/user/report/reportdicadd") ; 
    	ModelAndView modelView = request(responseData , orgi) ;
    	modelView.addObject("parentid", location) ;
    	modelView.addObject("tabid", tabid) ;
    	DataDic dic = null;
    	if("0".equals(location)){
    		dic = new DataDic();
    		dic.setId("0");
    		if("pub".equals(tabid)){
    			dic.setName("公共文件夹");
    		}else{
    			dic.setName("个人文件夹");
    		}
    		
    	}else{
    		dic = ReportFactory.getReportInstance().getReportDic(location, orgi, super.getUser(request));
    	}
    	modelView.addObject("reportdic",dic);
    	return modelView ; 
    }
	/**
	 * 
	 * @param request
	 * @param orgi
	 * @param location
	 * @param report
	 * @return
	 * @throws DicExistEception 
	 */
	@RequestMapping(value="/{location}/reportdicaddo" , name="reportdicaddo" , type="user" , subtype="public")  
    public ModelAndView reportdicaddo(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid , @PathVariable String location , @Valid DataDic datadic) throws DicExistEception{  
		ResponseData responseData = new ResponseData("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=S_REPORT_10010002") ;
		int count = ReportFactory.getReportInstance().getCount(DetachedCriteria.forClass(DataDic.class)
				.add(Restrictions.eq("orgi",orgi))
				.add(Restrictions.eq("name",datadic.getName()))
				.add(Restrictions.eq("parentid",datadic.getParentid()))
				.add(Restrictions.eq("tabtype",tabid)), super.getUser(request));
		if(count>0){
			responseData.setPage("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=E_REPORT_10010003");
			//throw new DicExistEception(LocalTools.getMessage("E_REPORT_10010003"));
		}else{
			datadic.setOrgi(orgi) ;
			datadic.setTabtype(tabid) ;
			datadic.setCreater(super.getUser(request).getId());
			datadic.setCreatetime(new Date()) ;
			datadic.setUpdatetime(new Date());
			datadic.setPublishedtype(RivuDataContext.ReportStatusEnum.PUBLISHED.toString()) ;
			datadic.setStatus(RivuDataContext.ReportStatusEnum.AVAILABLE.toString()) ;
			ReportFactory.getReportInstance().createReportDic(datadic, super.getUser(request)) ;
			responseData.setPage(new StringBuffer().append("redirect:/{orgi}/user/{tabid}/reportdic/").append(datadic.getId()).append(".html?msgcode=S_REPORT_10010002").toString());
		}
		return request(responseData , orgi) ; 
    }
	
	/**
	 * 
	 * @param request
	 * @param orgi
	 * @param location
	 * @param report
	 * @return
	 * @throws DicExistEception 
	 * @throws DicNotExistException 
	 */
	@RequestMapping(value="/{location}/reportdicrm" , name="reportdicrm" , type="user" , subtype="public")  
    public ModelAndView reportdicrm(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid , @PathVariable String location) throws DicExistEception, DicNotExistException{  
		ResponseData responseData = new ResponseData("redirect:/{orgi}/user/{tabid}/tabindex.html") ;
		List<DataDic> datadicList = ReportFactory.getReportInstance().getReportDicList(orgi, super.getUser(request), location, tabid) ;
		if(datadicList.size()>0){
			responseData = new ResponseData("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=E_USER_600100001") ;
		}else{
			int count = ReportFactory.getReportInstance().getCount(DetachedCriteria.forClass(PublishedReport.class).add(Restrictions.eq("dicid",location)).add(Restrictions.eq("orgi",orgi)), super.getUser(request));
			if(count>0){
				responseData = new ResponseData("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=E_USER_600100002") ;
			}else{
				ReportFactory.getReportInstance().rmDataDicByID(super.getUser(request), orgi, location) ;
				responseData = new ResponseData("redirect:/{orgi}/user/{tabid}/tabindex.html?msgcode=E_USER_600100003") ;
			}
		}
		return request(responseData , orgi) ; 
    }
	
	/**
	 * 创建报表目录页面
	 * @param request
	 * @param orgi
	 * @param location
	 * @return
	 * @throws DicNotExistException 
	 */
	@RequestMapping(value="/{dicid}/reportdicedit" , name="reportdicedit" , type="user" , subtype="public")  
    public ModelAndView reportdicedit(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid ,  @PathVariable String dicid) throws DicNotExistException{  
    	ResponseData responseData = new ResponseData("/pages/user/report/reportdicedit") ; 
    	ModelAndView view = request(responseData , orgi) ;
    	DataDic dic = new DataDic();
    	if(!"0".equals(dicid)){
    		dic= ReportFactory.getReportInstance().getReportDic(dicid, orgi, super.getUser(request));
    	} else {
    		dic.setName("公共文件夹");
    		dic.setTabtype("pub");
    		dic.setParentid("1");
    		dic.setId("0");
    		//dic.setCreatetime(new Date());
    		//dic.setUpdatetime(new Date());
    		dic.setDescription("根目录");
    		view.addObject("dicInfo", dic);
    	}
    	dic.setAuthList(authInterface.filterAuth(dic, super.getUser(request) , null , orgi , RivuDataContext.AuthTypeEnum.DIC.toString()));
		view.addObject("dicInfo", dic);
    	//获取所有角色
    	List<Role> roleList = super.getService().findPageByCriteria(DetachedCriteria.forClass(Role.class).add(Restrictions.eq("orgi",orgi)));
    	view.addObject("roleList", roleList) ;
    	view.addObject("groupList",super.getService().findAllByCriteria(DetachedCriteria.forClass(RoleGroup.class).add(Restrictions.eq("orgi",orgi))));
    	//获取该报表的所有权限拥有着(角色/组织/个人)
    	view.addObject("authlist",authInterface.getDicAuthList(dicid, orgi));
//    	
//    	view.addObject("organList", super.getService().findPageByCriteria(DetachedCriteria.forClass(Organ.class).add(Restrictions.eq("orgi",orgi)))) ;
//    	view.addObject("userorgans",super.getService().findAllByCriteria(DetachedCriteria.forClass(UserOrgan.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("organid","0"))));
    	
    	return view ; 
    }
	
	/**
	 * 
	 * @param request
	 * @param orgi
	 * @param location
	 * @param report
	 * @return
	 * @throws ReportException 
	 * @throws DicExistEception 
	 */
	@RequestMapping(value="/{location}/reportaddo" , name="reportaddo" , type="user" , subtype="public")  
    public ModelAndView reportaddo(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid , @PathVariable String location , @Valid PublishedReport report) throws ReportException, DicExistEception{  
		ResponseData responseData = new ResponseData("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=S_REPORT_10010005") ;
		int count = ReportFactory.getReportInstance().getCount(DetachedCriteria.forClass(PublishedReport.class)
				.add(Restrictions.eq("orgi",orgi))
				.add(Restrictions.eq("name",report.getName()))
				.add(Restrictions.eq("reporttype",RivuDataContext.ReportTypeEnum.REPORT.toString()))
				.add(Restrictions.eq("dicid",report.getDicid())),super.getUser(request));
		if(count>0){
			responseData = new ResponseData("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=E_REPORT_10010005") ;
			//throw new ReportException(LocalTools.getMessage("E_REPORT_10010005"));
		}else{
			report.setOrgi(orgi) ;
			report.setCreater(super.getUser(request).getId());
			report.setCreatetime(new Date()) ;
			report.setUpdatetime(new Date()) ;
			report.setTabtype(tabid) ;
			report.setCache(true);
			report.setUsername(super.getUser(request).getUsername()) ;
			report.setUseremail(super.getUser(request).getEmail());
			report.setReporttype(RivuDataContext.ReportTypeEnum.REPORT.toString()) ;
			report.setPublishedtype(RivuDataContext.ReportStatusEnum.NEW.toString()) ;
			report.setStatus(RivuDataContext.ReportStatusEnum.UNAVAILABLE.toString()) ;
			report.setReportpackage(this.getParent(report.getDicid(), orgi));
			report.setBlacklist(report.getRolename()) ;
			
			ReportFactory.getReportInstance().createReport(report , super.getUser(request)) ;
			report.setUserid(report.getId()) ;
			ReportFactory.getReportInstance().updateReport(report, super.getUser(request)) ;
 			/**
			 * 如果是动态报表，则同时在 DataDic 中创建一个 DataDic 对象，类型即为 Dashboard
			 */
			if(report.getRolename()!=null && ReportTypeEnum.DASHBOARD.toString().equals(report.getRolename())){
				DataDic datadic = new DataDic();
				datadic.setName(report.getName()) ;
				datadic.setId(report.getId()) ;
				datadic.setOrgi(orgi) ;
				datadic.setTabtype(tabid) ;
				datadic.setCreater(super.getUser(request).getId());
				datadic.setCreatetime(new Date()) ;
				datadic.setUpdatetime(new Date());
				datadic.setParentid(report.getDicid()) ;
				datadic.setCode(RivuDataContext.ReportTypeEnum.DASHBOARD.toString()) ;
				datadic.setPublishedtype(RivuDataContext.ReportStatusEnum.PUBLISHED.toString()) ;
				datadic.setStatus(RivuDataContext.ReportStatusEnum.AVAILABLE.toString()) ;
				ReportFactory.getReportInstance().createReportDic(datadic, super.getUser(request)) ;
			}
		}
		
		return request(responseData , orgi) ; 
    }
	
	
	/**
	 * 
	 * @param request
	 * @param orgi
	 * @param location
	 * @param report
	 * @return
	 * @throws ReportException 
	 */
	@RequestMapping(value="/authorize" , name="authorize" , type="user" , subtype="authorize")  
    public ModelAndView authorize(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid) throws ReportException{  
		ResponseData responseData = new ResponseData("/pages/user/report/reportauthorize") ;
		if(request.getParameter("ownertype")!=null && request.getParameter("ownertype").equals(Auth.RESOURCE_TYPE_DIC)){
			responseData = new ResponseData("/pages/user/report/dicauthorize") ;
		}
		String authexpress = request.getParameter("authexpress") ;
		String[] userid = authexpress.split(",");
		for (String string : userid) {//user_
			if(string.indexOf("_")>0){
				String ownertype = string.substring(0 , string.indexOf("_"));
				if(string.length()>(string.indexOf("_")+1)){
					String ownerid = string.substring(string.indexOf("_")+1);
					Auth auth = new Auth();
					auth.setResourcedic(request.getParameter("dicid")) ;
					if(request.getParameter("ownertype")!=null && request.getParameter("ownertype").equals(Auth.RESOURCE_TYPE_DIC)){
						auth.setResourcetype(Auth.RESOURCE_TYPE_DIC);
					}else{
						auth.setResourcetype(Auth.RESOURCE_TYPE_REPORT);
					}
					if(ownertype.equals("organ")){
						auth.setOwnertype(Auth.OWNER_TYPE_ORGAN) ;
					}else if(ownertype.equals("role")){
						auth.setOwnertype(Auth.OWNER_TYPE_ROLE) ;
					}else{//剩下的都是user
						auth.setOwnertype(Auth.OWNER_TYPE_USER) ;
					}
					auth.setOrgi(orgi) ;
					auth.setDataid(request.getParameter("dicid")) ;
					auth.setResourceid(request.getParameter("reportid")) ;
					auth.setOwnerid(ownerid) ;
					int count = super.getService().getCountByCriteria(DetachedCriteria.forClass(Auth.class)
						.add(Restrictions.eq("orgi",orgi))
						.add(Restrictions.eq("ownerid",auth.getOwnerid()))
						.add(Restrictions.eq("resourceid",auth.getResourceid()))) ;
					if(count == 0 ){
						super.getService().saveIObject(auth) ;
					}
				}	
			}
		}
		ModelAndView view = request(responseData , orgi) ; 
		//获取该报表的所有权限拥有着(角色/组织/个人)
    	view.addObject("authlist",authInterface.getReportAuthList(request.getParameter("reportid"), orgi));
    	view.addObject("reportid",request.getParameter("reportid")) ;
		view.addObject("dicid",request.getParameter("dicid")) ;
		return view ; 
    }
	
	/**
	 * 
	 * @param request
	 * @param orgi
	 * @param location
	 * @param report
	 * @return
	 * @throws ReportException 
	 */
	@RequestMapping(value="/rmauthorize" , name="authorize" , type="user" , subtype="rmauthorize")  
    public ModelAndView rmauthorize(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid , @Valid Auth auth) throws ReportException{  
		ResponseData responseData = new ResponseData("/pages/user/report/reportauthorize") ; 
		String id = request.getParameter("id") ;
		if(auth!=null && auth.getId()!=null){
			super.getService().deleteIObject(auth) ;
		}
		ModelAndView view = request(responseData , orgi) ; 
		//获取该报表的所有权限拥有着(角色/组织/个人)
		view.addObject("reportid",request.getParameter("reportid")) ;
		view.addObject("dicid",request.getParameter("dicid")) ;
		
    	view.addObject("authlist",authInterface.getReportAuthList(request.getParameter("reportid"), orgi));
		return view ; 
    }
	
	
	@RequestMapping(value="/{location}/reportedito" , name="reportedito" , type="user" , subtype="public")  
    public ModelAndView reportedito(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid , @PathVariable String location , @ModelAttribute("data") PublishedReport report,@Valid String publishType,@Valid String isRecover,@Valid String url,@Valid String[] authInfo) throws Exception{  
		ResponseData responseData = new ResponseData("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=S_REPORT_10010006") ;
		PublishedReport rpt = (PublishedReport) RivuDataContext.getService().getIObjectByPK(PublishedReport.class, report.getId()) ;
		int count = ReportFactory.getReportInstance().getCount(DetachedCriteria.forClass(PublishedReport.class)
				.add(Restrictions.eq("orgi",orgi))
				.add(Restrictions.eq("name",report.getName()))
				.add(Restrictions.eq("reporttype",RivuDataContext.ReportTypeEnum.REPORT.toString()))
				.add(Restrictions.eq("dicid",rpt.getDicid())),super.getUser(request));
		if(!rpt.getName().equals((report.getName()))&&count>0){
			throw new ReportException(LocalTools.getMessage("E_REPORT_10010006"));
		}else{
			//add by huqi 2014/5/7 增加报表权限
			if(authInfo!= null && authInfo.length>0 || authInfo == null){
				if(authInfo==null){
					authInfo = new String[]{};
				}
				updateAuth(rpt.getId(), orgi, authInfo, Auth.RESOURCE_DIC_N);
			}
			rpt = resetReportProperties(rpt, report) ;

			ReportFactory.getReportInstance().updateReport(rpt, super.getUser(request)) ;
			
			Collection<?> allReportList = CacheHelper.getDistributedCacheBean().getAllCacheObject(orgi) ; 
			for(Object name : allReportList){
				Object cacheData = CacheHelper.getDistributedCacheBean().getCacheObject(name.toString(), orgi) ;
				if(cacheData!=null && cacheData instanceof PublishedReport){
					PublishedReport cacheReport = (PublishedReport) cacheData ;
					if(cacheReport.getId().equals(report.getId())){
						cacheReport = resetReportProperties(cacheReport, report) ;
						CacheHelper.getDistributedCacheBean().update(name.toString(), orgi, cacheReport);
					}
				}
			}
			
			/**
			 * 要将当期缓存的所有用户报表更新，否则，可能会出现数据覆盖
			 */
			if(report.getRolename()!=null && ReportTypeEnum.DASHBOARD.toString().equals(report.getRolename())){
				DataDic datadic = ReportFactory.getReportInstance().getReportDic(report.getId(), orgi, super.getUser(request)) ;
				datadic.setName(rpt.getName()) ;
				ReportFactory.getReportInstance().updateReportDic(datadic, super.getUser(request)) ;
			}
			//发布
			if(!"no".equals(publishType)){
				this.publisho(request, orgi, report.getId(), publishType, isRecover, url);
			}
			
		}

		return request(responseData , orgi) ; 
    }
	/**
	 * 
	 * @param rpt
	 * @param report
	 * @return
	 */
	private PublishedReport resetReportProperties(PublishedReport rpt , PublishedReport report){
		if(rpt!=null && report!=null){
			rpt.setRolename(report.getRolename()) ;
			rpt.setName(report.getName());
			rpt.setUsername(report.getUsername());
			rpt.setTitle(report.getTitle()) ;
			rpt.setUseremail(report.getUseremail());
			rpt.setUseacl(report.getUseacl()) ;
			rpt.setHtml(report.getHtml());
			rpt.setExtparam(report.getExtparam()) ;
			rpt.setUpdatetime(new Date());
			rpt.setDescription(report.getDescription());
			rpt.setCache(report.isCache());
			rpt.setStatus(report.getStatus());
			rpt.setBlacklist(report.getRolename()) ;
			rpt.setObjectcount(report.getObjectcount());
		}
		return rpt ;
	}
	
	@RequestMapping(value="/{location}/reportdicedito" , name="reportdicedito" , type="user" , subtype="public")  
    public ModelAndView reportdicedito(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid , @PathVariable String location , @Valid DataDic datadic,@Valid String[] authInfo) throws Exception{
		ResponseData responseData = new ResponseData("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=S_REPORT_10010004") ;
		if(authInfo != null && authInfo.length> 0 || authInfo == null){
			if(authInfo == null){
				authInfo = new String[]{};
			}
			updateAuth(datadic.getId(),orgi, authInfo,Auth.RESOURCE_TYPE_DIC);
		}
		if (!datadic.getId().equals("0")){
			DataDic dic = ReportFactory.getReportInstance().getReportDic(datadic.getId(), orgi, super.getUser(request));
			int count = ReportFactory.getReportInstance().getCount(DetachedCriteria.forClass(DataDic.class)
					.add(Restrictions.eq("orgi",orgi))
					.add(Restrictions.eq("name",datadic.getName()))
					.add(Restrictions.eq("parentid",dic.getParentid()))
					.add(Restrictions.eq("tabtype",tabid)), super.getUser(request));
			if(!dic.getName().equals(datadic.getName())&&count>0){
				responseData.setPage("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=E_REPORT_10010004");
				//throw new DicExistEception(LocalTools.getMessage("E_REPORT_10010004"));
			}else{
				dic.setName(datadic.getName());
				dic.setDescription(datadic.getDescription());
				dic.setUpdatetime(new Date());
				dic.setType(datadic.getType());
				ReportFactory.getReportInstance().updateReportDic(dic, super.getUser(request)) ;
				responseData.setPage(new StringBuffer().append("redirect:/{orgi}/user/{tabid}/reportdic/").append(datadic.getId()).append(".html?msgcode=S_REPORT_10010004").toString());
			}
			
		}	
		
		return request(responseData , orgi) ; 
    }
	
	/**
	 * 更新权限
	 * @param dataDic
	 * @param authInfoArray
	 * @throws Exception
	 */
	private void updateAuth(String resourceid,String orgi,String[] authInfoArray,String isDic) throws Exception{
		Map<String,StringBuffer> map = new HashMap<String, StringBuffer>();
		Map<String, String> ownerTypeMap = new HashMap<String, String>();
		for(String authInfo : authInfoArray){
			String roleId = authInfo.split("_")[0];
			String authId = authInfo.split("_")[1];
			String authtype = authInfo.split("_")[2];
			ownerTypeMap.put(roleId, authtype);
			if (!map.containsKey(roleId)){
				map.put(roleId,new StringBuffer());
			}
			map.get(roleId).append(authId).append(",");
		}
		List<Auth> list = new ArrayList<Auth>();
		//设置权限列表
		for(String key : map.keySet()){
			Auth auth = new Auth();
			auth.setResourceid(resourceid);
			auth.setCreatetime(new Date());
			auth.setOrgi(orgi);
			auth.setOwnerid(key);
			auth.setOwnertype(ownerTypeMap.get(key));
			auth.setResourcedic(isDic);
			auth.setResourcetype(Auth.RESOURCE_TYPE_REPORT);
			String authType = map.get(key).toString();
			auth.setAuthtype(authType.substring(0,authType.length()-1));
			//如果包含读的权限，单独设置
			if(authType.contains(Auth.AUTH_READ))auth.setAuthread(Auth.AUTH_READ);
			list.add(auth);
		}
		if(list!=null){
			authInterface.updateAuth(resourceid, list);
		}
		
	}
		
	
	@RequestMapping(value="/{location}/reportimport" , name="reportimport" , type="user" , subtype="public")  
    public ModelAndView reportimport(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid , @PathVariable String location ) throws ReportException,DicNotExistException{  
		ResponseData responseData = new ResponseData("/pages/user/report/reportimport") ; 
		ModelAndView modelView = request(responseData , orgi) ;
		return modelView; 
    }
	
	@RequestMapping(value="/{location}/{reportid}/reportdelo" , name="reportdelo" , type="user" , subtype="public")  
    public ModelAndView reportdelo(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid , @PathVariable String location,@PathVariable String reportid) throws ReportException{  
		ResponseData responseData = new ResponseData("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=S_REPORT_10010008") ;
		
		PublishedReport publishedReport = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportid) ;
		
		//查询所有的与此报表相关联的报表，比如快捷方式，另存为的报表
		List<PublishedReport> saveAsReportList = ReportFactory.getReportInstance().getReportList(reportid, publishedReport.getOrgi(),RivuDataContext.TabType.PRI.toString() , null);
		for(PublishedReport report : saveAsReportList){//删除关联数据；
			ReportFactory.getReportInstance().rmReportByID(report.getId(), super.getUser(request), orgi) ;
		}
		
		//查询所有的与此报表相关联的指标关联数据
		String rmid = RivuTools.md5(reportid) ;
		List<ReportModel> reportmodellist = super.getService().findAllByCriteria(DetachedCriteria.forClass(ReportModel.class).add(Restrictions.eq("id",rmid))) ;
		for(ReportModel model : reportmodellist){//删除关联数据；
			super.getService().deleteIObject(model);
		}
		
		ReportFactory.getReportInstance().rmReportByID(reportid, super.getUser(request), orgi);
		if(publishedReport.getRolename()!=null && ReportTypeEnum.DASHBOARD.toString().equals(publishedReport.getRolename())){
			/**
			 * 删除目录下的所有报表
			 */
			List<PublishedReport> reportList = ReportFactory.getReportInstance().getReportList(publishedReport.getId(), publishedReport.getOrgi(), super.getUser(request), publishedReport.getTabtype(), RivuDataContext.ReportTypeEnum.REPORT.toString()) ;
			for(PublishedReport report : reportList){
				//查询所有的与此报表相关联的报表，比如快捷方式，另存为的报表
				saveAsReportList = ReportFactory.getReportInstance().getReportList(report.getId(), report.getOrgi(),RivuDataContext.TabType.PRI.toString() , null);
				for(PublishedReport savereport : saveAsReportList){//删除关联数据；
					ReportFactory.getReportInstance().rmReportByID(savereport.getId(), super.getUser(request), orgi) ;
				}
				
				ReportFactory.getReportInstance().rmReportByID(report.getId(), super.getUser(request), orgi) ;
			}
			try {
				ReportFactory.getReportInstance().rmDataDicByID(super.getUser(request), orgi, publishedReport.getId()) ;
			} catch (DicNotExistException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		ModelAndView modelView = request(responseData , orgi) ;
		return modelView; 
    }
	
	@RequestMapping(value="/{location}/{reportid}/reportrename" , name="reportrename" , type="user" , subtype="public")  
    public ModelAndView reportrename(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid , @PathVariable String location,@PathVariable String reportid , @Valid String name) throws ReportException{  
		ResponseData responseData = new ResponseData("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=S_REPORT_10010021") ;
		PublishedReport publishedReport = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportid) ;
		if(name!=null && name.length()>0){
			publishedReport.setName(name);
			ReportFactory.getReportInstance().updateReport(publishedReport, super.getUser(request)) ;
		}
		
		return  request(responseData , orgi) ;
    }
	
	@RequestMapping(value="/{location}/taskadd" , name="taskadd" , type="user" , subtype="public")  
    public ModelAndView taskadd(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid, @PathVariable String location,@Valid String[] reportids) throws ReportException{  
		ResponseData responseData = new ResponseData("/pages/user/report/taskadd") ;
		ModelAndView modelView = request(responseData , orgi) ;
		List<PublishedReport> list = new ArrayList<PublishedReport>();
		PublishedReport report = null;
		String name = "";
		if(reportids!=null){
			for (int i = 0; i < reportids.length; i++) {
				report = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportids[i]);
				list.add(report);
				if(name == ""){
					name = report.getName();
				}
			}
		}
		modelView.addObject("name",name);
		modelView.addObject("reports",list);
		modelView.addObject("user",super.getUser(request));
		modelView.addObject("location",location);
		return modelView; 
    }
	
	@RequestMapping(value="/{location}/{planid}/taskedit" , name="taskedit" , type="user" , subtype="public")  
    public ModelAndView taskedit(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid, @PathVariable String location,@PathVariable String planid) throws ReportException{  
		ResponseData responseData = new ResponseData("/pages/user/report/taskedit") ;
		ModelAndView modelView = request(responseData , orgi) ;
		PublishedReport plan = ReportFactory.getReportInstance().getReportByIDFromDB(super.getUser(request), orgi, planid) ;
		modelView.addObject("plan", plan) ;
		List<PublishedReport> list = new ArrayList<PublishedReport>();
		PublishedReport report = null;
		if(plan!=null && plan.getReportcontent()!=null && plan.getReportcontent().length()>0){
			TaskInfo taskInfo = JSON.parseObject(plan.getReportcontent(), TaskInfo.class)  ;
			if(taskInfo!=null){
				String[] reportids = taskInfo.getReportids();
				for (int i = 0; i < reportids.length; i++) {
					report = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportids[i]);
					list.add(report);
				}
				modelView.addObject("taskinfo",taskInfo);
			}
		}
		modelView.addObject("reports",list);
		modelView.addObject("user",super.getUser(request));
		modelView.addObject("location",location);
		return modelView; 
    }
	
	
	
	/**
	 * 同步到jobdetail
	 * @param user
	 * @param taskinfo
	 * @param report
	 * @param location
	 * @throws ParseException 
	 * @throws Exception
	 */
	private void saveJobs(User user,TaskInfo taskinfo,PublishedReport report,String location) throws ParseException{
		JobDetail job = new JobDetail();
		job.setName(report.getName());
		job.setTaskid(report.getId());
		job.setTaskstatus("0");
		job.setTasktype(RivuDataContext.TaskTypeEnum.REPORT.toString()) ;
		job.setFetcher(true);
		job.setPause(false);
		job.setPlantask(true);
		job.setPriority(taskinfo.getPriority()) ;
		job.setUserid(user.getId());
		job.setUsername(user.getUsername());
		job.setEmail(user.getEmail());
		job.setNickname(user.getNickname());
		StringBuffer sb = new StringBuffer();
		String[] strs = taskinfo.getReportids();
		for (int i = 0; i <strs.length ; i++) {
			sb.append(strs[i]).append(",");
		}
		job.setDataid(sb.toString());
		job.setDicid(taskinfo.getDicids());
		job.setOrgi(report.getOrgi());
		//"0 15 10 * * ? *" 每天上午10:15触发
		job.setCronexp(RivuTools.convertCrond(taskinfo));
		/**
		 * 设定触发时间
		 */
		job.setNextfiretime(CronTools.getFinalFireTime(job.getCronexp() , new Date())) ;
		List<JobDetail> jobDetailList = super.getService().findAllByCriteria(DetachedCriteria.forClass(JobDetail.class).add(Restrictions.eq("orgi",report.getOrgi())).add(Restrictions.eq("taskid",report.getId()))) ;
		if(jobDetailList.size()>0){
			for(JobDetail jobDetail : jobDetailList){
				super.getService().deleteIObject(jobDetail) ;
			}
		}
		super.getService().saveIObject(job);
	}
	
	@RequestMapping(value="/{location}/taskaddo" , name="taskaddo" , type="user" , subtype="public")  
    public ModelAndView taskaddo(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid, @PathVariable String location,@Valid TaskInfo taskinfo,@Valid PublishedReport report,@Valid String emails) throws ReportException, ParseException{  
		ResponseData responseData = new ResponseData(new StringBuffer("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=S_REPORT_100100010").toString()) ;
		int count = ReportFactory.getReportInstance().getCount(DetachedCriteria.forClass(PublishedReport.class)
				.add(Restrictions.eq("orgi",orgi))
				.add(Restrictions.eq("name",report.getName()))
				.add(Restrictions.eq("reporttype",RivuDataContext.ReportTypeEnum.PLAN.toString()))
				.add(Restrictions.eq("dicid",location)),super.getUser(request));
		if(count>0){
			responseData = new ResponseData(new StringBuffer("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=E_REPORT_10010011").toString()) ;
		}else{
			User user = super.getUser(request);
			report.setOrgi(orgi) ;
			report.setCreater(user.getId());
			report.setUsername(user.getUsername());
			report.setUseremail(user.getEmail());
			report.setCreatetime(new Date()) ;
			report.setUpdatetime(new Date()) ;
			report.setTabtype(tabid) ;
			report.setReporttype(RivuDataContext.ReportTypeEnum.PLAN.toString()) ;
			report.setPublishedtype(RivuDataContext.ReportStatusEnum.PUBLISHED.toString()) ;
			report.setStatus(RivuDataContext.ReportStatusEnum.AVAILABLE.toString()) ;
			report.setReportpackage(this.getParent(location, orgi));
			report.setDicid(location);
			taskinfo.setDicids(this.getParentIds(location, orgi));
			taskinfo.setEmails(emails);
			report.setReportcontent(JSON.toJSONString(taskinfo));
			ReportFactory.getReportInstance().createReport(report , super.getUser(request)) ;
			
			//把任务同步到jobdetail
			
			saveJobs(user, taskinfo, report, location);
		}
		return request(responseData , orgi) ;
    }
	
	
	@RequestMapping(value="/{location}/taskeditdo" , name="taskeditdo" , type="user" , subtype="public")  
    public ModelAndView taskeditdo(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid, @PathVariable String location,@ModelAttribute TaskInfo taskinfo,@Valid PublishedReport report,@Valid String emails) throws ReportException, ParseException{  
		ResponseData responseData = new ResponseData(new StringBuffer("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=S_REPORT_100100011").toString()) ;
		int count = ReportFactory.getReportInstance().getCount(DetachedCriteria.forClass(PublishedReport.class)
				.add(Restrictions.eq("orgi",orgi))
				.add(Restrictions.eq("name",report.getName()))
				.add(Restrictions.eq("reporttype",RivuDataContext.ReportTypeEnum.PLAN.toString()))
				.add(Restrictions.eq("dicid",location)),super.getUser(request));
		PublishedReport rpt = (PublishedReport)super.getService().getIObjectByPK(PublishedReport.class, report.getId());
		if(count>0&&!rpt.getName().equals(report.getName())){
			responseData.setPage(new StringBuffer("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=E_REPORT_100100011").toString());
		}else{	
			report.setOrgi(rpt.getOrgi()) ;
			report.setCreater(rpt.getCreater());
			report.setCreatetime(rpt.getCreatetime()) ;
			report.setUpdatetime(new Date()) ;
			report.setTabtype(rpt.getTabtype()) ;
			report.setReporttype(rpt.getReporttype()) ;
			report.setPublishedtype(rpt.getPublishedtype()) ;
			report.setStatus(rpt.getStatus()) ;
			report.setReportpackage(this.getParent(location, orgi));
			report.setDicid(location);
			report.setUseremail(rpt.getUseremail());
			report.setUsername(rpt.getUsername());
			
			report.setReporttype(RivuDataContext.ReportTypeEnum.PLAN.toString()) ;
			report.setPublishedtype(RivuDataContext.ReportStatusEnum.PUBLISHED.toString()) ;
			report.setStatus(RivuDataContext.ReportStatusEnum.AVAILABLE.toString()) ;
			
			taskinfo.setDicids(this.getParentIds(location, orgi));
			taskinfo.setEmails(emails);
			report.setReportcontent(JSON.toJSONString(taskinfo));
			ReportFactory.getReportInstance().updateReport(report, super.getUser(request)) ;
			//把任务同步到jobdetail
			saveJobs(super.getUser(request), taskinfo, report, location);
		}
		
		return request(responseData , orgi) ;
    }
	
	@RequestMapping(value="/reportmove" , name="reportmove" , type="user" , subtype="public")  
    public ModelAndView reportmove(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid ) throws ReportException{  
		ResponseData responseData = new ResponseData("/pages/user/report/reportmove") ;
		ModelAndView modelView = request(responseData , orgi) ;
		modelView.addObject("dicList",ReportFactory.getReportInstance().getAllReportDicList(orgi, super.getUser(request), tabid));
		return modelView; 
    }
	
	@RequestMapping(value="/shortcutadd/{reportid}" , name="shortcutadd" , type="user" , subtype="public")  
    public ModelAndView shortcutadd(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid, @PathVariable String reportid ) throws ReportException{  
		ResponseData responseData = new ResponseData("/pages/user/report/shortcutadd") ;
		ModelAndView modelView = request(responseData , orgi) ;
		modelView.addObject("dicList",ReportFactory.getReportInstance().getAllReportDicList(orgi, super.getUser(request), null));
		modelView.addObject("reportid",reportid);
		return modelView; 
    }
	
	@RequestMapping(value="/shortcutaddo" , name="shortcutaddo" , type="user" , subtype="public")  
    public ModelAndView shortcutaddo(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid, @Valid PublishedReport report, @Valid String targetReportid) throws ReportException{  
		ResponseData responseData = new ResponseData(new StringBuffer(new StringBuffer().append("redirect:/{orgi}/user/").append(report.getTabtype()).append("/reportdic/").append(report.getDicid()).append(".html?msgcode=S_REPORT_10020011")).toString()) ;
		ModelAndView modelView = request(responseData , orgi) ;
		report.setCreatetime(new Date());
		report.setCreater(super.getUser(request).getId());
		report.setReporttype(RivuDataContext.ReportTypeEnum.SHORTCUTS.toString());
		report.setOrgi(orgi);
		
		PublishedReport targetReport = new PublishedReport();
		targetReport.setId(targetReportid);
		report.setTargetReport(targetReportid);
		report.setStatus(RivuDataContext.ReportStatusEnum.AVAILABLE.toString());
		report.setPublishedtype(RivuDataContext.ReportStatusEnum.PUBLISHED.toString());
		targetReport = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, targetReportid) ;
		report.setName(targetReport.getName()+"的快捷方式");
		report.setCreater(targetReport.getUserid());
		report.setUserid(targetReport.getUserid());
		report.setStatus(RivuDataContext.ReportStatusEnum.AVAILABLE.toString());
		report.setUsername(targetReport.getUsername());
		report.setUseremail(targetReport.getUseremail());
		
		report.setCreatetime(new Date());
		report.setUpdatetime(new Date());
		
		ReportFactory.getReportInstance().createReport(report, super.getUser(request));
		return modelView; 
    }
	
	
	@RequestMapping(value="/reportmovelist" , name="reportmovelist" , type="user" , subtype="public",method=RequestMethod.POST)  
    public ModelAndView reportmovelist(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid,@Valid String[] reportids,@Valid String dicid) throws ReportException{
		ResponseData responseData = new ResponseData(new StringBuffer("redirect:/{orgi}/user/{tabid}/reportdic/").append(dicid).append(".html?msgcode=S_REPORT_10010009").toString()) ;
		PublishedReport report = null;
		if(reportids!=null){
			boolean flag = true;
			int count = 0;
			List<PublishedReport> list = new ArrayList<PublishedReport>();
			for (int i = 0; i < reportids.length; i++) {
				report = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportids[i]);
				
				count = ReportFactory.getReportInstance().getCount(DetachedCriteria.forClass(PublishedReport.class)
						.add(Restrictions.eq("orgi",orgi))
						.add(Restrictions.eq("name",report.getName()))
						.add(Restrictions.eq("dicid",dicid)), super.getUser(request));
				if(count>0){
					flag = false;
					break;
				}
				list.add(report);
			}
			if(flag){
				for (int i = 0; i < list.size(); i++) {
					report = list.get(i);
					report.setDicid(dicid);
					ReportFactory.getReportInstance().updateReport(report, super.getUser(request));
				}
			}else{
				responseData.setPage(new StringBuffer("redirect:/{orgi}/user/{tabid}/reportdic/").append(dicid).append(".html?msgcode=E_REPORT_10010009").toString());
			}
			
		}
		ModelAndView modelView = request(responseData , orgi) ;
		return modelView; 
    }
	
	@RequestMapping(value="/reportcopy" , name="reportcopy" , type="user" , subtype="public")  
    public ModelAndView reportcopy(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid ) throws ReportException{  
		ResponseData responseData = new ResponseData("/pages/user/report/reportcopy") ;
		ModelAndView modelView = request(responseData , orgi) ;
		modelView.addObject("dicList",ReportFactory.getReportInstance().getAllReportDicList(orgi, super.getUser(request), tabid));
		return modelView; 
    }
	
	@RequestMapping(value="/reportimporto" , name="reportimporto" , type="user" , subtype="public")  
    public ModelAndView reportimporto(HttpServletRequest request , HttpServletResponse response,@PathVariable String orgi ,@RequestParam MultipartFile file , @Valid String command) throws Exception{  
		ResponseData responseData = new ResponseData("redirect:/{orgi}/user/index.html?msgcode=S_REPORT_10010007") ;
		ModelAndView modelView = request(responseData , orgi) ;
		ReportAPIController reportController = new ReportAPIController();
		reportController.reportimporto(request, response, orgi, file, null, null, command);
		return modelView; 
    }
	
	@RequestMapping(value="/{location}/taskimport" , name="taskimport" , type="user" , subtype="public")  
    public ModelAndView taskimport(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid , @PathVariable String location ) throws ReportException,DicNotExistException{  
		ResponseData responseData = new ResponseData("/pages/user/report/taskimport") ; 
		ModelAndView modelView = request(responseData , orgi) ;
		return modelView; 
    }
	
	@RequestMapping(value="/taskimporto" , name="taskimporto" , type="user" , subtype="public")  
    public ModelAndView taskimporto(HttpServletRequest request , HttpServletResponse response,@PathVariable String orgi ,@RequestParam MultipartFile file , @Valid String command) throws Exception{  
		ResponseData responseData = new ResponseData("redirect:/{orgi}/user/index.html?msgcode=S_REPORT_100100200") ;
		
		ReportAPIController reportController = new ReportAPIController();
		int count = reportController.taskimporto(request, response, orgi, file, null, null, command);
		if(count==0){
			responseData.setPage("redirect:/{orgi}/user/index.html?msgcode=E_REPORT_100100200");
		}
		ModelAndView modelView = request(responseData , orgi) ;
		return modelView; 
    }
	
	@RequestMapping(value="/reportcopylist" , name="reportcopylist" , type="user" , subtype="public",method=RequestMethod.POST)  
    public ModelAndView reportcopylist(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid,@Valid String[] reportids,@Valid String dicid) throws ReportException,Exception{
		ResponseData responseData = new ResponseData(new StringBuffer("redirect:/{orgi}/user/{tabid}/reportdic/").append(dicid).append(".html?msgcode=S_REPORT_10010010").toString()) ;
		PublishedReport report = null;
		if(reportids!=null){
			boolean flag = true;
			int count = 0;
			List<PublishedReport> list = new ArrayList<PublishedReport>();
			for (int i = 0; i < reportids.length; i++) {
				report = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportids[i]);
				
				count = ReportFactory.getReportInstance().getCount(DetachedCriteria.forClass(PublishedReport.class)
						.add(Restrictions.eq("orgi",orgi))
						.add(Restrictions.eq("name",report.getName()))
						.add(Restrictions.eq("dicid",dicid)), super.getUser(request));
				if(count>0){
					flag = false;
					break;
				}
				list.add(report);
			}
			if(flag){
				PublishedReport rpt = null;
				for (int i = 0; i < list.size(); i++) {
					report = list.get(i);
					rpt = new PublishedReport();
					BeanUtils.copyProperties(rpt, report);
					rpt.setId(null);
					rpt.setDicid(dicid);
					ReportFactory.getReportInstance().createReport(rpt, super.getUser(request));
				}
			}else{
				responseData.setPage(new StringBuffer("redirect:/{orgi}/user/{tabid}/reportdic/").append(dicid).append(".html?msgcode=E_REPORT_10010010").toString());
			}
			
		}
		ModelAndView modelView = request(responseData , orgi) ;
		return modelView; 
    }
	
	@RequestMapping(value="/{location}/reportdellist" , name="reportdellist" , type="user" , subtype="public",method=RequestMethod.POST)  
    public ModelAndView reportdellist(HttpServletRequest request , @PathVariable String orgi, @PathVariable String tabid,@Valid String[] reportids) throws ReportException{
		ResponseData responseData = new ResponseData("redirect:/{orgi}/user/{tabid}/reportdic/{location}.html?msgcode=S_REPORT_10010011") ;
		PublishedReport report = null;
		for (int i = 0; i < reportids.length; i++) {
			ReportFactory.getReportInstance().rmReportByID(reportids[i], super.getUser(request), orgi);
		}
		ModelAndView modelView = request(responseData , orgi) ;
		return modelView; 
    }
	
	@RequestMapping(value="/reportexport" , name="reportexport" , type="user" , subtype="public")  
    public ModelAndView reportexport(HttpServletRequest request ,HttpServletResponse response, @PathVariable String orgi, @PathVariable String tabid) throws ReportException{  
		ResponseData responseData = new ResponseData("/pages/user/report/reportexport") ;
		ModelAndView modelView = request(responseData , orgi) ;
		return modelView; 
    }
	
	@RequestMapping(value="/{location}/reportdownload" , name="reportdownload" , type="user" , subtype="public")  
    public ModelAndView reportdownload(HttpServletRequest request ,HttpServletResponse response, @PathVariable String orgi, @PathVariable String tabid ,
    		@PathVariable String location , @Valid String[] reportids,@Valid Boolean reportIsExport,
    		@Valid Boolean cubeIsExport,@Valid Boolean dsIsExport,@Valid Boolean authIsExport) throws ReportException,DicNotExistException,Exception{  
		DataDic dic = null;
		if("0".equals(location)){
			dic = new DataDic();
			dic.setId("0");
			if("pub".equals(tabid)){
				dic.setName("公共文件夹");
			}else{
				dic.setName("个人文件夹");
			}
		}else{
			dic = ReportFactory.getReportInstance().getReportDic(location, orgi, super.getUser(request));
		}
		//如果导出目录下不存在报表，直接return null
//		if(reportids==null){
//			List<PublishedReport> reports = ReportFactory.getReportInstance().getReportList(location, orgi,super.getUser(request), tabid,RivuDataContext.ReportTypeEnum.REPORT.toString());
//			if(reports == null || reports.size() ==0){
//				ResponseData responseData = new ResponseData("redirect:/{orgi}/user/index.html?msgcode=E_REPORT_100100012") ;
//				ModelAndView modelView = request(responseData , orgi) ;
//				return modelView; 
//			}
//		}
		
		response.setContentType("charset=UTF-8;application/zip"); 
		response.addHeader("Content-Disposition","attachment;filename="+URLEncoder.encode(dic.getName(),"utf-8")+".zip");
		ByteArrayOutputStream byteArrayOutputStream = null ;
        ZipOutputStream out = new ZipOutputStream(byteArrayOutputStream = new ByteArrayOutputStream()); 
        out.setEncoding(System.getProperty("sun.jnu.encoding")) ;
		List<String> entryNames = new ArrayList<String>();
			try {    
				List<Object> diclist = super.getService().findAllByCriteria(DetachedCriteria.forClass(DataDic.class).add(Restrictions.eq("orgi",orgi)));
				Map<Integer, Object> map = ZipTools.getAllParentDic(location, diclist,DataDic.class);
				String path = ZipTools.parentDicToZip(map,DataDic.class,"report/",out,entryNames,orgi);//所有的父级目录添加到zip
				User user =  super.getUser(request);
				
				if(reportids!=null){//压缩选中报表
					List<PublishedReport> reports = new ArrayList<PublishedReport>();
					PublishedReport rpt = null;
					for (int i = 0; i < reportids.length; i++) {
						rpt = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportids[i]);
						//if(i==0 && rpt != null)response.addHeader("Content-Disposition","attachment;filename="+URLEncoder.encode(rpt.getName(),"utf-8")+".zip"); 
						reports.add(rpt);
					}
					ZipTools.reportsToZip(reports,path,reportIsExport,cubeIsExport,dsIsExport,authIsExport,super.getUser(request),tabid,orgi, out,entryNames);
				}else{//压缩全部
					List<PublishedReport> reports = ReportFactory.getReportInstance().getReportList(location, orgi,user, tabid,RivuDataContext.ReportTypeEnum.REPORT.toString());
					//if(reports!= null && reports.size()>0)response.addHeader("Content-Disposition","attachment;filename="+URLEncoder.encode(reports.get(0).getName(),"utf-8")+".zip"); 
					entryNames = ZipTools.reportsToZip(reports, path,reportIsExport,cubeIsExport,dsIsExport,authIsExport,user,tabid,orgi, out,entryNames);
					List<DataDic> childList = super.getService().findAllByCriteria(DetachedCriteria.forClass(DataDic.class).add(Restrictions.eq("parentid",location)).add(Restrictions.eq("orgi",orgi)));
					ZipTools.childReportsAndDicToZip(childList, path,reportIsExport,cubeIsExport,dsIsExport,authIsExport, user, tabid, orgi, out,entryNames);
				}
			} catch (Exception e) {    
	            e.printStackTrace();  
	        } finally {    
	        	if(out!=null){
	        		try {
						out.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	        	}
	        }
			if(byteArrayOutputStream!=null){
	        	try {
					byteArrayOutputStream.writeTo(response.getOutputStream());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
		
		
		return null;
    }
	
	
	@RequestMapping(value="/{location}/taskdownload" , name="taskdownload" , type="user" , subtype="public")  
    public ModelAndView taskdownload(HttpServletRequest request ,HttpServletResponse response, @PathVariable String orgi, @PathVariable String tabid ,
    		@PathVariable String location , @Valid String[] reportids) throws ReportException{  
		
		response.setContentType("charset=UTF-8;application/zip"); 
		//response.addHeader("Content-Disposition","attachment;filename=reports.zip"); 
		ByteArrayOutputStream byteArrayOutputStream = null ;
        ZipOutputStream out = new ZipOutputStream(byteArrayOutputStream = new ByteArrayOutputStream()); 
        out.setEncoding(System.getProperty("sun.jnu.encoding")) ;
		List<String> entryNames = new ArrayList<String>();
			try {    
				List<Object> diclist = super.getService().findAllByCriteria(DetachedCriteria.forClass(DataDic.class).add(Restrictions.eq("orgi",orgi)));
				Map<Integer, Object> map = ZipTools.getAllParentDic(location, diclist,DataDic.class);
				String path = ZipTools.parentDicToZip(map,DataDic.class,"task/",out,entryNames,orgi);//所有的父级目录添加到zip
				
				if(reportids!=null){//压缩选中计划任务
					List<PublishedReport> reports = new ArrayList<PublishedReport>();
					PublishedReport rpt = null;
					JobDetail job = null;
					List<JobDetail> joblist = null;
					for (int i = 0; i < reportids.length; i++) {
						rpt = ReportFactory.getReportInstance().getReportByIDFromDB(super.getUser(request), orgi, reportids[i]);
						joblist = RivuDataContext.getService().findAllByCriteria(DetachedCriteria.forClass(JobDetail.class).add(Restrictions.eq("taskid",rpt.getId())).add(Restrictions.eq("orgi",orgi)));
						if(joblist.size()>0){
							job = joblist.get(0);
						}
						TaskInfo task = JSON.parseObject(rpt.getReportcontent(), TaskInfo.class);
						task.setJob(job);
						rpt.setReportcontent(JSON.toJSONString(task));
						if(i==0 && rpt != null)response.addHeader("Content-Disposition","attachment;filename="+URLEncoder.encode(rpt.getName(),"utf-8")+".zip"); 
						reports.add(rpt);
					}
					ZipTools.tasksToZip(reports, path, out);
					
				}
			} catch (Exception e) {    
	            e.printStackTrace();  
	        } finally {    
	        	if(out!=null){
	        		try {
						out.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	        	}
	        }
			if(byteArrayOutputStream!=null){
	        	try {
					byteArrayOutputStream.writeTo(response.getOutputStream());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
		
		
		return null;
    }
	
	

    @RequestMapping(value="/newreport" , name="newreport" , type="user" , subtype="public")  
    public ModelAndView newreport(HttpServletRequest request , @PathVariable String orgi , @PathVariable String tabid){  
    	ResponseData responseData = new ResponseData("/pages/user/report/newreport") ; 
    	ModelAndView modelView = request(responseData , orgi) ;
    	/**
    	 * 将模板类型放入到选择列表里
    	 */
    	modelView.addObject("reporttempletList", super.getService().findAllByCriteria(DetachedCriteria.forClass(SearchResultTemplet.class).addOrder(Order.asc("createtime"))
    			.add(Restrictions.eq("templettype", RivuDataContext.TempletType.REPORT.toString())))) ;
    	/**
    	 * 获取已发布的数据模型，JSON格式 ，需要按角色获取 ，已发布的模型包含授权数据，从授权信息表里获取 ， 当前用户角色 可能包含多个
    	 */
    	modelView.addObject("cubeList", super.getService().findAllByCriteria(DetachedCriteria.forClass(PublishedCube.class).addOrder(Order.asc("name"))
    			.add(Restrictions.eq("orgi", orgi)))) ;
    	modelView.addObject("tabid", tabid) ;
    	return modelView ; 
    }
    
    @RequestMapping(value="/newchart", name="newreport" , type="user" , subtype="public")  
    public ModelAndView newchart(HttpServletRequest request , @PathVariable String orgi , @PathVariable String tabid){  
    	ResponseData responseData = new ResponseData("/pages/user/report/newchart") ; 
    	return request(responseData , orgi) ; 
    }
    
    
    @RequestMapping(value="/reportproperties/{dicid}/{reportid}", name="reportproperties" , type="user" , subtype="public")  
    public ModelAndView properties(HttpServletRequest request , @PathVariable String orgi , @PathVariable String tabid , @PathVariable String dicid, @PathVariable String reportid) throws ReportException{  
    	ResponseData responseData = new ResponseData("/pages/user/report/reportproperties") ;
    	ModelAndView view = request(responseData , orgi) ; 
    	PublishedReport report = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportid);
    	authInterface.filterReportAuth(report, super.getUser(request));
    	view.addObject("report", report) ;
    	view.addObject("dicid",dicid);
    	//获取所有角色
    	List<Role> roleList = super.getService().findAllByCriteria(DetachedCriteria.forClass(Role.class).add(Restrictions.eq("orgi",orgi)));
    	view.addObject("roleList", roleList) ;
    	view.addObject("groupList",super.getService().findAllByCriteria(DetachedCriteria.forClass(RoleGroup.class).add(Restrictions.eq("orgi",orgi))));
    	
    	//获取该报表的所有权限拥有着(角色/组织/个人)
    	view.addObject("authlist",authInterface.getReportAuthList(reportid, orgi));
    	
    	//view.addObject("organList", super.getService().findAllByCriteria(DetachedCriteria.forClass(Organ.class).add(Restrictions.eq("orgi",orgi)))) ;
    	
    	//view.addObject("userorgans",super.getService().findAllByCriteria(DetachedCriteria.forClass(UserOrgan.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("organid","0"))));
    
    	//view.addObject("userRoles", super.getService().findAllByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("organid","0"))));
    	return view;
    }
    
    
    @RequestMapping(value="/publish/{dicid}/{reportid}", name="reportpublish" , type="user" , subtype="public")  
    public ModelAndView publish(HttpServletRequest request , @PathVariable String orgi , @PathVariable String tabid , @PathVariable String reportid, @PathVariable String dicid) throws ReportException{  
    	ResponseData responseData = new ResponseData("/pages/user/report/reportpublish") ;
    	ModelAndView view = request(responseData , orgi) ; 
    	view.addObject("report", ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportid)) ;
    	view.addObject("dicid",dicid);
    	return view; 
    }
    
    @RequestMapping(value="/reportselect", name="reportselect" , type="user" , subtype="public")  
    public ModelAndView reportselect(HttpServletRequest request , @PathVariable String orgi , @PathVariable String tabid){  
    	ResponseData responseData = new ResponseData("/pages/manage/task/reportselect") ;
    	ModelAndView view = request(responseData , orgi) ; 
    	view.addObject("reports", ReportFactory.getReportInstance().getReportList("0", orgi, super.getUser(request), tabid, RivuDataContext.ReportTypeEnum.REPORT.toString())) ;
    	view.addObject("dicList", ReportFactory.getReportInstance().getAllReportDicList(orgi , super.getUser(request) , tabid)) ;
    	return view; 
    }
    
 
    
    /**
     * add by huqi 2014/5/9
     * 显示该组织下的用户和下级组织
     * @param request
     * @param orgi
     * @param tabid
     * @param organid
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/ogranselect/{organid}", name="ogranselect" , type="user" , subtype="public")  
    public ModelAndView organselect(HttpServletRequest request , @PathVariable String orgi , @PathVariable String tabid, @PathVariable String organid) throws Exception{  
    	ResponseData responseData = new ResponseData("/pages/user/report/organselect") ;
    	ModelAndView view = request(responseData , orgi) ; 
    	Map<String, List> map = new HashMap<String, List>();
    	//获取该组织下面的用户
    	List<UserOrgan> list =  super.getService().findAllByCriteria(DetachedCriteria.forClass(UserOrgan.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("organid",organid)));
    	map.put("users", list);
    	//获取下级组织
    	List<Organ> subOrgans = super.getService().findAllByCriteria(DetachedCriteria.forClass(Organ.class).add(Restrictions.eq("parentid", organid)));
    	map.put("organs", subOrgans);
    	view.addObject("userandorgan",map);
    	view.addObject("user",super.getUser(request));
    	return view; 
    }
    
    
    /**
     * add by huqi 2014/5/9
     * 显示该组织下的用户和下级组织
     * @param request
     * @param orgi
     * @param tabid
     * @param organid
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/rolegroupselect/{groupid}/{roleid}", name="ogranselect" , type="user" , subtype="public")  
    public ModelAndView rolegroupselect(HttpServletRequest request , @PathVariable String orgi , @PathVariable String tabid, @PathVariable String roleid,@PathVariable String groupid) throws Exception{  
    	ResponseData responseData = new ResponseData("/pages/user/report/roleselect") ;
    	ModelAndView modelView = request(responseData , orgi) ; 
    	if("0".equals(roleid)){
    		//选择的是目录，需要查询出角色
			List<Role> roleList =  super.getService().findAllByCriteria(DetachedCriteria.forClass(Role.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("groupid",groupid)));
    		//查询出该组织下所有角色关联的用户信息，分页签显示
    		if(roleList != null &&roleList.size()>=0){
    			Set<UserRole>  userlist = new HashSet<UserRole>();
    			for(Role role : roleList){
    				userlist.addAll(super.getService().findAllByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("roleid",role.getId()))));
    			}
    			modelView.addObject("useroleList", userlist);
    		}
    	} else {
	    	modelView.addObject("useroleList", 	super.getService().findAllByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("orgi",orgi)).add(Restrictions.eq("roleid",roleid)))) ;
    	}
    	modelView.addObject("groupList",super.getService().findAllByCriteria(DetachedCriteria.forClass(RoleGroup.class).add(Restrictions.eq("orgi",orgi))));
    	modelView.addObject("roleList", super.getService().findAllByCriteria(DetachedCriteria.forClass(Role.class).add(Restrictions.eq("orgi",orgi)))) ;
    	modelView.addObject("groupid",groupid);
    	return modelView; 
    }
    
    
    /**
     * add by huqi
     * 增加权限拥有者
     * @param request
     * @param orgi
     * @param tabid
     * @param dicid
     * @param reportid
     * @param ids
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/{dicid}/{resourceid}/roleditdo", name="organeditdo" , type="user" , subtype="public")  
    public ModelAndView roleditdo(HttpServletRequest request , @PathVariable String orgi , @PathVariable String tabid,@PathVariable String dicid,@PathVariable String resourceid, @Valid String  userids,@Valid String  roleids,@Valid String  isDic,@Valid String isReport) throws Exception{  
		ResponseData responseData = new ResponseData("redirect:/{orgi}/user/report/{tabid}/reportproperties/{dicid}/{resourceid}.html") ;
	   	
		//如果是目录
		if("1".equals(isDic)){
			responseData = new ResponseData("redirect:/{orgi}/user/report/{tabid}/{dicid}/reportdicedit.html") ;
    	}
		try{
			if (StringUtils.isNotBlank(userids)){
	    		authInterface.addOwnerList(resourceid, userids, orgi, Auth.OWNER_TYPE_USER,isDic);
			}
			if (StringUtils.isNotBlank(roleids)){
				authInterface.addOwnerList(resourceid, roleids, orgi, Auth.OWNER_TYPE_ROLE,isDic);
			}
		}catch(Exception ce){
			logger.error("添加用户权限出错，错误信息为:"+ce.getMessage(),ce);
			responseData = new ResponseData("redirect:/{orgi}/user/report/{tabid}/{dicid}/reportdicedit.html?msgcode=A_REPORT_100111") ;
		}
    	ModelAndView view = request(responseData , orgi);
    	return view; 
    }
    
    
    @RequestMapping(value="/{dicid}/reportlist", name="reportlist" , type="user" , subtype="public")  
    public ModelAndView reportlist(HttpServletRequest request , @PathVariable String orgi , @PathVariable String tabid, @Valid String dicid,String type ,@Valid String name){  
    	ResponseData responseData = new ResponseData("/pages/manage/task/reportlist") ;
    	ModelAndView view = request(responseData , orgi) ; 
    	view.addObject("reports", ReportFactory.getReportInstance().searchReportList(dicid , type ,name, orgi, super.getUser(request), tabid, RivuDataContext.ReportTypeEnum.REPORT.toString() , null)) ;
    	return view; 
    }
    
    public boolean publisho(HttpServletRequest request ,String orgi  ,String reportid,String publishType,String isRecover,String url) throws Exception{  
    	boolean flag = true;
    	PublishedReport rpt = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportid);
    	if("local".equals(publishType)){//本地发布
    		rpt.setPublishedtype("published");
    		rpt.setStatus("available");
    		if(!"yes".equals(isRecover)){
    			rpt.setReportversion(rpt.getReportversion()+1);
    		}
    		ReportFactory.getReportInstance().updateReport(rpt, super.getUser(request));
    	}else{//远程发布
    		List<NameValuePair> nvps=new ArrayList<NameValuePair>();  
    		nvps.add(new BasicNameValuePair("isRecover", isRecover)) ;
    		nvps.add(new BasicNameValuePair("reportjson", RivuTools.encryption(JSON.toJSONString(rpt)))) ;
    		
    		try {
				String result = RivuTools.postData(new StringBuffer().append(url).append("/api/report/reportpublish.html").toString(), nvps);
				if(!"successful".equals(result))
					flag=false;
				
			} catch (Exception e) {//服务器地址输入错误，或者服务器已关闭
				// TODO Auto-generated catch block
				e.printStackTrace();
				flag = false;
			}finally{
				return flag;
			}
    		
    	}
    	return flag;
    }
    /**
	 * 查询模型目录路径
	 * @param id当前模型目录
	 * @param str
	 * @param orgi
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String getParent(String id,String orgi){
		StringBuffer strb = new StringBuffer() ;
		List<DataDic> diclist = super.getService().findAllByCriteria(DetachedCriteria.forClass(DataDic.class).add(Restrictions.eq("orgi",orgi)));
		DataDic parent = null ;
		while(parent==null){
			for(DataDic type : diclist){
				if(type.getId().equals(id)){
					parent = type ;
					id = parent.getParentid();
					break ;
				}
			}
			if(parent!=null){
				strb.insert(0, parent.getName()).insert(0,"/");
				parent = null;
			}else{
				break ;
			}
		}
		return strb.toString();
	}
	/**
	 * 查询模型目录路径id字符串
	 * @param id
	 * @param orgi
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String getParentIds(String id,String orgi){
		if("0".equals(id))return "0";
		StringBuffer strb = new StringBuffer() ;
		List<DataDic> diclist = super.getService().findAllByCriteria(DetachedCriteria.forClass(DataDic.class).add(Restrictions.eq("orgi",orgi)));
		DataDic parent = null ;
		while(parent==null){
			for(DataDic type : diclist){
				if(type.getId().equals(id)){
					parent = type ;
					id = parent.getParentid();
					break ;
				}
			}
			if(parent!=null){
				strb.insert(0, parent.getId()).insert(0,"/");
				parent = null;
			}else{
				break ;
			}
		}
		return strb.toString();
	}
	
	@RequestMapping(value="/{usertype}/selUser", name="selUser" , type="user" , subtype="public") 
	public ModelAndView selUser(HttpServletRequest request,
			@PathVariable String orgi,@PathVariable String usertype) throws Exception{
		ModelAndView view = request(
				super.createManageResponse("/pages/user/selusergroup/seluser"), orgi);
		String v_username=request.getParameter("username");
		//判断usertype是否为空，如果为空默认成角色
		List<User> users = new ArrayList<User>();
		if(usertype == null || usertype == ""){
			usertype ="organ";
		}
		Object seltype = request.getParameter("seltype");
		if(seltype!=null){
			view.addObject("seltype", seltype.toString());
			view.addObject("cubeid", request.getParameter("cubeid"));
			view.addObject("measureid", request.getParameter("measureid"));
			view.addObject("levelid", request.getParameter("levelid"));
		}
		
		
		if(v_username != null && v_username!= ""){
			users = super.getService().findAllByCriteria(DetachedCriteria.forClass(User.class)
					.add(Restrictions.eq("orgi", orgi))
					.add(Restrictions.or(Restrictions.like("username", v_username, MatchMode.ANYWHERE), Restrictions.like("nickname", v_username, MatchMode.ANYWHERE)) )
					.addOrder(Order.desc("nickname")));
		}else{
		 if(usertype.equals("role")){
			//List<Role> roles = super.getService().findAllByCriteria(DetachedCriteria.forClass(Role.class)
			//		.add(Restrictions.eq("orgi", orgi)).add(Restrictions.eq("groupid", "0")));
			List<RoleGroup> groups = super.getService().findAllByCriteria(DetachedCriteria.forClass(RoleGroup.class)
					.add(Restrictions.eq("orgi", orgi)).add(Restrictions.eq("parentid", "0")));
			//view.addObject("userPackageList", roles);
			view.addObject("userPackageList", groups);
			List<Role> rolesList = super.getService().findAllByCriteria(DetachedCriteria.forClass(Role.class)
					.add(Restrictions.eq("groupid", "0"))
					.add(Restrictions.eq("orgi", orgi)));
			view.addObject("roleList", rolesList) ;
//			view.addObject("userPackageGroup", groups);
		}else if (usertype.equals("organ")){
			List<Organ> organs = super.getService().findAllByCriteria(DetachedCriteria.forClass(Organ.class)
					.add(Restrictions.eq("orgi", orgi)));
			view.addObject("userPackageList", organs);
			List<Organ> organList = super.getService().findAllByCriteria(DetachedCriteria.forClass(Organ.class)
					.add(Restrictions.eq("parentid", "0"))
					.add(Restrictions.eq("orgi", orgi)));
			view.addObject("organList", organList) ;
			List<UserOrgan> userorgans = super.getService().findAllByCriteria(DetachedCriteria.forClass(UserOrgan.class)
					.add(Restrictions.eq("organid", "0"))
					.add(Restrictions.eq("orgi", orgi)));
			for (UserOrgan userOrgan : userorgans) {
				users.add(userOrgan.getUser());
			}
		 }
		}	 
		if(request.getParameter("sub")!=null){
			view.addObject("sub", true) ;
		}
		view.addObject("usertype", usertype);
		view.addObject("userList", users);
		return view;
	}
	
	
	@RequestMapping(value="/{usertype}/{userdicid}/userdic", name="userdic" , type="user" , subtype="public") 
	public ModelAndView userdic(HttpServletRequest request,
			@PathVariable String orgi,@PathVariable String userdicid,@PathVariable String usertype) throws Exception{
		ModelAndView view = request(
				super.createManageResponse("/pages/user/selusergroup/userdic"), orgi);
		Object seltype = request.getParameter("seltype");
		if(seltype!=null){
			view.addObject("seltype", seltype.toString());
			view.addObject("cubeid", request.getParameter("cubeid"));
			view.addObject("measureid", request.getParameter("measureid"));
			view.addObject("levelid", request.getParameter("levelid"));
		}
		List<User> users = new ArrayList<User>();
		if(usertype.equals("role")){
			List<UserRole> roles = super.getService().findAllByCriteria(DetachedCriteria.forClass(UserRole.class)
					.add(Restrictions.eq("roleid", userdicid))
					.add(Restrictions.eq("orgi", orgi)));
			
			List<Role> rolesList = super.getService().findAllByCriteria(DetachedCriteria.forClass(Role.class)
					.add(Restrictions.eq("groupid", userdicid))
					.add(Restrictions.eq("orgi", orgi)));
			view.addObject("roleList", rolesList) ;
			for (UserRole userRole : roles) {
				users.add(userRole.getUser());
			}
		}else if (usertype.equals("organ")){
			List<UserOrgan> organs = super.getService().findAllByCriteria(DetachedCriteria.forClass(UserOrgan.class)
					.add(Restrictions.eq("organid", userdicid))
					.add(Restrictions.eq("orgi", orgi)));
			
			List<Organ> organList = super.getService().findAllByCriteria(DetachedCriteria.forClass(Organ.class)
					.add(Restrictions.eq("parentid", userdicid))
					.add(Restrictions.eq("orgi", orgi)));
			view.addObject("organList", organList) ;
			for (UserOrgan userOrgan : organs) {
				users.add(userOrgan.getUser());
			}
		}
		if(request.getParameter("sub")!=null){
			view.addObject("sub", true) ;
		}
		view.addObject("userList", users);
		return view;
	}
	
	

} 