package com.rivues.api.util;

import org.apache.commons.lang.StringUtils;

import com.rivues.api.user.R3User;
import com.rivues.api.user.R3UserHelper;

public class StringUtil {
	public static boolean isBlank(String arg0){
		boolean flag = false;
		if(arg0==null)
			flag = true;
		if("".equals(arg0))
			flag = true;
		
		return flag;
	}
	
}
