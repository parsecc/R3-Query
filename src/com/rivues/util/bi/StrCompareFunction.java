package com.rivues.util.bi;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mondrian.olap.Evaluator;
import mondrian.olap.Syntax;
import mondrian.olap.type.BooleanType;
import mondrian.olap.type.StringType;
import mondrian.olap.type.Type;
import mondrian.spi.UserDefinedFunction;

public class StrCompareFunction implements UserDefinedFunction {
	
	public StrCompareFunction(){}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "compare";
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return "Compare String OR Number";
	}

	@Override
	public Syntax getSyntax() {
		// TODO Auto-generated method stub
		return Syntax.Function;
	}

	@Override
	public Type[] getParameterTypes() {
		// TODO Auto-generated method stub
		return new Type[] {new StringType() , new StringType() , new StringType() , new StringType()};
	}

	@Override
	public Type getReturnType(Type[] parameterTypes) {
		return new BooleanType();
	}
	
	@Override
	public Object execute(Evaluator evaluator, Argument[] arguments) {
		final Object argValue1 = arguments[0].evaluateScalar(evaluator);
		final Object argValue2 = arguments[1].evaluateScalar(evaluator);
		final Object argValue3 = arguments[2].evaluateScalar(evaluator);
		final Object argValue4 = arguments[3].evaluateScalar(evaluator);
		
		if(Boolean.valueOf(((String)argValue1).matches((String)argValue2)) || argValue2==null || ((String)argValue2).length()==0){
			if(argValue2==null || ((String)argValue2).length()==0){
				return compare((String)argValue1 , (String)argValue3) && compare((String)argValue4 , (String)argValue1);
			}else{
				Pattern pattern = Pattern.compile((String)argValue2) ;
				Matcher mat = pattern.matcher((String)argValue1) ;
				if(mat.find() && mat.groupCount()>=1){
					String value = mat.group(1) ;
					return compare((String)value , (String)argValue3) && compare((String)argValue4 , (String)value);
				}else{
					return compare((String)argValue1 , (String)argValue3) && compare((String)argValue4 , (String)argValue1);
				}
			}
		}else{
			return false;
		}
	}
	private static boolean compare(String str1, String str2){
		boolean flag = true;
		if(str1!=null&&str1.length()>0&&str2!=null&&str2.length()>0){//判断不为空或者空字符串然后再做处理
			if (str1.matches("\\d+([.]\\d+)?")&&str2.matches("\\d+([.]\\d+)?")) {//数字类型
				try {
					BigDecimal bd1 = new BigDecimal(str1);
					BigDecimal bd2 = new BigDecimal(str2);
					flag = bd1.compareTo(bd2)>=0;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{//字符串类型
				flag = str1.compareTo(str2)>=0;
			}
		}
		return flag;
	}
	@Override
	public String[] getReservedWords() {
		// TODO Auto-generated method stub
		return null;
	}
	public static void main(String[] args){
		String str1="11231122313213212311232131";//\\d+[^](.\\d+)?
		if (str1.matches("\\d+([.]\\d+)?")) {//数字类型
			System.out.println("数字");
			BigDecimal bd1 = new BigDecimal(str1);
			System.out.println(bd1.doubleValue());
		}else{//字符串类型
			System.out.println("字符串");
		}
	}
}
