package com.rivues.util.session.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.shiro.session.Session;

import com.rivues.util.service.cache.CacheHelper;
import com.rivues.util.session.ShiroSessionRepository;

public class HazlcastSessionReporitoryImpl implements ShiroSessionRepository{

	@Override
	public void saveSession(Session session) {
		if(session!=null && session.getId()!=null){
			CacheHelper.getDistributedSessionCacheBean().put(session.getId().toString(), session, null) ;
		}
	}

	@Override
	public void deleteSession(Serializable sessionId) {
		if(sessionId!=null) {
			CacheHelper.getDistributedSessionCacheBean().delete(sessionId.toString(), null) ;
		}
	}

	@Override
	public Session getSession(Serializable sessionId) {
		return sessionId!=null ? (Session) CacheHelper.getDistributedSessionCacheBean().getCacheObject(sessionId.toString(), null) : null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Session> getAllSessions() {
		
		Collection<org.apache.shiro.session.mgt.SimpleSession> sessionestr = (Collection<org.apache.shiro.session.mgt.SimpleSession>) CacheHelper.getDistributedSessionCacheBean().getAllCacheObject(null);
		List<Session> sessiones = new ArrayList<Session>();
		for (org.apache.shiro.session.mgt.SimpleSession string : sessionestr) {
			sessiones.add(string);
		}
		return sessiones;
		
	}

	
}
