package com.rivues.util.session;

import java.io.Serializable;
import java.util.Collection;

import org.apache.shiro.session.Session;
/**
 * Session 存储接口
 * @author admin
 *
 */
public interface ShiroSessionRepository {  
	/**
	 * 保存Session  
	 * @param session
	 */
    void saveSession(Session session);  
    
    /**
     * 删除Session
     * @param sessionId
     */
    void deleteSession(Serializable sessionId);  
    /**
     * 根据Session ID获取 Session
     * @param sessionId
     * @return
     */
    Session getSession(Serializable sessionId);  
    
    /**
     * 获取所有Session
     * @return
     */
    Collection<Session> getAllSessions();  
}  