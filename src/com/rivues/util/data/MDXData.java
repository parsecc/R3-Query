package com.rivues.util.data;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("rawtypes")
public class MDXData implements ScriptData{
	
	private List rowDimeasion = new ArrayList(2);
	private List colDimeasion = new ArrayList(2);
	private List measures  = new ArrayList(2);
	private List sort  = new ArrayList(2);
	private List filter  = new ArrayList(2);
	public List getRowDimeasion() {
		return rowDimeasion;
	}
	public void setRowDimeasion(List rowDimeasion) {
		this.rowDimeasion = rowDimeasion;
	}
	public List getColDimeasion() {
		return colDimeasion;
	}
	public void setColDimeasion(List colDimeasion) {
		this.colDimeasion = colDimeasion;
	}
	public List getMeasures() {
		return measures;
	}
	public void setMeasures(List measures) {
		this.measures = measures;
	}
	public List getSort() {
		return sort;
	}
	public void setSort(List sort) {
		this.sort = sort;
	}
	public List getFilter() {
		return filter;
	}
	public void setFilter(List filter) {
		this.filter = filter;
	}
	
}
