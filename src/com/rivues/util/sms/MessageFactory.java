package com.rivues.util.sms;

import com.rivues.core.RivuDataContext;
import com.rivues.util.service.cache.CacheHelper;

public class MessageFactory {
	
	private static MessageSend messageSend;
	
	public static MessageSend getInstance(String orgi) throws Exception{
		String clazz = (String) CacheHelper.getDistributedDictionaryCacheBean().getCacheObject(RivuDataContext.SystemParamEnum.SHORT_MESSAGE_CLASS.toString(), orgi) ;
		return messageSend!=null ? messageSend : (messageSend = (MessageSend) Class.forName(clazz!=null ? clazz : "com.rivues.util.sms.MessageSendImpl").newInstance());
	}

}
