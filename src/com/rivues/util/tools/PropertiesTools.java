package com.rivues.util.tools;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import com.rivues.util.RivuTools;

import freemarker.template.TemplateException;

public class PropertiesTools {
	private ResourceBundle myResource = null;//;
	private PropertiesTools(){}
	
	public PropertiesTools(String propertiesName){
		myResource = ResourceBundle.getBundle(propertiesName);
	}
	
	public String getMessage(String code){
		return myResource.getString(code);
	}
}
