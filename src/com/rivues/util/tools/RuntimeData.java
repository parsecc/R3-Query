/**
 * 
 */
package com.rivues.util.tools;

import java.io.File;
import java.util.Date;

import com.rivues.core.RivuDataContext;
import com.rivues.util.local.LocalTools;

/**
 * @author iceworld
 *
 */
public class RuntimeData {
	private long allocateMemory = Runtime.getRuntime().totalMemory();
	private long remainingMemory  = Runtime.getRuntime().freeMemory();
	private long usingMemory  = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
	private static String operatingSystem = System.getProperty("os.name");
	private static String jdkPath = System.getProperty("java.home");
	private static String userPath = System.getProperty("user.dir");
	private static String osUser = System.getProperty("user.name");
	private static String jvm = System.getProperty("java.vm.name");
	private static String jreVersion = System.getProperty("java.class.version");
	private static String jvmVersion = System.getProperty("java.version");
	private static int searchQueryNum = 0;
	private float averageResponseTime = 0 ;
	private static long searchQueryTime = 0 ;
	private long diskSpace = new File(".").getTotalSpace();
	private long diskFreeSpace = new File(".").getFreeSpace();
	private long usedDiskSpace = diskSpace - diskFreeSpace;
	private static Date starttime = new Date();
	private long uptime ;
	private static String ipaddr = "";
	private static String hostname = "";
	private static int port = 0;
	private static String storagepath = "" ;
	private static long storagesize = 0 ;
	private static int storagefiles = 0 ;
	private static Date lastcheck = new Date();
	private static long storagemax = 0 ;


	
	public String getRunningTime(){
		return new StringBuffer().append((int)Math.floor(getUptime()/(3600*24))).append(LocalTools.getMessage("I_MONITOR_1000001")).append((int)Math.floor((getUptime()%(3600*24))/3600)).append(LocalTools.getMessage("I_MONITOR_1000002")).append((int)Math.floor((getUptime()%(3600))/60)).append(LocalTools.getMessage("I_MONITOR_1000003")).append((int)Math.floor((getUptime()%(60)))).append(LocalTools.getMessage("I_MONITOR_1000004")).toString() ;
	}
	
	public long getAllocateMemory() {
		return allocateMemory;
	}
	public void setAllocateMemory(long allocateMemory) {
		this.allocateMemory = allocateMemory;
	}
	public long getRemainingMemory() {
		return remainingMemory;
	}
	public void setRemainingMemory(long remainingMemory) {
		this.remainingMemory = remainingMemory;
	}
	public long getUsingMemory() {
		return usingMemory;
	}
	public void setUsingMemory(long usingMemory) {
		this.usingMemory = usingMemory;
	}
	public String getOperatingSystem() {
		return operatingSystem;
	}
	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}
	public String getJdkPath() {
		return jdkPath;
	}
	public void setJdkPath(String jdkPath) {
		this.jdkPath = jdkPath;
	}
	public String getUserPath() {
		return userPath;
	}
	public void setUserPath(String userPath) {
		this.userPath = userPath;
	}
	public String getOsUser() {
		return osUser;
	}
	public void setOsUser(String osUser) {
		this.osUser = osUser;
	}
	public String getJvm() {
		return jvm;
	}
	public void setJvm(String jvm) {
		this.jvm = jvm;
	}
	public String getJreVersion() {
		return jreVersion;
	}
	public void setJreVersion(String jreVersion) {
		this.jreVersion = jreVersion;
	}
	public String getJvmVersion() {
		return jvmVersion;
	}
	public void setJvmVersion(String jvmVersion) {
		this.jvmVersion = jvmVersion;
	}
	
	public int getSearchQueryNum() {
		return this.searchQueryNum;
	}
	public static void setSearchQueryNum(int searchQueryNum) {
		RuntimeData.searchQueryNum += searchQueryNum;
	}
	public float getAverageResponseTime() {
		return ((float)((int)((this.searchQueryNum!=0? ((float)searchQueryTime/1000f)/(float)this.searchQueryNum:0)*1000)))/1000f;
	}
	public void setAverageResponseTime(float averageResponseTime) {
		this.averageResponseTime = averageResponseTime;
	}
	public long getDiskSpace() {
		return diskSpace;
	}
	public void setDiskSpace(long diskSpace) {
		this.diskSpace = diskSpace;
	}
	public long getDiskFreeSpace() {
		return diskFreeSpace;
	}
	public void setDiskFreeSpace(long diskFreeSpace) {
		this.diskFreeSpace = diskFreeSpace;
	}
	public long getUsedDiskSpace() {
		return usedDiskSpace;
	}
	public void setUsedDiskSpace(long usedDiskSpace) {
		this.usedDiskSpace = usedDiskSpace;
	}
	public Date getStarttime() {
		return starttime;
	}
	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	public long getSearchQueryTime() {
		return searchQueryTime;
	}
	public static void setSearchQueryTime(long curremtSearchQueryTime) {
		RuntimeData.searchQueryTime += curremtSearchQueryTime;
	}
	public String getIpaddr() {
		return ipaddr;
	}
	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public long getUptime() {
		return (System.currentTimeMillis() - starttime.getTime())/1000 ;
	}
	public void setUptime(long uptime) {
		this.uptime = uptime;
	}

	public static int getPort() {
		return port;
	}

	public static String getStoragepath() {
		return storagepath;
	}

	public static void setStoragepath(String storagepath) {
		RuntimeData.storagepath = storagepath;
	}


	public static long getStoragesize() {
		return storagesize;
	}

	public static void setStoragesize(long storagesize) {
		RuntimeData.storagesize = storagesize;
	}

	public static int getStoragefiles() {
		return storagefiles;
	}

	public static void setStoragefiles(int storagefiles) {
		RuntimeData.storagefiles = storagefiles;
	}

	public static Date getLastcheck() {
		return lastcheck;
	}

	public static void setLastcheck(Date lastcheck) {
		RuntimeData.lastcheck = lastcheck;
	}

	public static long getStoragemax() {
		return storagemax;
	}

	public static void setStoragemax(long storagemax) {
		RuntimeData.storagemax = storagemax;
	}
}
