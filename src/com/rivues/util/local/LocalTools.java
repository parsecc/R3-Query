package com.rivues.util.local;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import com.rivues.util.RivuTools;
import com.rivues.util.exception.RivuException;

import freemarker.template.TemplateException;

public class LocalTools {
	private final static ResourceBundle myResource = ResourceBundle.getBundle("local/message/messages_"+Locale.getDefault(),Locale.getDefault());
	
	public static String getMessage(String code){
		String message = "";
		try {
			message = myResource.getString(code);
		} catch (Exception e) {
			new RivuException("CONFIG_ERR_00001");
		}
		return message;
	}
	/**
	 * 带参数的国际化语言
	 * @param code
	 * @return
	 */
	public static String getMessage(String code , Map<String , Object> values){
		String message = "";
		try {
			message = RivuTools.getTemplet(myResource.getString(code), values);
		} catch (Exception e) {
			new RivuException("CONFIG_ERR_00001");
		}
		return message;
	}
}
