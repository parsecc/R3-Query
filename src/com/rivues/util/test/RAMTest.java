package com.rivues.util.test;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;

public class RAMTest {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		FileSystemManager manager = VFS.getManager() ;
		FileObject fileObject = manager.createVirtualFileSystem("ram://test") ;
		fileObject.getContent().getOutputStream().write("abc".getBytes()) ;
		byte[] data = new byte[1024] ;
		fileObject.getContent().getInputStream().read(data) ;
		System.out.println(new String(data));
	}

}
