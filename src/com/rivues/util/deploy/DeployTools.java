package com.rivues.util.deploy;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.tools.ant.Task;

public class DeployTools extends Task{
	java.util.logging.Logger log = java.util.logging.Logger.getLogger(DeployTools.class.toString()) ;
	private String overwrite = "yes" ;
	private boolean newversion = true ;
	private String url ;
	private String username ;
	private String password ; 
	private File source ;
	private String cmd = "tip";
	private String path = "/rivues/api/report/reportimporto.html" ;
	
	public void execute(){
		if("tip".equals(cmd)){
			System.out.println("请输入需要执行操作的名称：");
			System.out.println("		publish：发布报表或模型");
			System.out.println("		update：更新报表或模型");
			System.out.println("		delete：删除报表或模型");
			System.out.println("		unavailable：将报表或模型状态置为不可用");
			System.out.println("		patch：为报表或模型更新到新版本");
		}else{
			HttpPost post = new HttpPost(url+"/"+path);  
			List<NameValuePair> nvps=new ArrayList<NameValuePair>();  
			nvps.add(new BasicNameValuePair("isRecover", overwrite)) ;
			HttpClient client = new DefaultHttpClient(); 
			try {
				MultipartEntity multipartEntity = new MultipartEntity(  
		                HttpMultipartMode.BROWSER_COMPATIBLE, null,  
		                Charset.forName("UTF-8"));  
		        FileBody cbFileBody = new FileBody(source);  
		        multipartEntity.addPart("file", cbFileBody);  
		        multipartEntity.addPart("username", new StringBody(username)) ;
		        multipartEntity.addPart("password", new StringBody(password)) ;
		        multipartEntity.addPart("isRecover", new StringBody(overwrite)) ;
		        multipartEntity.addPart("command", new StringBody(cmd)) ;
		        
		        post.setEntity(multipartEntity);  
		        HttpResponse response = client.execute(post) ;
				String msg = EntityUtils.toString(response.getEntity(), "GBK");
		        if(msg.length()>0){
		        	throw new Exception(msg);
		        }
			} catch (Exception e) {//服务器地址输入错误，或者服务器已关闭
				log.info(e.getMessage()) ;
			}finally{
				client.getConnectionManager().shutdown();  
			}
		}
		
	}
	
	public boolean isNewversion() {
		return newversion;
	}
	public void setNewversion(boolean newversion) {
		this.newversion = newversion;
	}

	public String getOverwrite() {
		return overwrite;
	}

	public void setOverwrite(String overwrite) {
		this.overwrite = overwrite;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public File getSource() {
		return source;
	}

	public void setSource(File source) {
		this.source = source;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

}

