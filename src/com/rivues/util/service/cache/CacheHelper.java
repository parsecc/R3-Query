package com.rivues.util.service.cache;

import com.rivues.util.service.cache.impl.CacheInstance;
import com.rivues.util.service.cache.impl.ehcache.EhCacheHelper;

public class CacheHelper {
	
	/**
	 * 获取缓存实例
	 */
	private static CacheInstance cacheInstance = new EhCacheHelper();
	/**
	 * 
	 * @return
	 */
	public static CacheBean getDistributedCacheBean(){
		return cacheInstance.getDistributedCacheBean();
	}
	
	/**
	 * 
	 * @return
	 */
	public static CacheBean getDistributedDictionaryCacheBean(){
		return cacheInstance.getDistributedDictionaryCacheBean();
	}
	
	/**
	 * 
	 * @return
	 */
	public static CacheBean getDistributedReporyDataCacheBean(){
		return cacheInstance.getDistributedReporyDataCacheBean() ;
	}
	
	/**
	 * 
	 * @return
	 */
	public static CacheBean getDistributedSessionCacheBean(){
		return cacheInstance.getDistributedSessionCacheBean() ;
	}
	/**
	 * 
	 * @return
	 */
	public static CacheBean getMdxAggCacheBean(){
		return cacheInstance.getMDXAggCacheBean() ;
	}
	
	/**
	 * 
	 * @return
	 */
	public static CacheBean getClusterServerCacheBean(){
		return cacheInstance.getClusterServerCacheBean() ;
	}
}
