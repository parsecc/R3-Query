package com.rivues.util.service.system;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.JobDetail;
import com.rivues.module.platform.web.model.warning.EventBean;
import com.rivues.util.RivuTools;
import com.rivues.util.local.LocalTools;
import com.rivues.util.service.distributed.DeleteFileTask;
import com.rivues.util.service.monitor.BusinessService;
import com.rivues.util.tools.RuntimeData;

@Component
public class ExportFileService extends BusinessService{
	private static Logger log = LoggerFactory.getLogger(ExportFileService.class) ;
	private static boolean taskRunning = false ;
	@Scheduled(fixedRate = 1000*5)  
	public void service(){
		if (RivuDataContext.isStart() && !RivuDataContext.isStopping() && !taskRunning) {
			try{
				taskRunning = true ; 
				int files = 0 ;
				long storage = 0;
				File[] jobsListFiles = RivuTools.getDefaultExportJobPath("rivues").listFiles() ;
				for(File job : jobsListFiles){
					if(job.length()>0){
						JobDetail jobDetail = (JobDetail) RivuTools.toObject(FileUtils.readFileToByteArray(job)) ;
						File dataFile = RivuTools.getExportFileWithOutTimeOut(jobDetail.getOrgi(), jobDetail.getId()) ;
						File cataFileCheckWithTimeOut = RivuTools.getExportFile(jobDetail.getOrgi(), jobDetail.getId()) ;
						if(cataFileCheckWithTimeOut==null || !dataFile.exists()){
							/**
							 * 文件已过期，从分布式系统中删除文件
							 */
							new DeleteFileTask(jobDetail.getId() , jobDetail.getOrgi()).call() ;
						}
					}
				}
				
				
				File[] fileListFiles = RivuTools.getDefaultExportFilePath("rivues").listFiles() ;
				for(File job : fileListFiles){
					storage = storage + job.length() ;
					files++;
				}
				
				File nasPath = RivuTools.getDefaultExportFilePath("rivues") ;
				RuntimeData.setStoragefiles(files);
				RuntimeData.setStoragesize(storage);
				RuntimeData.setStoragepath(nasPath.getAbsolutePath());
				RuntimeData.setStoragemax(RivuTools.getMaxExportFileStorageSize("rivues"));
				RuntimeData.setLastcheck(new Date());
				RuntimeData runtimeData = new RuntimeData() ;
				/**
				 * 
				 */
				if(RuntimeData.getStoragesize() > RivuTools.getMaxExportFileStorageSize("rivues")){
					EventBean eventLog = new EventBean();
					eventLog.setHostname(runtimeData.getHostname()) ;
					eventLog.setIpaddr(runtimeData.getIpaddr()) ;
					eventLog.setGroupid("") ;
					eventLog.setDatatype(RivuDataContext.SystemInfoType.R3QUERY_V50_SERVER.toString());
					eventLog.setPort(RuntimeData.getPort());
					eventLog.setEventype(RivuDataContext.EventType.STORAGESPACE.toString()) ;
					Map<String , Object> values = new  HashMap<String, Object>() ;
					values.put("space", RivuTools.getMaxExportFileStorageSize("rivues")) ;
					values.put("storage", RuntimeData.getStoragesize()) ;
					
					eventLog.setEventmsg(new StringBuffer().append(LocalTools.getMessage("E_MSG_9008001" , values)).append(eventLog.getIpaddr()).append(":").append(eventLog.getPort()).append("(").append(eventLog.getHostname()).append(")").toString());
					eventLog.setEventlevel(RivuDataContext.EventLevelType.CRITICAL.toString()) ;
					
					DataPersistenceService.persistence(eventLog) ;
					log.warn(eventLog.getEventmsg());
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}finally{
				taskRunning = false ;
			}
		}
	}
	
	/**
	 * 服务名称
	 */
	public String getName(){
		return RivuDataContext.ServiceTypeName.EXPORT_FILE.toString();
	}
}
