package com.rivues.util.service.system;

import java.util.Collection;
import java.util.Iterator;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.rivues.core.RivuDataContext;
import com.rivues.util.service.cache.CacheHelper;
import com.rivues.util.service.monitor.BusinessService;

@Component
public class ClusterServerService extends BusinessService{
	
	@Scheduled(fixedRate = 1000 * 3)  
	public void service(){
		ServerStatus serverStatus = new ServerStatus() ;
		serverStatus.setServer(RivuDataContext.getServerName()) ;
		serverStatus.setTasks(RivuDataContext.getLocalRunningJob().size()) ;
		serverStatus.setTimes(System.currentTimeMillis()) ;
		/**
		 
		 */
	}

	@Override
	public String getName() {
		return "CLUSTER_SERVER_STATUS";
	}
	/**
	 * 返回 任务执行的目标服务器
	 * @return
	 */
	public static String getRunServer(){
		return "";
	}
}
