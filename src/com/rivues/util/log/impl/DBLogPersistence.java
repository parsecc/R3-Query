package com.rivues.util.log.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.Log;
import com.rivues.module.platform.web.model.QueryLog;
import com.rivues.module.platform.web.model.RequestLog;
import com.rivues.module.platform.web.model.SystemLog;
import com.rivues.util.log.Persistence;

/**
 * 日志存放在  数据库的持久化实现
 * @author admin
 *
 */
public class DBLogPersistence extends Persistence {
	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Log> getBusLogList(String orgi, String level , String flowid , String startid, int start, int ps) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Log.class).add(Restrictions.eq("orgi", orgi)).addOrder(Order.desc("createdate")) ;
		if(level!=null && level.length() > 0){
			criteria.add(Restrictions.eq("level", level)) ;
		}
		if(flowid!=null && flowid.length()>0){
			criteria.add(Restrictions.eq("flowid", flowid)) ;
		}
		if(startid!=null && startid.length()>0){
			criteria.add(Restrictions.eq("startid", startid)) ;
		}
				
		return RivuDataContext.getService().findPageByCriteria(criteria,ps,start);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Log> getBusLogPage(DetachedCriteria criteria, int start, int ps) {
		return RivuDataContext.getService().findPageByCriteria(criteria,ps,start);
	}
	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SystemLog> getSystemLogList(String orgi, String type,
			int start, int ps) {
		DetachedCriteria criteria = DetachedCriteria.forClass(SystemLog.class).add(Restrictions.eq("orgi", orgi)).addOrder(Order.desc("starttime")) ;
		if(type!=null && type.length() > 0){
			criteria.add(Restrictions.eq("type", type)) ;
		}
		return RivuDataContext.getService().findPageByCriteria(criteria,ps,start);
	}
	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<QueryLog> getRequestLogList(String orgi, String type, int start,
			int ps) {
		DetachedCriteria criteria = DetachedCriteria.forClass(RequestLog.class).add(Restrictions.eq("orgi", orgi)).addOrder(Order.desc("starttime")) ;
		String[] types; //存储多参数
		types=type.trim().split("-");  //参数按照'-'符号拆分
		
		if(types.length>1){   //若参数不唯一，则添加条件
		  if(types[0]!=null && types[1]!=null && types[0].length()>0 && types[1].length()>0){	
			criteria.add(Restrictions.eq("funtype", types[1])).add(Restrictions.eq("name", types[0]));
		  }	
		}else{
		
		  if(type!=null && type.length() > 0){
			criteria.add(Restrictions.eq("funtype", type)) ;
		  }
		
		}
		return RivuDataContext.getService().findPageByCriteria(criteria,ps,start);
	}

	@Override
	public List<Log> getBusLogPageByIndex(DetachedCriteria criteria, int start,
			int ps) {
		// TODO Auto-generated method stub
		return RivuDataContext.getService().findPageByCriteria(criteria, ps, start);
	}

	@Override
	public int getLogCount(DetachedCriteria criteria) {
		// TODO Auto-generated method stub
		return RivuDataContext.getService().getCountByCriteria(criteria);
	}
}
