package com.rivues.util.log;

import com.rivues.util.log.impl.DBLogPersistence;

public class LoggerFactory {
	/**
	 * 
	 * @param clazz
	 * @return
	 */
	public static Logger getLogger(Class<?> clazz){
		return new Logger(clazz) ;
	}
	
}
