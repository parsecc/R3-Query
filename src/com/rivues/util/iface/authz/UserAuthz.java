package com.rivues.util.iface.authz;

import java.io.Serializable;
import java.util.List;

import com.rivues.module.platform.web.model.Auth;
import com.rivues.module.platform.web.model.Organ;
import com.rivues.module.platform.web.model.Role;
import com.rivues.module.platform.web.model.User;
import com.rivues.module.platform.web.model.UserRole;

public interface UserAuthz extends Serializable{
	/**
	 * 提供安全认证
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean authentication(String username , String password , String orgi) ;
	/**
	 * 根据用户ID获取用户信息，根据接口不同
	 * @param userid
	 * @return
	 */
	public User getUser(String userid , String orgi) ;
	
	/**
	 * 根据用户UserName获取用户信息，根据接口实现不同
	 * @param username
	 * @return
	 */
	public User getUserName(String username , String orgi) ;
	
	/**
	 * 
	 * @return
	 */
	public List<Role> getRoles(String orgi) ;
	
	/**
	 * 根据资源id获取角色
	 * @param resourceid
	 * @param orgi
	 * @return
	 */
	public List<Auth> getAuth(String resourceid,String orgi) ;
	
	
	/**
	 * 根据用户 获取用户的 授权角色 
	 * @param user
	 * @return
	 */
	public List<UserRole> getUserRoleList(User user) ;
	/**
	 * 根据角色获取所有用户 ， 部分实现支持，R3内置用户系统支持获取所有用户列表
	 * @return
	 */
	public List<User> getUserListByRole(String role , String orgi) ;
	
	/**
	 * 
	 * @param organ , 根据组织机构获取用户列表
	 * @return
	 */
	public List<User> getUserListByOrgan(String organ , String orgi) ;
	
	/**
	 * 用户是否为系统管理员
	 * @param orgi
	 * @return
	 */
	public boolean isSystemManager(String orgi,String username);
	/**
	 * 用户是否为模型管理员
	 * @param orgi
	 * @return
	 */
	public boolean isModelManager(String orgi,String username);
	/**
	 * 用户是否为报表管理员
	 * @param orgi
	 * @return
	 */
	public boolean isReportManager(String orgi,String username);
	/**
	 * 用户是否为目录管理员
	 * @param orgi
	 * @return
	 */
	public boolean isDicManager(String orgi,String username);
	
	
	
}
