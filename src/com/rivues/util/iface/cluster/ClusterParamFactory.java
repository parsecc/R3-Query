package com.rivues.util.iface.cluster;

public class ClusterParamFactory {
	private static ParamInterface param ;
	/**
	 * 
	 * @return
	 */
	public static ParamInterface getParamTools(){
		return param!=null ? param : (param = new ParamDatabaseImpl()) ;
	}
}
