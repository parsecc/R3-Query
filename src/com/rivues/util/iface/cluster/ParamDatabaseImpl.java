package com.rivues.util.iface.cluster;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.ConfigureParam;

public class ParamDatabaseImpl implements ParamInterface{

	/**
	 * 获取参数值
	 */
	@Override
	public String getParam(String param) {
		List<ConfigureParam> paramList = RivuDataContext.getService().findAllByCriteria(DetachedCriteria.forClass(ConfigureParam.class).add(Restrictions.eq("name", param)))  ;
		return paramList!=null && paramList.size()>0 ? paramList.get(0).getValue() : null ;
	}

	@Override
	public ConfigureParam getConfigureParam(String name) {
		List<ConfigureParam> paramList = RivuDataContext.getService().findAllByCriteria(DetachedCriteria.forClass(ConfigureParam.class).add(Restrictions.eq("name", name)))  ;
		return paramList!=null && paramList.size()>0 ? paramList.get(0) : null ;
	}

}
