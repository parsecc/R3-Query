package com.rivues.util.datasource;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.rivues.module.platform.web.model.AnalyzerReportModel;
import com.rivues.module.platform.web.model.Cube;
import com.rivues.module.platform.web.model.CubeLevel;
import com.rivues.module.platform.web.model.QueryLog;
import com.rivues.module.platform.web.model.QueryText;
import com.rivues.module.platform.web.model.ReportFilter;
import com.rivues.util.data.ReportData;
import com.rivues.util.tools.ExportFile;

public class R3VDataTools  implements DataSource {

	@Override
	public ReportData getData(AnalyzerReportModel model,
			Map<String, Object> requestParamValues, Cube cube,
			HttpServletRequest request, boolean parseParam,
			QueryText queryText, QueryLog queryLog) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void getRSData(AnalyzerReportModel model,
			Map<String, Object> requestParamValues, Cube cube,
			HttpServletRequest request, boolean parseParam,
			QueryText queryText, ExportFile export) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void refresh(String ds, Cube cube) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public QueryText getQuery(AnalyzerReportModel model, Cube cube,
			HttpServletRequest request, boolean parseParam, QueryLog queryLog) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public QueryText getQuery(AnalyzerReportModel model, Cube cube,
			HttpServletRequest request, CubeLevel level, String con,
			QueryLog queryLog, boolean useStaticFilters, ReportFilter filter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processParam(String value, Cube cube,
			AnalyzerReportModel model, HttpServletRequest request,
			boolean parseParam, QueryText queryText) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean valid(Cube cube) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ReportData getVData(AnalyzerReportModel model,
			Map<String, Object> requestParamValues, Cube cube,
			HttpServletRequest request, boolean parseParam,
			QueryText queryText, QueryLog queryLog) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
