package com.rivues.util.datasource;

import java.util.ArrayList;
import java.util.List;

import com.rivues.module.platform.web.model.Dimension;
import com.rivues.module.platform.web.model.ReportFilter;

public class ConditionExpress implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2823774912655568093L;
	private String condition ;
	private List<ReportFilter> queryFilterValuetList = new ArrayList<ReportFilter>();;
	private List<Dimension> conDimList = new ArrayList<Dimension>();

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public List<Dimension> getConDimList() {
		return conDimList;
	}

	public void setConDimList(List<Dimension> conDimList) {
		this.conDimList = conDimList;
	}

	public List<ReportFilter> getQueryFilterValuetList() {
		return queryFilterValuetList;
	}

	public void setQueryFilterValuetList(List<ReportFilter> queryFilterValuetList) {
		this.queryFilterValuetList = queryFilterValuetList;
	}

}
