package com.rivues.util.datasource;

public class QueryDim implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2959257107454847206L;
	private String dimid;
	private String constr ;
	private int index ;
	public QueryDim(String dimid , String constr , int inx) {
		this.dimid = dimid ;
		this.constr = constr ;
		this.index = inx ;
	}
	
	public String getDimid() {
		return dimid;
	}
	public void setDimid(String dimid) {
		this.dimid = dimid;
	}
	public String getConstr() {
		return constr;
	}
	public void setConstr(String constr) {
		this.constr = constr;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
}
